---
tags:
  - System Administration
  - Sysadmin
  - GNU Core Utilities Commands
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Shred

`shred` overwrites a file to hide its contents, and optionally delete it.

When deleting a file from Linux (or from any OS), the file is generally not deleted permanently
from the hard disk. It first gets moved to the trash, and even after emptying the trash: the file
is still there in the hard drive and could be recovered. Indeed, only the pointer - pointing to the
file location - is cleared, so the associated data space is considered unallocated (free space) but
can be recovered easily.

The file gets permanently deleted when the OS writes over the data space of the file which was
considered as unallocated. So, in order to delete a file completely from hard disk, `shred` can be
used. This command overwrites the contents of a file multiple times, using patterns chosen to
maximize the destruction of the residual data, making it harder for even very expensive hardware
probing to recover it.

???+ Note "Reference(s)"
    * <https://www.geeksforgeeks.org/shred-command-in-linux-with-examples/>
    * <https://man.archlinux.org/man/shred.1>
    * <https://wiki.archlinux.org/title/Securely_wipe_disk#shred>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

`shred` is part of the [GNU Core Utilities](https://en.wikipedia.org/wiki/GNU_Core_Utilities), so
it should be installed by default. You can check it by running `$ shred --version`.

---
## Use

* overwrite the contents of the file multiple times (by default 3 times with random content, in
  order to make it almost unrecoverable), it will change the file data in such a way that it would
  be really hard to get the old file back:
    ```console
    $ shred filename.txt
    ```
    > Note: after running this command `filename.txt` *will still exist*, but it's content will be
    > overwritten.

* overwrite *and delete* the file as well:
    ```console
    $ shred -u filename.txt # -u or --remove
    ```

* overwrite, change the number of times a file is to be overwritten (e.g. 10 times), and delete the
  file:
    ```console
    $ shred -n 10 -u filename.txt # -n or --iterations
    ```

* overwrite *only the first bytes* (e.g. 5 first bytes), some specific bytes of text only:
    ```console
    $ shred -s 5 filename.txt # -s or --size
    ```
    > Note: after running this command `filename.txt` *will still exist*, and only the first 5
    > bytes will be overwritten.

    !!! Tip ""
        One can also the size in Kilo-bytes (K), Mega-bytes (M), or Giga-bytes (G), e.g. `$ shred
        -s 5M`.

* run `shred` in "verbose" mode, in order to print how many times the file is overwritten (and
  print when it is deleted if specifying `-u` / `--remove`):
    ```console
    $ shred -v filename.txt # -v or --verbose
    ```

* run `shred` in "force" mode, in order to change permissions to allow writing if necessary:
    ```console
    $ shred -f filename.txt # -f or --force
    ```

* overwrite, change the number of times a file is to be overwritten (e.g. 10 times), delete the
  file, and add final overwrite with zeros in order to hide shredding:
    ```console
    $ shred -n 10 -u -z filename.txt` # -z or --zero
    ```

* print basic details and version of shred command:
    ```console
    $ shred -v # -v or --version
    ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
