---
tags:
  - System Administration
  - Sysadmin
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# diffoscope

diffoscope will try to get to the bottom of what makes files or directories different. It will
recursively unpack archives of many kinds and transform various binary formats into more human
readable form to compare them. E.g. it can compare two tarballs, ISO images, or PDF just as easily.

???+ Note "Reference(s)"
    * <https://github.com/anthraxx/diffoscope>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

**TODO**

---
## Config

**TODO**

---
## Use

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
