---
tags:
  - System Administration
  - Sysadmin
  - Backup
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# restic

Restic is a backup program that aims to be fast, efficient and secure.

???+ Note "Reference(s)"
    * <https://restic.net/>
    * <https://github.com/restic/restic>
    * <https://restic.readthedocs.io/en/latest/>
    * <https://forum.restic.net/>
    * <https://wiki.archlinux.org/index.php/Cron#Cronie_2>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
    * [Complementary tools](#complementary-tools)
    * [Alternatives](#alternatives)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->


---
## Install

TODO

### Complementary tools

* [restic-browser](https://github.com/emuell/restic-browser) (a GUI to browse and restore restic
  backup repositories)
* [restic-automatic-backup-scheduler](https://github.com/erikw/restic-automatic-backup-scheduler)
  (automatic backups using restic through systemd timers with Backblaze B2 storage backend)

### Alternatives

* [rustic](https://github.com/rustic-rs/rustic)


---
## Config

TODO


---
## Use

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
