---
tags:
  - Boot Loader
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Grub

GRUB is a multi boot loader. It is derived from PUPA which was a research project to develop the
replacement of what is now known as GRUB Legacy. The latter had become too difficult to maintain
and GRUB was rewritten from scratch with the aim to provide modularity and portability. The current
GRUB is also referred to as GRUB `v2` while GRUB Legacy corresponds to `v0.9x`.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/GRUB>
    * <https://wiki.archlinux.org/title/GRUB/Tips_and_tricks>
    * <https://wiki.gentoo.org/wiki/GRUB2>
    * <https://wiki.gentoo.org/wiki/GRUB2_Quick_Start>
    * <https://en.wikipedia.org/wiki/GNU_GRUB>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
    * [Change grub wallpaper](#change-grub-wallpaper)
* [Use](#use)
    * [Themes](#themes)

<!-- vim-markdown-toc -->

---
## Install

TODO


---
## Config

TODO

### Change grub wallpaper

TODO


---
## Use

TODO

### Themes

<https://wiki.archlinux.org/title/GRUB/Tips_and_tricks#Theme>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
