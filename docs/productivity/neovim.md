---
tags:
  - Productivity
  - Text Editor
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Neovim

Neovim is a fork of Vim seeking to aggressively refactor Vim in order to simplify maintenance,
encourage contributions, split the work between multiple developers, enable advanced UIs without
modifications to the core, and to maximize extensibility.

???+ Note "Reference(s)"
    * <https://github.com/neovim/neovim>
    * <https://github.com/neovim/neovim/wiki/FAQ>
    * <https://wiki.archlinux.org/index.php/Neovim>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
    * [Install from source](#install-from-source)
* [Config](#config)
* [Use](#use)
    * [Troubleshooting](#troubleshooting)
        * [`E437: terminal capability "cm" required`](#e437-terminal-capability-cm-required)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        ```console
        # emerge -a app-editors/neovim
        ```

    === "pacman"
        ```console
        # pacman -S neovim
        ```

    === "apt"
        ```console
        # apt install neovim
        ```

    === "yum"
        ```console
        # yum install neovim
        ```

    === "dnf"
        ```console
        # dnf install neovim
        ```

### Install from source

See <https://github.com/neovim/neovim/wiki/Building-Neovim>.

Install the build prerequisites:

* <https://github.com/neovim/neovim/wiki/Building-Neovim#build-prerequisites>

Clone the project and checkout to the desired release (e.g. `stable` or `v0.4.3`):
```console
$ mkdir -p ~/apps/src-apps
$ cd ~/apps/src-apps
$ git clone https://github.com/neovim/neovim.git
$ cd neovim
$ git checkout v0.4.4 # e.g. checkout to v0.4.4
```

Build the project:
```console
$ make CMAKE_BUILD_TYPE=Release
$ sudo make insall
```

---
## Config

You can check my personal config [here](https://gitlab.com/stephane.tzvetkov/nvim-config).


---
## Use

There is no better place to learn how to use Neovim than the manual:
```console
$ nvim
    :help nvim
```

### Troubleshooting

#### `E437: terminal capability "cm" required`

If you get the bellow error when opening Neovim:
```console
E437: terminal capability "cm" required
```

Then you might want to prefix your Neovim command with `TERM=xterm`.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
