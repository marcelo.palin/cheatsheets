---
tags:
  - Data Bases
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Redis

Redis is a software project that implements data structure servers. It is open-source, networked,
in memory, and stores keys with optional durability.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Redis>
    * <https://redis.io/>
    * <https://redis.io/documentation>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
    * [Special config with `dhcpcd`](#special-config-with-dhcpcd)
* [Use](#use)
    * [Update procedure](#update-procedure)
    * [Troubleshooting](#troubleshooting)

<!-- vim-markdown-toc -->

---
## Install

Install Redis:

!!! Note ""

    === "emerge"
        ```console
        # emerge -a dev-db/redis
        ```

    === "pacman"
        ```console
        # pacman -S redis
        ```

    === "apt"
        ```console
        # apt install redis
        ```

    === "yum"
        ```console
        # yum install redis
        ```

    === "dnf"
        ```console
        # dnf install redis
        ```


---
## Config

Configure Redis:
```console
# vi /etc/redis.conf
    > ...
    >
    > bind 127.0.0.1 # make sure this line is not commented, unless you want
    >                # your Redis instance to listen to all interfaces
    > ...
    >
    > # Accept connections on the specified port, default is 6379 (IANA #815344).
    > # If port 0 is specified Redis will not listen on a TCP socket.
    > port 0
    >
    > ...
    >
    > # Unix socket.
    > #
    > # Specify the path for the Unix socket that will be used to listen for
    > # incoming connections. There is no default, so Redis will not listen
    > # on a unix socket when not specified.
    > #
    > # unixsocket /tmp/redis.sock
    > # unixsocketperm 700
    >
    > # Enable and update the Redis socket path:
    > unixsocket /run/redis/redis.sock
    > # Set permission to all members of the redis user group:
    > unixsocketperm 770
    > ...
```

Add Redis to the init system:

!!! Note ""

    === "OpenRC"
        ```console
        # rc-update add redis default
        ```

    === "Runit"
        ```console
        # ln -s /etc/runit/sv/redis /run/runit/service/
        ```

    === "SysVinit"
        ```console
        # chkconfig redis on
        ```

    === "SystemD"
        ```console
        # systemctl enable redis
        ```

Give Redis the good group membership:
```console
# usermod -a -G nginx redis # add redis to the nginx group
```

### Special config with `dhcpcd`

**If**, you are using `dhcpcd` instead of `netifrc`, **then** you need to config Redis accordingly:
```console
# vi /etc/conf.d/redis
    > ...
    > rc_need="net"

# vi /etc/conf.d/redis-sentinel
    > ...
    > rc_need="net"
```

And restart the Redis service:

!!! Note ""

    === "OpenRC"
        ```console
        # rc-service redis restart
        ```

    === "Runit"
        ```console
        # sv restart redis
        ```

    === "SysVinit"
        ```console
        # service redis restart
        ```

    === "SystemD"
        ```console
        # systemctl restart redis
        ```


---
## Use

* <https://redis.io/commands>

### Update procedure

**TODO**

### Troubleshooting

* <https://wiki.archlinux.org/index.php/Redis#Troubleshooting>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
