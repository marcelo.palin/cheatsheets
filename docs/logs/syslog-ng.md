---
tags:
  - Logs
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `syslog-ng`

`syslog-ng` is an enhanced log monitor, supporting a wide range of input and output methods:
syslog, unstructured text, message queues, databases (SQL and NoSQL alike), and more.

**TODO**

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Syslog-ng>
    * <https://wiki.gentoo.org/wiki/Syslog-ng>
    * <https://www.syslog-ng.com/products/open-source-log-management/>
    * <https://github.com/syslog-ng/syslog-ng>
    * <https://www.syslog-ng.com/technical-documents/list/syslog-ng-open-source-edition/3.26>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Optional: `logrotate`](#optional-logrotate)

<!-- vim-markdown-toc -->

---
## Install

Install `syslog-ng`:

!!! Note ""

    === "emerge"
        ```console
        $ emerge -a app-admin/syslog-ng
        ```

    === "pacman"
        ```console
        # pacman -S syslog-ng
        ```

    === "apt"
        ```console
        # apt install syslog-ng
        ```

    === "yum"
        ```console
        # yum install syslog-ng
        ```

    === "dnf"
        ```console
        # dnf install syslog-ng
        ```

Then add it to your init system and start it:

!!! Note ""

    === "OpenRC"
        ```console
        # /etc/init.d/syslog-ng start
        # rc-update add syslog-ng default
        ```

    === "Runit"
        ```console
        # ln -s /etc/runit/sv/syslog-ng /run/runit/service
        # sv start syslog-ng
        ```

    === "SysVinit"
        ```console
        # service syslog-ng start
        # chkconfig syslog-ng on
        ```

    === "SystemD"
        ```console
        # systemctl start syslog-ng
        # systemctl enable syslog-ng
        ```


---
## Config

Default config should be fine.

TODO


---
## Use

TODO

### Optional: `logrotate`

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
