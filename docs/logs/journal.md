---
tags:
  - Logs
  - SystemD
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `journal`

SystemD has its own logging system called the `journal`.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Systemd/Journal>
    * <https://www.youtube.com/watch?v=AXfboZnKtgg>
    * <https://materials.rangeforce.com/tutorial/2020/04/16/Systemd-Journal/>
    * <https://serverfault.com/questions/758244/how-to-configure-systemd-journal-remote>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**

* Look for errors in the log files located at `/var/log`, as well as high priority errors in the
  SystemD `journal`:
```console
# journalctl -p 3 -xb
```

```console
# journalctl -xef
```

```console
# journalctl -xelu service_name
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
