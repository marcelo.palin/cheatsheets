---
tags:
  - Logs
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `rsyslog`

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Rsyslog>
    * <https://wiki.archlinux.org/index.php/Rsyslog>
    * <https://www.rsyslog.com/doc/master/index.html>
    * <https://www.rsyslog.com/doc/v8-stable/>
    * <https://www.rsyslog.com/doc/v8-stable/configuration/templates.html>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
