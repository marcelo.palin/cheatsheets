---
tags:
  - Virtualization
  - Containers
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Proxmox

🚧 WIP/TODO 🚧

Recommended hardware:
    - `Intel VT` / `AMD-V` BIOS options
    - 16 GB RAM
    - Hardware RAID with batteries protected write cache (BBU) or flash protection like Adaptec
      Zero Maintenance Cache Protection (Software RAID is not recommended)
    - Fast hard drives, best results with fast SAS disks in Raid 10 configuration (do not install
      Proxmox on a flash card, a USB drive, SD card, etc)
    - 2 GB NIC (for bonding), additional NIC's depending on the preferred storage technology and
      cluster setup (10 GB also supported)

BIOS tuning:
    - Enable Intel-VT / AMD-V
    - Disable dynamic CPU frequencies (Intel turbo boost, AMD Cool'n'Quiet, ...)
    - Disable dynamic cores shutdown in case of inactivity (C1E, C-States,...)
    - Disable dynamic memory frequencies
    - Governor: performance

Network config:
    - Use a dedicated network for cluster communication (use bonding)

VM limits in Proxmox:
    - 4 TB RAM
    - 512 CPU
    - 32 NIC
    - 39 Virtual disks
        - 16 virtio
        - 6 SATA
        - 4 ide
        - 14 scsi

Full virtualization vs. paravirtualization:

- In full virtualization, the guest operating system runs on top of a hypervisor that sits on the
  bare metal. The guest is unaware that it is being virtualized and requires no changes to work in
  this configuration. Conversely, in paravirtualization, the guest operating system is not only
  aware that it is running on a hypervisor but includes code to make guest to hypervisor
  transitions more efficient. In the full virtualization scheme, the hypervisor must emulate device
  hardware, which is emulating at the lowest level of the conversation (for example, to a network
  driver). Although the emulation is clean at this abstraction, it's also the most inefficient and
  highly complicated. In the paravirtualization scheme, the guest and the hypervisor can work
  cooperatively to make this emulation efficient. The downside to the paravirtualization approach
  is that the operating system is aware that it's being virtualized and requires modifications to
  work.

Installation (e.g. with installer `v7.2`):
    - Get PVE (Proxmox Virtual Environment) installer from <https://proxmox.com>
    - Install it on installation media (e.g. USB key)
    - Boot with UEFI
    - Select "Install Proxmox"
    - Agree with the license (`GPLv3`)
    - Select the hard disk where to install PVE, then select the "Options" and the file system you
      want to use
        - E.g. EXT4 or XFS
            - `maxroot`: 20 to 30 GB is good (the installation takes ~5 GB)
            - `swap size:` at least 1 GB (up to 8 GB is enough)
            - `minfree` and `maxvz` left blank for default settings
    - setup a password and email (for backup notifications)
    - setup your network config
    - finally click "Install"

Updates:
- Select server (below "Data Center") -> Updates -> Refresh and Upgrade


Misc:
- Do not mix Intel and AMD based servers in the same cluster (it would go wrong during live
  migrations)
- Be careful with the configured number of cores per VM created, it must fit the available number
  of cores of the host (this must be also true during live migrations)
- Always use "virtio" for network (fastest available)
- Each VM use a single dedicated process
- Don't forget to enable the "discard" option, proxmox side **and** VM side.
- `/etc/pve` : 30MB monted space storage, common to all members of a cluster.
- No more than 20 nodes per cluster

Proxmox services:

- pvedaemon :
    - is the REST API
- pve-cluster :
    - service utlisé pour monter /etc/pve ?
- pvestatd:
    - is the PVE Status Daemon. It queries the status of all resources (VMs, Cnotainers and
      Storage), and sends the result to all cluster members.
    - you can easily query the resultng data with: `cat /etc/pve/.rrd`

- pve-manager / pve-guest:
    - a startup script (not a daemon) used to start/stop all VMs and Containers

- spice-proxy
    - handle spice clients connections and forward them to virtual machines
- HA services:
    - crm-manager
    - lrm-manager
    - fencing
- Standard services:
    - cron (commands scheduling)
    - ntp (time network protocol daemon)
    - rsyslog (message logging)
    - postfix (smtp mail verser)

---

ZFS:

- at **least** 2GB + 1GB / TB of dedicated RAM for ZFS

- RAIDZ 1 = RAID 5
- RAIDZ 2 = RAID 6

- Pas de RAID hardware derrière ZFS (ni CEPH d'ailleurs)

---

CEPH:

4GB de RAM per SSD disk
1 or 2GB RAM per HDD disk

- Pas de RAID hardware derrière CEPH
---

- quel service est utlisé pour monter /etc/pve ?
    - pve-cluster (même si on est sur un simple noeud sans être en cluster)

- quel service est utilisé comme bus de communication entre les noeuds ?
    - corosync

- dans une VM, peut-on modifier la mémoire à chaud ?
    - oui, en ajout uniquement sur Linux (pas encore possible en suppression, bientôt le cas)

- dans une VM, peut on modifier la taille du disque à chaud ?
    - oui, en ajout uniquement (sur Windows aussi)

- Peut on migrer à chaud une VM entre 2 versions majeures de Proxmox ?
    - oui, uniquement du plus ancien vers le plus récent

- Peut-on migrer à chaud une VM entre 2 serveurs avec des CPU de différentes générations ?
    - oui, en configurant le cpumodel de la VM sur le cpu le plus ancien

- Faut-il redémarrer le serveur pour appliquer les mises à jour ?
    - oui, uniquement lors d'une MaJ kernel

- Peut on faire des migrations à chaud de containers ?
    - non

- Quelle est la source de données réelle du répertoire /etc/pve ?
    - /var/lib/pve-cluster/config.db (disponible sur chaque noeud, mais contient la config de
      l'ensemble du cluster)

- Sans utiliser la HA, comment déplacer une VM sur un autre nœud, lorsque le nœud originel est en
  panne ?
    - en ligne de commande : mv /etc/pve/nodes/deadnode/qemu-server/vmid.conf
      /etc/pve/nodes/targetnode/qemu-server/vmid.conf

- Réseau : Quel type de bridge dois-je choisir si je veux passer un vlan en mode "access" (1 seul
  vlan) dans une VM ?
    - les deux, au choix (vlan-aware bridge et non vlan-aware bridge)

- Réseau : Quel type de bridge dois-je choisir si je veux passer un vlan en mode "trunk" (plusieurs
  vlans) dans une VM ?
    - vlan-aware bridge

- Peut-on migrer une VM à chaud entre 2 noeuds avec un stockage local ?
    - oui

- Firewall : Sur quoi s'appliquent les règles firewall configurées au niveau datacenter ?
    - les ip de management des noeuds Proxmox

- Firewall : a quoi servent les security groups ?
    - groupe de règles

- Réseau : bonding : quel type de bond permet d'utiliser plusieurs interfaces en parallèle de
  manière stable ?
    - lacp

- Quel type de format de disques de VM, venant d'autres hyperviseurs, supporte l'outil d'import de
  Proxmox ?
    - n'importe quel format

- Quel type de controleur disque de VM est le plus rapide ?
    - virtio


- Sur un cluster 4 nœuds, quel est le nombre minimum de serveurs opérationnels pour avoir le quorum ?
    - 3

- Quel service pilote la HA du cluster ?
    - pve-ha-crm

- Si je veux mettre en place un cluster multi sites, combien de sites au minimum sont nécessaires ?
    - 3

- Quel type de replication utilise ceph ?
    - synchrone (d'ailleurs ceph est sensible aux latences)

- quel type de replication utilise zfs ?
    - asynchrone

- Dans un cluster ceph, quel service est utilisé pour gérer le quorum ?
    - mon (moniteur, et pas mgr le manageur)

- Dans un cluster ceph, peut-on réduire la taille du cluster à chaud en supprimant des disques ?
    - oui

- Peut-on mettre à jour ceph sans interruption de service ?
    - oui

- Si un nœud perd le quorum, quel est le comportement de /etc/pve ?
    - lecture seule


- Quel est le comportement d'un serveur qui perd le réseau, lorsque la HA est activée sur une VM
  qui tourne sur ce serveur ?
    - le serveur s'éteint et la VM est redémarrée sur un autre nœud

- Que se passe-t-il si j'arrête une VM, via l'os de la VM (#shutdown -h), et que la HA est activée
  sur la VM ?
    - la HA redémarre la VM

- Les backups des VM en mode snapshot, utilisent la fonctionnalité snapshot du stockage ?
    - non

- Les backups des CT en mode snapshot, utilisent la fonctionnalité snapshot du stockage ?
    - oui

- Quel est le type de backup supporté lorsque l'on utilise Proxmox backup server pour les sauvegardes ?
    - full + incremental

create ceph OSD : DB disk -> mettre un disque SSD ici pour agir comme un cache de controleur PERC (pour accélérer les écriture)


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
