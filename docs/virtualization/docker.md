---
tags:
  - Virtualization
  - Containers
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Docker

Docker is a utility to pack, ship and run any application as a lightweight container.

!!! Note "Info"
    * See [Containers vs. Virtual Machines (VMs): What’s the Difference?](https://web.archive.org/web/20220510161213/https://www.ibm.com/cloud/blog/containers-vs-vms)

???+ Note "Reference(s)"
    * <https://docs.docker.com/get-started/overview/>
    * <https://docs.docker.com/>
    * <https://www.docker.com/>
    * <https://hub.docker.com/>
    * <https://wiki.gentoo.org/wiki/Docker>
    * <https://wiki.archlinux.org/index.php/Docker>
    * <https://blog.microlinux.fr/formation-docker/>
    * <https://stackoverflow.com/questions/39223249/multiple-run-vs-single-chained-run-in-dockerfile-which-is-better>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Docker Compose](#docker-compose)
    * [Docker scan](#docker-scan)
    * [Buildx](#buildx)
    * [Troubleshooting](#troubleshooting)
        * [error `pair interfaces: operation not supported`](#error-pair-interfaces-operation-not-supported)
        * [warning `/lib/rc/sh/openrc-run.sh: 258: ulimit: too many arguments`](#warning-librcshopenrc-runsh-258-ulimit-too-many-arguments)

<!-- vim-markdown-toc -->

---
## Install

!!! Warning ""
    Containers that produce kernel panics will induce kernel panics into the host operating system.

!!! Note ""

    === "Gentoo kernel"
        A correct [kernel config](../distros/gentoo-based/gentoo_kernel.md#kernel-config) is
        needed: See: <https://wiki.gentoo.org/wiki/Docker#Kernel>

        !!! Warning "Warning"
            After configuring the kernel don't forget to do a [kernel make and
            rebuild](..//distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!


!!! Note ""

    === "apk"
        ```console
        # apt add docker
        ```

    === "apt"
        ```console
        # apt install docker.io
        ```

    === "dnf"
        ```console
        # rpm --import https://download.docker.com/linux/centos/gpg
        # vi /etc/yum.repos.d/docker-ce.repo
            > [docker]
            > name=Docker
            > baseurl=https://download.docker.com/linux/centos/$releasever/$basearch/stable
            > enabled=1
            > gpgcheck=1
            > gpgkey=https://download.docker.com/linux/centos/gpg
        # dnf install -y docker-ce

        # dnf install docker-compose
        ```

    === "emerge"
        ```console
        $ sudo emerge -a app-containers/docker
        $ sudo emerge -a app-emulation/docker-compose
        $ /usr/share/docker/contrib/check-config.sh
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.docker
            # nix-env -iA nixos.docker-compose
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.docker
            # nix-env -iA nixpkgs.docker-compose
            ```

    === "pacman"
        ```console
        # pacman -S docker docker-compose
        ```

        !!! Tip "For Artix users"
            * **If** using `dinit`:
            ```console
            # pacman -S docker docker-compose docker-dinit
            ```
            * **If** using `openrc`:
            ```console
            # pacman -S docker docker-compose docker-openrc
            ```
            * **If** using `runit`:
            ```console
            # pacman -S docker docker-compose docker-runit
            ```
            * **If** using `s6`:
            ```console
            # pacman -S docker docker-compose docker-s6
            ```

    === "yum"
        ```console
        # yum install docker
        ```

    === "xbps"
        ```console
        # xbps-install -S docker
        ```

    === "zypper"
        ```console
        # zypper install docker
        ```

Then, add it to your init system and start it:

!!! Note ""

    === "OpenRC"
        ```console
        # rc-update add docker default
        # rc-service docker start
        ```

    === "Runit"
        Depending on your `runit` implementation, either run:
        ```console
        # ln -s /etc/runit/sv/docker /service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/docker /var/service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/docker /run/runit/service
        ```
        In any case, finally run:
        ```console
        # sv up docker
        ```

    === "SysVinit"
        ```console
        # service docker start
        # chkconfig docker on
        ```

    === "SystemD"
        ```console
        # systemctl enable docker
        # systemctl start docker
        ```

Finally, check with `# docker info` that everything is fine.

---
## Config

* **TODO** : proxy...

* Allow docker to be run without `sudo` (see <https://askubuntu.com/a/477554>):

    ```console
    $ sudo groupadd docker
    $ sudo gpasswd -a $USER docker
    $ newgrp docker
    ```

    ⚠️ Be careful with the associated security risks:
    <https://docs.docker.com/engine/security/#docker-daemon-attack-surface>

* Active experimental features of docker, by creating the file `/etc/docker/daemon.json` with the
  following content:

    ```console
    {"experimental":true}
    ```

* Change Docker root directory `/var/lib/docker` to another location (e.g. `/new/path/docker`):

!!! Note ""

    === "OpenRC"
        **TODO**

    === "Runit"
        Most distros using Runit won't store *active* services in the same directory. So let's
        define a `$ACTIVE_RUNIT_SERVICE_DIR` environment variable holding the path to that
        directory. Most common paths are:

        * `/service/`
        * `/var/service/` (e.g. for Void Linux)
        * `/etc/service/`
        * `/run/runit/service/` (e.g. for Artix Linux)

        ```console
        $ sudo sv stop docker

        $ sudo vi $ACTIVE_RUNIT_SERVICE_DIR/docker/run
            > ...
          ~ > exec chpst -o 1048576 -p 1048576 dockerd --data-root="/new/path/docker"

        $ sudo mkdir -p /new/path/docker
        $ sudo rsync -aqxP /var/lib/docker/ /new/path/docker

        $ sudo sv start docker

        $ ps aux | grep dockerd | grep -v grep
        ```

    === "SysVinit"
        **TODO**

    === "SystemD"
        ```console
        $ sudo systemctl stop docker.service
        $ sudo systemctl stop docker.socket

        $ sudo vi /lib/systemd/system/docker.service
            > ...
          ~ > ExecStart=/usr/bin/dockerd --data-root="/new/path/docker" -H fd://
            > ...

        $ sudo mkdir -p /new/path/docker
        $ sudo rsync -aqxP /var/lib/docker/ /new/path/docker

        $ sudo systemctl daemon-reload
        $ sudo systemctl start dockerd

        $ ps aux | grep dockerd | grep -v grep
        ```

* It is possible to use a Btrfs or ZFS (or etc) storage driver for Docker, see
  <https://wiki.archlinux.org/title/Docker#Storage_driver> and
  <https://wiki.gentoo.org/wiki/Docker#Storage_driver>.

---
## Use

* Print help:

    ```console
    $ docker --help

        Common Commands:
          run         Create and run a new container from an image
          exec        Execute a command in a running container
          ps          List containers
          build       Build an image from a Dockerfile
          pull        Download an image from a registry
          push        Upload an image to a registry
          images      List images
          login       Log in to a registry
          logout      Log out from a registry
          search      Search Docker Hub for images
          version     Show the Docker version information
          info        Display system-wide information
        
        Other Commands:
          attach      Attach local standard input, output, and error streams to a running container
          commit      Create a new image from a container's changes
          cp          Copy files/folders between a container and the local filesystem
          create      Create a new container
          diff        Inspect changes to files or directories on a container's filesystem
          events      Get real time events from the server
          export      Export a container's filesystem as a tar archive
          history     Show the history of an image
          import      Import the contents from a tarball to create a filesystem image
          inspect     Return low-level information on Docker objects
          kill        Kill one or more running containers
          load        Load an image from a tar archive or STDIN
          logs        Fetch the logs of a container
          pause       Pause all processes within one or more containers
          port        List port mappings or a specific mapping for the container
          rename      Rename a container
          restart     Restart one or more containers
          rm          Remove one or more containers
          rmi         Remove one or more images
          save        Save one or more images to a tar archive (streamed to STDOUT by default)
          start       Start one or more stopped containers
          stats       Display a live stream of container(s) resource usage statistics
          stop        Stop one or more running containers
          tag         Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE
          top         Display the running processes of a container
          unpause     Unpause all processes within one or more containers
          update      Update configuration of one or more containers
          wait        Block until one or more containers stop, then print their exit codes
        
        Management Commands:
          builder     Manage builds
          checkpoint  Manage checkpoints
          compose*    Docker Compose (Docker Inc., 2.19.1)
          container   Manage containers
          context     Manage contexts
          image       Manage images
          manifest    Manage Docker image manifests and manifest lists
          network     Manage networks
          plugin      Manage plugins
          system      Manage Docker
          trust       Manage trust on Docker images
          volume      Manage volumes
        
        Swarm Commands:
          swarm       Manage Swarm

        ...
    ```

* Create and run a container with an interactive (`-i`, `--interactive`) Bash shell (i.e. keeping
  stdin open even if not attached):

    ```console
    $ sudo docker run --interactive <image_name> bash
    ```

    * E.g.

    ```console
    $ docker run --interactive --name ubuntu-bash ubuntu bash
    $ docker run --interactive --name rocky-bash rockylinux bash
    $ docker run --interactive --name debian-bash debian bash
    ```

* Create and run a container with an interactive Bash shell, assigning a name (`--name`) to the
  container (easier to use/read than a container ID):

    ```console
    $ sudo docker run --interactive --name rocky-bash rockylinux bash
    ```

* Create and run a container with an interactive Bash shell, allocating a pseudo-tty (`-t`,
  `--tty`) and auto removing (`--rm`) the container when done executing:

    ```console
    $ sudo docker run --interactive --tty --rm rockylinux bash
    ```

* Create and run a container in detach (`-d`, `--detach`) mode (i.e. in the background), with an
  interactive Bash shell, and with a pseudo-tty:

    ```console
    $ sudo docker run -dit rockylinux bash
    ```

* Reattach to a detached running container:

    ```console
    $ sudo docker attach <container_name_or_id>
    ```

* "shell" into a running container (`docker-1.3+`), better than `docker attach` because exiting
  won't stop the container:

    ```console
    $ sudo docker exec -it <container_name_or_id> bash
    ```

* Stop a running container:

    ```console
    $ sudo docker stop <container_name_or_id>
    ```

* Start a stoped container:

    ```console
    $ sudo docker start <container_name_or_id>
    ```

* Inspect a running container:

    ```console
    $ sudo docker inspect <container_name_or_id>
    ```

* Get the process ID for a container:

    ```console
    $ sudo docker inspect --format {{.State.Pid}} <container_name_or_id>
    ```

* List the current mounted volumes for a container (and pretty print):

    ```console
    $ sudo docker inspect --format='{{json .Volumes}}' <container_id> | python -mjson.tool
    ```

* Copy files/folders between a container and your host:

    ```console
    $ sudo docker cp foo.txt mycontainer:/foo.txt
    ```

* List currently running containers:

    ```console
    $ sudo docker ps
    ```

* List all containers:

    ```console
    $ sudo docker ps -a
    ```

* Pull down an image (e.g. the latest Rocky Linux image):

    ```console
    $ sudo docker pull rockylinux
    ```

* List all images:

    ```console
    $ sudo docker images
    ```

* Remove a docker container:

    ```console
    $ sudo docker image rm <container_name>
    ```

* Remove a docker image (⚠️ check if a container is associated to it beforehand with `$ docker ps -a`):

    ```console
    $ sudo docker image rmi <image_name>
    ```

* Display system-wide information:

    ```console
    $ sudo docker system info
    ```

* Remove unused data:

    ```console
    $ sudo docker system prune
    ```

* Show docker disk usage:

    ```console
    $ sudo docker system df
    ```

* Remove all stopped containers, dangling images, and unused networks:

    ```console
    $ sudo docker system prune
    ```
    If you want to remove all unused images not just the dangling ones, add the `-a` (`--all`)
    option to the command.

* List all docker networks:

    ```console
    $ docker network ls
    ```

### Docker Compose

🚧 WIP

See <https://docs.linuxserver.io/general/docker-compose>.

Docker Compose is an alternate CLI front-end for the Docker Engine, which specifies properties of
containers using a `docker-compose.yml` YAML file rather than, for example, a script with docker
run options. This is useful for setting up services that are use often and/or have complex
configurations.

```console
$ sudo mkdir /etc/docker/compose/
```

```console
$ docker compose -f /etc/docker-compose/service_name.yml up -d
```

```console
$ docker compose -f /etc/docker-compose/service_name.yml down
```

```console
[Unit]
Description=service_name with docker compose
PartOf=docker.service
After=docker.service

[Service]
Type=oneshot
RemainAfterExit=true
WorkingDirectory=/etc/docker/compose/
ExecStart=/usr/bin/docker-compose -f service_name.yml up -d --remove-orphans
ExecStop=/usr/bin/docker-compose -f service_name.yml down

[Install]
WantedBy=multi-user.target
```

Shell access whilst the container is running:

```console
$ docker exec -it service_name /bin/bash
```

To monitor the logs of the container in real-time:

```console
$ docker logs -f service_name
```

Container version number

```console
$ docker inspect -f '{{ index .Config.Labels "build_version" }}' service_name
```

Image version number

```console
$ docker inspect -f '{{ index .Config.Labels "build_version" }}' lscr.io/linuxserver/service_name:latest
```


### Docker scan

TODO <https://github.com/docker/scan-cli-plugin>

### Buildx

<https://docs.docker.com/desktop/multi-arch/>

### Troubleshooting

#### error `pair interfaces: operation not supported`

If you get an error like this one:
```console
docker: Error response from daemon: failed to create endpoint cranky_einstein on network bridge: failed to add the host (vethf55744a) <=> sandbox (vethfce3f4d) pair interfaces: operation not supported.
```

Maybe you just did a Linux Kernel update, in that case: just restart the computer

#### warning `/lib/rc/sh/openrc-run.sh: 258: ulimit: too many arguments`

Using OpenRC, with `/bin/sh` as a symlink to `/bin/dash`, a warning is issued every time Docker is
started or stopped:
```console
/lib/rc/sh/openrc-run.sh: 258: ulimit: too many arguments
 * docker: unable to apply RC_ULIMIT settings
 * Stopping docker ...
```

See <https://github.com/moby/moby/issues/43370>.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
