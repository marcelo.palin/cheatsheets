---
tags:
  - Programming Languages
  - C
  - C++
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# C C++

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [GCC](#gcc)
* [Clang](#clang)
* [`gcovr`](#gcovr)

<!-- vim-markdown-toc -->

---
## GCC

???+ Note "Reference(s)"
    * <https://hackingcpp.com/cpp/cheat_sheets.html>
    * <https://hackingcpp.com/index.html>
    * <https://hackingcpp.com/cpp/community.html>
    * <https://www.youtube.com/watch?v=AMkAKlDI_Gw>
    * <https://learnxinyminutes.com/docs/c++/>
    * <https://stroustrup.com/4th.html>
    * <https://stroustrup.com/Tour.html>
    * <https://gcc.gnu.org/onlinedocs/>
    * <https://gcc.gnu.org/onlinedocs/gcc-11.2.0/gcc/>
    * <https://gcc.gnu.org/onlinedocs/gcc-11.2.0/gcc/Invoking-GCC.html#Invoking-GCC>
    * <https://gcc.gnu.org/onlinedocs/gcc-11.2.0/gcc/C-Dialect-Options.html#C-Dialect-Options>
    * <https://gcc.gnu.org/onlinedocs/gcc-11.2.0/gcc/C_002b_002b-Dialect-Options.html#C_002b_002b-Dialect-Options>
    * <https://www.rapidtables.com/code/linux/gcc.html>

When using GNU make With GCC (maybe also with Clang?), some flags can be passed, e.g.:
```console
$ make clean && make CXXFLAGS="-fpermissive -fPIC"
```

E.g. some useful options:

* See `-fpermissive`: <https://gcc.gnu.org/onlinedocs/gcc-11.2.0/gcc/C_002b_002b-Dialect-Options.html#index-fpermissive>
* See `-fPIC`: <https://wiki.gentoo.org/wiki/Project:AMD64/Fixing_-fPIC_Errors_Guide>
* See `-Wall`: <https://www.rapidtables.com/code/linux/gcc/gcc-wall.html>
* See `-Wextra`: <https://www.rapidtables.com/code/linux/gcc.html>
* See `-O`: <https://www.rapidtables.com/code/linux/gcc/gcc-o.html#optimization>

Also, different compiling standards (`-std=`) can be used, e.g.:

* `$ make clean && make CFLAGS="-std=gnu90 -fPIC" CXXFLAGS="-std=c++14 -fpermissive -fPIC"`

!!! Note ""
    Here are the available standards:

    * `c++98` or `c++03` for `ISO C++ 1998 with amendments` standard
    * `gnu++98` or `gnu++03` for `ISO C++ 1998 with amendments and GNU extensions` standard
    * `c++11` for `ISO C++ 2011 with amendments` standard
    * `gnu++11` for `ISO C++ 2011 with amendments and GNU extensions` standard
    * `c++14` for `ISO C++ 2014 with amendments` standard
    * `gnu++14` for `ISO C++ 2014 with amendments and GNU extensions` standard
    * `c++17` for `ISO C++ 2017 with amendments` standard
    * `gnu++17` for `ISO C++ 2017 with amendments and GNU extensions` standard
    * `c++20` for `ISO C++ 2020 DIS` standard
    * `gnu++20` for `ISO C++ 2020 DIS with GNU extensions` standard
    * `c89`, `c90`, or `iso9899:1990` for `ISO C 1990` standard
    * `iso9899:199409` for `ISO C 1990 with amendment 1` standard
    * `gnu89` or `gnu90` for `ISO C 1990 with GNU extensions` standard
    * `c99` or `iso9899:1999` for `ISO C 1999` standard
    * `gnu99` for `ISO C 1999 with GNU extensions` standard
    * `c11` or `iso9899:2011` for `ISO C 2011` standard
    * `gnu11` for `ISO C 2011 with GNU extensions` standard
    * `c17`, `iso9899:2017`, `c18`, or `iso9899:2018` for `ISO C 2017` standard
    * `gnu17` or `gnu18` for `ISO C 2017 with GNU extensions` standard
    * `c2x` for `Working Draft for ISO C2x` standard
    * `gnu2x` for `Working Draft for ISO C2x with GNU extensions` standard

---
## Clang

Clang is a "LLVM native" C/C++/Objective-C compiler using LLVM as a back end and optimizer. It aims
to be GCC compatible yet stricter, offers fast compile times with low memory usage, and has useful
error and warning messages for easier compile troubleshooting.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Clang>
    * <https://wiki.archlinux.org/index.php/Clang>

**TODO**

`make CC=/usr/bin/clang CXX=/usr/bin/clang++`
`cmake . -DCMAKE_CXX_COMPILER=/usr/bin/clang++ -DCMAKE_CC_COMPILER=/usr/bin/clang`

---
## `gcovr`

Code coverage:

* <https://gcovr.com/en/stable/guide.html>
* <https://stackoverflow.com/questions/37957583/how-to-use-gcov-with-cmake>
* <https://jhbell.com/using-cmake-and-gcov>
* ? `$ make clean && make CFLAGS="-fprofile-arcs -ftest-coverage -fPIC -O0"` with GCC only


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
