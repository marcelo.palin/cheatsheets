---
tags:
  - Programming Languages
  - LaTeX
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# LaTeX

???+ Note "Reference(s)"
    * <https://en.wikibooks.org/wiki/LaTeX>
    * <https://tobi.oetiker.ch/lshort/lshort.pdf>
    * <https://www.andy-roberts.net/writing/latex>
    * <https://www.texfaq.org/>
    * <https://tug.org/texlive/doc.html>
    * <https://tug.org/texlive/doc/texlive-en/texlive-en.html>
    * <https://wiki.archlinux.org/index.php/TeX_Live#Usage>
    * <https://wiki.archlinux.org/index.php/TeX_Live/FAQ>
    * <https://www.archlinux.org/groups/x86_64/texlive-most/>
    * <https://tex.stackexchange.com/>
    * <https://detexify.kirelabs.org/classify.html>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**

```console
# pacman -S texlive-most
```

then

```console
$ pdflatex file.tex
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
