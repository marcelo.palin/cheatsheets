---
tags:
  - Programming Languages
  - JavaScript
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# javascript

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Node.js>
    * <https://nodejs.org/en/>
    * <https://github.com/nodejs/node>
    * <https://github.com/nvm-sh/nvm>
    * <https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/JavaScript_basics>
    * <https://developer.mozilla.org/en-US/docs/Learn/JavaScript>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Avoid dotfile madness](#avoid-dotfile-madness)
* [Install Node.js and Npm](#install-nodejs-and-npm)

<!-- vim-markdown-toc -->

---
## Avoid dotfile madness

Prior to installation, [make sure you stay in control of your home
directory](https://web.archive.org/web/20210807080152/https://0x46.net/thoughts/2019/02/01/dotfile-madness/).

!!! Warning "Prerequisite"
    * [XDG](../shells/xdg.md)

See [how to handle javascript related
dotfiles](../admin/avoid_dotfile_madness.md#npm-and-nodejs-npm-npmrc-and-node_repl_history).

---
## Install Node.js and Npm

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
