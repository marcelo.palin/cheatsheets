---
tags:
  - Programming Languages
  - Lua
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Lua

Lua is a efficient, lightweight, embeddable scripting language. It supports procedural programming,
object-oriented programming, functional programming, data-driven programming, and data description.

Lua combines simple procedural syntax with data description constructs based on associative arrays
and extensible semantics. Lua is dynamically typed, runs by interpreting bytecode with a
register-based virtual machine, and has automatic memory management with incremental garbage
collection, making it a good choice for configuration, scripting, and rapid prototyping.

???+ Note "Reference(s)"
    * <https://www.lua.org/>
    * <https://www.lua.org/about.html>
    * <https://wiki.archlinux.org/title/Lua>
    * <https://wiki.gentoo.org/wiki/Lua>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

**TODO**

---
## Config

**TODO**

---
## Use

**TODO**

**WIP**

```console
$ /bin/ls -la /usr/bin/lua*
-rwxr-xr-x 1 root root  284608 Jan 26 23:30 /usr/bin/lua
-rwxr-xr-x 1 root root  202688 Aug 29  2021 /usr/bin/lua5.1
-rwxr-xr-x 1 root root  223064 Aug 23  2020 /usr/bin/lua5.2
-rwxr-xr-x 1 root root  247640 Sep 28  2020 /usr/bin/lua5.3
lrwxrwxrwx 1 root root      12 Jan 26 23:30 /usr/bin/lua5.4 -> /usr/bin/lua
-rwxr-xr-x 1 root root  198616 Jan 26 23:30 /usr/bin/luac
-rwxr-xr-x 1 root root  141272 Aug 29  2021 /usr/bin/luac5.1
-rwxr-xr-x 1 root root  149360 Aug 23  2020 /usr/bin/luac5.2
-rwxr-xr-x 1 root root  165744 Sep 28  2020 /usr/bin/luac5.3
lrwxrwxrwx 1 root root      13 Jan 26 23:30 /usr/bin/luac5.4 -> /usr/bin/luac
lrwxrwxrwx 1 root root      57 Apr 17 19:11 /usr/bin/luafindfont -> /usr/share/texmf-dist/scripts/luafindfont/luafindfont.lua
-rwxr-xr-x 1 root root 7425696 Apr 17 19:03 /usr/bin/luahbtex
lrwxrwxrwx 1 root root      18 May  3 17:54 /usr/bin/luajit -> luajit-2.1.0-beta3
-rwxr-xr-x 1 root root   22480 May  3 17:54 /usr/bin/luajit-2.1.0-beta3
-rwxr-xr-x 1 root root 7298784 Apr 17 19:03 /usr/bin/luajithbtex
-rwxr-xr-x 1 root root 7242488 Apr 17 19:03 /usr/bin/luajittex
lrwxrwxrwx 1 root root       8 Apr 17 19:03 /usr/bin/lualatex -> luahbtex
lrwxrwxrwx 1 root root      60 Apr 17 19:11 /usr/bin/luaotfload-tool -> /usr/share/texmf-dist/scripts/luaotfload/luaotfload-tool.lua
-rwxr-xr-x 1 root root 7365216 Apr 17 19:03 /usr/bin/luatex
lrwxrwxrwx 1 root root      57 Apr 17 19:11 /usr/bin/luatools -> /usr/share/texmf-dist/scripts/context/stubs/unix/luatools

$ lua -v
Lua 5.4.4  Copyright (C) 1994-2022 Lua.org, PUC-Rio

$ sudo ln -sf /usr/bin/lua5.1 /usr/local/bin/lua
```

Now in a new terminal:
```console
$ lua -v
Lua 5.1.5  Copyright (C) 1994-2012 Lua.org, PUC-Rio
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
