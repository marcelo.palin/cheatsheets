---
tags:
  - Programming Languages
  - Maven
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Maven

Maven, a Yiddish word meaning accumulator of knowledge, is a java build system. Maven specifies a
standard way to build projects, a clear definition of what the project consisted of, an easy way to
publish project information, and a way to share JARs across several projects.

Maven is a tool that can be used for building and managing any Java based project.

???+ Note "Reference(s)"
    * <https://maven.apache.org/>
    * <https://repology.org/project/maven/versions>
    * <https://wiki.gentoo.org/wiki/Maven>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Avoid dotfile madness](#avoid-dotfile-madness)
* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->


---
## Avoid dotfile madness


Prior to installation, [make sure you stay in control of your home
directory](https://web.archive.org/web/20210807080152/https://0x46.net/thoughts/2019/02/01/dotfile-madness/).

!!! Warning "Prerequisite(s)"
    * [XDG](../../shells/xdg.md)

See [how to handle Maven related dotfiles](../../admin/avoid_dotfile_madness.md#maven-m2).


---
## Install

!!! Note ""

    === "emerge"
        ```console
        # emerge -a dev-java/maven-bin
        ```

    === "pacman"
        ```console
        # pacman -S maven
        ```

    === "apt"
        ```console
        # apt install maven
        ```

    === "yum"
        ```console
        # yum install maven
        ```

    === "dnf"
        ```console
        # dnf install maven
        ```


---
## Config

WIP


---
## Use

WIP

* `mvn clean install`


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
