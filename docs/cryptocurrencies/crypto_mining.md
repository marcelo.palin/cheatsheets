---
tags:
  - Crypto
  - Cryptocurrencies
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Crypto mining...

**TODO**

!!! Warning "Warning"
    This page is still a **work in progress** and is very Gentoo oriented.

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [...Ethereum with Ethminer](#ethereum-with-ethminer)
    * [Install](#install)
    * [If running AMD](#if-running-amd)
        * [If choosing ROCm](#if-choosing-rocm)
            * [Interesting links](#interesting-links)
            * [ROCm WIP:](#rocm-wip)
        * [If choosing AMDGPU-PRO](#if-choosing-amdgpu-pro)
    * [If running Nvidia](#if-running-nvidia)
    * [How to tweak your AMD graphic card](#how-to-tweak-your-amd-graphic-card)
        * [Install](#install-1)
        * [Config](#config)
        * [Use](#use)
        * [Interesting links](#interesting-links-1)
    * [How to tweak your Nvidia graphic card](#how-to-tweak-your-nvidia-graphic-card)
* [...Ethereum with Claymore?](#ethereum-with-claymore)

<!-- vim-markdown-toc -->

---
## ...Ethereum with Ethminer

Ethminer is an Ethash GPU mining worker: with Ethminer you can mine every coin which relies on an
Ethash Proof of Work thus including Ethereum, Ethereum Classic, Metaverse, Musicoin, Ellaism, Pirl,
Expanse and others.

!!! Note "Prerequisite(s)"
    * A valid graphic installation (you can confirm it with a valid Xorg install), meaning that the
      kernel is well configured with all the needed drivers.
    * An Ethereum wallet (e.g. with MyEtherWallet: <https://www.myetherwallet.com/> ). So you should
      have a public Ethereum address (like `0x123456abcdef...`).
    * A mining pool:

        * <https://cryptobtcmining.com/best-ethereum-mining-pools/>

        * <https://github.com/ethereum-mining/ethminer/blob/master/docs/POOL_EXAMPLES_ETH.md#pools-alphabetic-order>
    
        * Here, I will use <https://miningpoolhub.com/>. This mining pool...

            * ...support a lot of others coins (not only based on Ethash)

            * ...has a quite low pool fee of 0.9%

            * ...can auto-exchange your mined coins to any other supported altcoins

            * ...can mine several coins with the same algorithm with an algorithm switch mining
              functionality (e.g. with Ethminer, any Ethash based coin can be mined), in order to
              auto-mine the most profitable coin

            * ...has a pretty advanced payment system, it's calculated based on rewards depending
              on the coin difficulty, the block finding fee, transaction fee, the bonuses, the
              market cost, your hash rate etc.

            * ...has a PPLNS (Pay Per Last N Shares) system: a part of the payment is calculated
              not base on your hash rate but based on the number of shares you submitted during a
              shift

            * ...claims that it don't track miners

            * ...servers in each location (Europe, US, Asia) are redundant, meaning there’s no need
              for a backup

        * In any case, chose a pool that has a close server, a low fee, and that can mine multiple
          blocks per day (<https://etherscan.io/blocks>).

???+ Note "Reference(s)"
    * <https://www.reddit.com/r/EtherMining/wiki/index> (Reddit ether mining reference)
    * <https://www.myetherwallet.com/> (my ether wallet: a good open source Ethereum wallet)
    * <https://github.com/ethereum-mining/ethminer> (Ethminer: a good open source Ethereum miner)
    * <https://github.com/ethereum-mining/ethminer/blob/master/docs/POOL_EXAMPLES_ETH.md#pools-alphabetic-order>
    * <https://cryptobtcmining.com/best-ethereum-mining-pools/> (Ethereum mining pools presentation)
    
    * <https://www.phoronix.com/scan.php?page=article&item=ethereum-eoy-2017&num=1> (AMD vs Nvidia bench)
    * <https://www.phoronix.com/scan.php?page=article&item=amdgpu-1740-rocm&num=1> (ROCm vs AMDGPU bench)

### Install

Install the last Ethminer (here, 0.18.0), and install it:
```console
$ git clone https://github.com/ethereum-mining/ethminer.git
$ cd ehtminer
$ git submodule update --init --recursive
$ git checkout v0.18.0 # checkout to the last stable version (e.g. v0.18.0)
$ mkdir build
$ cd build
$ cmake .. -DETHASHCUDA=OFF # if running AMD, -DETHASHCL=OFF if running Nvidia without OpenCl
$ cmake --build .
$ sudo make install
```

### If running AMD

Make sure those kernels options are set to [y]:

* `CONFIG_HMM_MIRROR`
* `CONFIG_ZONE_DEVICE`
* `CONFIG_DRM_AMDGPU_USERPTR`

Then install [OpenCL](https://wiki.gentoo.org/wiki/OpenCL) "Open Computing Language" is a framework
for writing programs that execute across heterogeneous computing platforms (CPUs, GPUs, FPGAs,
ASICs, etc.). It is needed to mine with the GPU.

As far as I know, there is two valid options to install OpenCL:

* ROCm (open source) (see <https://github.com/RadeonOpenCompute/ROCm>)
* AMDGPU-PRO (proprietary): (see
  <https://amdgpu-install.readthedocs.io/en/latest/install-installing.html#installing-the-pro-variant>)

A third option would be to use `media-libs/mesa` (with `--oneshot`) with the `opencl` use flag on.
But one would only get OpenCL `1.1` (not `2.x`) which seems not to be supported by Ethminer (to be
confirmed).

#### If choosing ROCm

Install `rocm-opencl-runtime`:
```console
# emerge -a rocm-opencl-runtime
```

Check your OpenCL installation:
```console
# emerge -a rocminfo

$ rocminfo # optionnal
```

!!! Tip
    One might also want to install `clinfo` to check the `opencl` installation, **but it might
    conflict with `eselect-opencl`**:
    ```console
    # emerge -a dev-util/clinfo
    ```

    `clinfo` should print something like:
    ```console
    $ clinfo -a
        > Number of platforms 1
        >   Platform Name     AMD Accelerated Parallel Processing
        > ...
    ```

Now it's time to launch Ethminer (`v0.18.0`):
```console
$ ethminer --cl-nobin --cl-local-work 256 --cl-global-work 8192 -G -P stratum+tcp://brioche.1:x@europe.ethash-hub.miningpoolhub.com:20535
```

Why `--cl-nobin`? Because binary kernels are not yet compiled for ROCm platform:
<https://github.com/ethereum-mining/ethminer/issues/1827>

To better understand the other options, run:
```console
$ ./ethminer --help
```

##### Interesting links

* ROCm Ethminer thread: <https://github.com/RadeonOpenCompute/ROCm/issues/132>
* Ethminer ROCm thread: <https://github.com/ethereum-mining/ethminer/issues/1635>
* Ethminer ROCm build bin: <https://github.com/ethereum-mining/ethminer/issues/1520>

##### ROCm WIP:

Binary kernels are not yet compiled for ROCm platform, one might try to build it's own...

* `isa` files: `/home/user/gitapps/ethminer/libethash-cl/kernels/isa`
* kernels: `/usr/local/bin/kernels` and `/home/user/gitapps/ethminer/libethash-cl/kernels/bin`

Install `clrx` (`clrx` might be useful to build it's own binary kernels, **TODO**):
```console
$ git clone git@github.com:CLRX/CLRX-mirror.git
$ cd CLRX-mirror
$ mkdir build
$ cd build
$ cmake .. -DBUILD_TESTS=ON -DBUILD_SAMPLES=ON -DBUILD_DOCUMENTATION=ON
$ make
$ sudo make install
```

Then build new kernel binary:?
```console
?
$ cd ~/gitapps/ethminer/libethash-cl/kernels/isa
$ clxrasm GCN_ethash.isa # ?
?
```

#### If choosing AMDGPU-PRO

Download the AMDGPU-PRO driver from:
<https://www.amd.com/en/support/kb/release-notes/rn-amdgpu-unified-navi-linux>. E.g.
`amdgpu-pro-19.30-934563-ubuntu-18.04.tar.xz`. <br/>

Find your `DISTDIR` location (see the `DISTDIR` variable in `/etc/portage/make.conf` indicated the
path), and copy the downloaded AMDGPU-PRO driver to that location. <br/>

Make sure that `media-libs/mesa` (with `--oneshot`) isn't compiled with the `opencl` use flag and
that `rocm-opencl-runtime` isn't emerged. <br/>

Then install `amdgpu-pro-opencl`:
```console
# emerge -a amdgpu-pro-opencl
```

Check your OpenCL installation:
```console
# emerge -a rocminfo

$ rocminfo # optionnal
```

!!! Tip
    One might also want to install `clinfo` to check the `opencl` installation, **but it might
    conflict with `eselect-opencl`**:
    ```console
    # emerge -a dev-util/clinfo
    ```

    `clinfo` should print something like:
    ```console
    $ clinfo -a
        > Number of platforms 1
        >   Platform Name     AMD Accelerated Parallel Processing
        > ...
    ```

TODO

Old guide to avoid:
<https://web.archive.org/web/20170819033405/http://www2.warwick.ac.uk:80/fac/cross_fac/complexity/people/staff/delgenio/amdgpuprogentoo>

### If running Nvidia

TODO?

### How to tweak your AMD graphic card

#### Install

In order to tweak the clocking and the voltage of the GPU, we will install and use:
<https://www.github.com/sibradzic/amdgpu-clocks>
```console
$ mkdir -p ~/gitapps
$ cd ~/gitapps
$ git clone https://github.com/sibradzic/amdgpu-clocks.git
```

#### Config

First, edit `/etc/default/grub` in order to add the `amdgpu.ppfeaturemask=0xfffd7fff` parameter:
```console
# vi /etc/default/grub
    > ...
    > GRUB_CMDLINE_LINUX_DEFAULT="amdgpu.ppfeaturemask=0xfffd7fff"
    > ...
```

According to this discussion:
<https://forum.ethereum.org/discussion/14678/overclocking-rx-580-on-ubuntu-16-04>, one can also add
the `amdgpu.vm_fragment_size=9` parameter (only relevant for AMDGPU-PRO?):
```console
# vi /etc/default/grub
    > ...
    > GRUB_CMDLINE_LINUX_DEFAULT="amdgpu.ppfeaturemask=0xfffd7fff amdgpu.vm_fragment_size=9"
    > ...
```

Reconfigure grub and reboot:
```console
# grub-mkconfig -o /boot/grub/grub.cfg
# reboot
```

Now copy and execute the following script to make sure that the `amdgpu.ppfeaturemask=0xfffd7fff`
parameter has been taken into account:
```console
$ vi check_ppfeaturemask.sh
    >
    > #!/bin/bash
    >
    > USER_STATES_PATH=${USER_STATES_PATH:-/etc/default/amdgpu-custom-state}
    > SYS_PPFMASK=/sys/module/amdgpu/parameters/ppfeaturemask
    > [ ! -r ${SYS_PPFMASK} ] && echo "Can't access ${SYS_PPFMASK}" && exit 2
    >
    > function check_ppfeaturemask() {
    >   CURRENT_INT10_MASK=$(cat ${SYS_PPFMASK})
    >   # 0x4000 or 14th bit is one indicating if OverDrive has been enabled
    >   OVERDRIVE_MASK=$(printf '%#x' "$(($CURRENT_INT10_MASK & 0x4000))")
    >   [ "x${OVERDRIVE_MASK}" == "x0x4000" ] && echo "..." && echo "[OK] ppfeaturemask is well set, OverDrive is enabled!" && echo "..." && return 0
    >   echo -n "In order to set custom amdgpu power states, boot the machine with "
    >   echo -n "amdgpu.ppfeaturemask=0xfffd7fff or amdgpu.ppfeaturemask=0xffffffff "
    >   echo    "kernel option" && exit 2
    > }
    >
    > check_ppfeaturemask
    >

$ chown +x check_ppfeaturemask.sh
$ ./check_ppfeaturemask.sh
    > ...
    > [OK] ppfeaturemask is well set, OverDrive is enabled!
    > ...
```

Thanks to the `amdgpu.ppfeaturemask=0xfffd7fff` parameter, `OverDrive` functionality is enabled
which will allow us to modify the clocking and the voltage.<br/>

So, one can configure `amdgpu-clocks` to use it:
```console
$ sudo vi /etc/default/amdgpu-custom-states.card0 # create this file if it's the first time
    > # Set custom GPU states 7:
    > OD_SCLK:
    > 7:       1450MHz        1150mV
    > #default:
    > #7:       1380MHz        1150mV
    >
    > # Set custom memory states 2:
    > OD_MCLK:
    > 2:       2050MHz        950mV
    > #default:
    > #2:       2000MHz        950mV
    >
    > # Only allow SCLK states 7:
    > FORCE_SCLK: 7
    >
    > # Force fixed memory state:
    > FORCE_MCLK: 2
    >
    > # In order to allow FORCE_SCLK & FORCE_MCLK:
    > FORCE_PERF_LEVEL: manual
```

When running `amdgpu-clocks`, this will override the `/sys/class/drmm/card0/device/pp_od_clk_voltage`
file, which controls the card (see <https://github.com/sibradzic/amdgpu-clocks> to get more
information).

#### Use

Now simply run `amdgpu-clocks`:
```console
$ cd ~/gitapps/amdgpu-clocks
# ./amdgpu-cloks
```

#### Interesting links

* <https://www.reddit.com/r/Amd/comments/agwroj/how_to_overclock_your_amd_gpu_on_linux/> (Reddit guide OC)
* <https://forum.ethereum.org/discussion/14678/overclocking-rx-580-on-ubuntu-16-04> (AMDGPU-PRO OC)
* <https://github.com/sibradzic/amdgpu-clocks> (AMD OC)
* <https://github.com/sibradzic/amdgpu-clocks/blob/master/amdgpu-clocks>

### How to tweak your Nvidia graphic card

TODO?

---
## ...Ethereum with Claymore?

* <https://github.com/nanopool/Claymore-Dual-Miner>
* <https://www.cruxpool.com/help/how-to-mine-with-claymore/>
* <https://github.com/RadeonOpenCompute/ROCm/issues/495>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
