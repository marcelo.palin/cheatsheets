---
tags:
  - Crypto
  - Cryptocurrencies
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `cointop`

`cointop` is a fast and lightweight interactive terminal based UI application for tracking and
monitoring cryptocurrency coin stats in real-time.

???+ Note "Reference(s)"
    * <https://www.youtube.com/watch?v=HV5zvDRR-ZE>
    * <https://aur.archlinux.org/packages/cointop-bin/>
    * <https://aur.archlinux.org/packages/cointop/>
    * <https://www.youtube.com/watch?v=HV5zvDRR-ZE>
    * <https://cointop.sh/cli/>
    * <https://github.com/miguelmota/cointop>


---
## Table of contents
<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

TODO

---
## Config

TODO

---
## Use

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
