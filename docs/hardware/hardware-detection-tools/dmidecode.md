---
tags:
  - Hardware
  - Hardware Detection Tools
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `dmidecode`

`dmidecode` (DMI table decoder): a tool for dumping a computer's DMI (some say SMBIOS) table
contents in a human readable format. This table contains a description of the system's hardware
components, as well as other useful pieces of information such as serial numbers and  BIOS
revision. Thanks to this table, you can retrieve this information without having to probe for the
actual hardware.

???+ Note "Reference(s)"
    * `$ man dmidecode`
    * <https://wiki.gentoo.org/wiki/Hardware_detection>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        ```console
        # emerge -a sys-apps/dmidecode
        ```

    === "pacman"
        ```console
        # pacman -S dmidecode
        ```

    === "apt"
        ```console
        # apt install dmidecode
        ```

    === "yum"
        ```console
        # yum install dmidecode
        ```

    === "dnf"
        ```console
        # dnf install dmidecode
        ```

---
## Use

```console
$ sudo dmidecode -s
$ sudo dmidecode -s system-serial-number
$ sudo dmidecode -s system-manufacturer
$ sudo dmidecode -s system-product-name
$ sudo dmidecode -t
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
