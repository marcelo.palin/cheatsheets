---
tags:
  - Hardware
  - RAID
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# PercCLI

PercCLI seems to be a forked version of StorCLI, by Dell.

???+ Note "Reference(s)"
    * TODO

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install


!!! Note ""

    === "apk"
        TODO

    === "~~apt~~ dpkg (by hand)"
        Download the latest version of PercCLI (`7.1910.00` at the time of writing, check it on
        [repology](https://repology.org/project/perccli/versions). You can also find the URL link
        to the latest tarball when looking at the [NixOS (unstable)
        package](https://github.com/NixOS/nixpkgs/blob/nixos-unstable/pkgs/tools/misc/perccli/default.nix)
        (just search for "url" in that file).
        ```console
        $ cd /tmp
        $ mkdir perccli && cd perccli
        $ wget https://dl.dell.com/FOLDER07815522M/1/PERCCLI_7.1910.00_A12_Linux.tar.gz
        $ tar -zxvf PERCCLI_7.1910.00_A12_Linux.tar.gz
        $ cd PERCCLI_7.1910.00_A12_Linux/
        $ sudo dpkg -f perccli_007.1910.0000.0000_all.deb Package # print package name
        $ sudo dpkg -i perccli_007.1910.0000.0000_all.deb # install package
        
        $ ls /opt/MegaRAID/perccli/
            perccli64
        
        $ sudo /opt/MegaRAID/perccli/perccli64 --help
        ```
        (Note: to uninstall, run `$ sudo dpkg -P perccli`)

    === "dnf (with EPEL)"
        TODO

    === "dnf (by hand)"
        Download the latest version of PercCLI (`7.5.007.0529` at the time of writing: check it on
        [repology](https://repology.org/project/perccli/versions). You can also find the URL link
        to the latest tarball when looking at the [NixOS (unstable)
        package](https://github.com/NixOS/nixpkgs/blob/nixos-unstable/pkgs/tools/misc/perccli/default.nix)
        (just search for "url" in that file).
        ```console
        $ wget https://downloads.dell.com/FOLDER05235308M/1/perccli_linux_NF8G9_A07_7.529.00.tar.gz
        $ tar xzf perccli_linux_NF8G9_A07_7.529.00.tar.gz
        $ sudo dnf localinstall ./perccli_7.5-007.0529_linux/Linux/perccli-007.0529.0000.0000-1.noarch.rpm
        ```

    === "emerge"
        ```console
        # emerge -a sys-block/perccli
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.perccli
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.perccli
            ```

    === "~~pacman~~ AUR (by hand)"
        Install with [AUR](../distros/arch-based/aur.md):
        ```console
        $ mkdir -p ~/apps/aur-apps
        $ cd ~/apps/aur-apps
        $ git clone https://aur.archlinux.org/perccli.git
        $ cd perccli
        $ makepkg -si # --syncdeps to auto-install deps, --install to install after building
        ```

    === "yum (with EPEL)"
        TODO

    === "yum (by hand)"
        Download the latest version of PercCLI (`7.5.007.0529` at the time of writing: check it on
        [repology](https://repology.org/project/perccli/versions). You can also find the URL link
        to the latest tarball when looking at the [NixOS (unstable)
        package](https://github.com/NixOS/nixpkgs/blob/nixos-unstable/pkgs/tools/misc/perccli/default.nix)
        (just search for "url" in that file).
        ```console
        $ wget https://downloads.dell.com/FOLDER05235308M/1/perccli_linux_NF8G9_A07_7.529.00.tar.gz
        $ tar xzf perccli_linux_NF8G9_A07_7.529.00.tar.gz
        $ sudo yum localinstall ./perccli_7.5-007.0529_linux/Linux/perccli-007.0529.0000.0000-1.noarch.rpm
        ```

    === "xbps"
        TODO

    === "zypper"
        TODO


---
## Use

!!! Note ""
    When specified, the PERC controller used in the below examples is the controller 0 (i.e. `/c0`).

* same as [StorCLI](./storcli.md).

* print information on the controller and the configuration:
```console
$ sudo perccli /c0 show all
```

* print information on the existing hard drives and their status (IDs,...):
```console
$ sudo perccli /c0 /eall /sall show
```

* print information on the existing virtual drives and their status:
```console
$ sudo perccli /c0 /vall show
```

* print current status of all rebuilds:
```console
$ sudo perccli /c0 /eall /sall show rebuild
```


* locate a physical disk, e.g. disk 1 in enclosure 64 of the controller 0:
```console
$ sudo perccli /c0/e64/s1 start locate
$ sudo perccli /c0/e64/s1 stop locate
```

* create a RAID 5 virtual drive of 11 disks from slot 0 to 10 (included), in enclosure 64:
```console
$ perccli /c0 add vd type=raid5 names=data drives=64:0-10
```

* create a RAID 5 virtual drive of 11 disks from slot 0 to 10 (included) in enclosure 64, with a hot
  spare from slot 11 in enclosure 64:
```console
$ perccli /c0 add vd type=raid5 name=data drives=64:0-10 spares=64:11
```

* add hot spare disk, e.g. disk 11 in enclosure 64 of the controller 0 to the drive group 2:
```console
$ perccli /c0/e64/s11 add hotsparedrive gd=2
```

* add hot spare disk, e.g. disk 11 in enclosure 64 of the controller 0, globally:
```console
$ perccli /c0/e64/s11 add hotsparedrive
```

* delete hot spare disk, e.g. disk 11 in enclosure 64 of the controller 0:
```console
$ perccli /c0/e64/s11 delete hotsparedrive
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
