---
tags:
  - Hardware
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Printers

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Category:Printers>
    * <https://wiki.gentoo.org/wiki/Printing>
    * <https://www.networkworld.com/article/3373502/printing-from-the-linux-command-line.html>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

TODO

- [cups](https://repology.org/project/cups/versions)

- [foomatic-db](https://github.com/OpenPrinting/foomatic-db)
- [gutenprint](https://github.com/echiu64/gutenprint)
- For arch based distros, see <https://aur.archlinux.org/packages/ppds-meta>, including the
  following:
    - foomatic-db
    - foomatic-db-engine
    - foomatic-db-gutenprint-ppds
    - foomatic-db-nonfree
    - foomatic-db-nonfree-ppds
    - foomatic-db-ppds
    - gutenprint

---
## Config

TODO

---
## Use

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
