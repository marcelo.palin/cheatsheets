---
tags:
  - Hardware
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Bootable media: how to get a live USB

Like a live CD or DVD, a bootable USB drive lets you run practically any Linux distribution without
affecting your computer. You can also install a Linux distribution on your PC from it, no CD or DVD
drive required. You **can’t** simply copy or extract the ISO file to the USB drive and expect it to
work, however. While you don’t technically "burn" the ISO file to a USB drive, there’s a special
process required to take a Linux ISO file and make a bootable USB drive with it.

Here is how.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/USB_flash_installation_medium>
    * <https://wiki.archlinux.org/index.php/Install_Arch_Linux_on_a_removable_medium>
    * <https://www.howtogeek.com/414574/how-to-burn-an-iso-file-to-a-usb-drive-in-linux/>
    * <http://www.system-rescue-cd.org>
    * <https://www.system-rescue.org/Installing-SystemRescue-on-a-USB-memory-stick/>
    * <http://www.system-rescue-cd.org/Installing-SystemRescueCd-on-a-USB-stick>
    * <https://www.howtogeek.com/howto/14912/create-a-persistent-bootable-ubuntu-usb-flash-drive/>
    * <https://unix.stackexchange.com/questions/312687/why-is-sync-so-important-when-making-a-bootable-linux-usb-stick>
    * <https://wiki.gentoo.org/wiki/LiveUSB>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Find an ISO image](#find-an-iso-image)
* [Install an ISO image into an USB drive...](#install-an-iso-image-into-an-usb-drive)
    * [...on Windows with `rufus`](#on-windows-with-rufus)
    * [...on Linux with `usbimager` (graphical user interface)](#on-linux-with-usbimager-graphical-user-interface)
    * [...on Linux with `dd` (command line interface)](#on-linux-with-dd-command-line-interface)
        * [Hybrid ISO](#hybrid-iso)
    * [Renaming the USB partitions](#renaming-the-usb-partitions)
* [Boot from USB](#boot-from-usb)
* [Bonus: live Linux USB drive with persistent storage](#bonus-live-linux-usb-drive-with-persistent-storage)

<!-- vim-markdown-toc -->

---
## Find an ISO image

You might want to boot on a live USB for multiple reasons. Probably the most common one is just to
install the Linux distribution you want: here are ISO download links for some common distros:

* <https://ubuntu.com/download/desktop>
* <https://xubuntu.org/download/>
* <https://kubuntu.org/getkubuntu/>
* <https://linuxmint.com/download.php>
* <https://getfedora.org/en/workstation/download/>
* <https://www.centos.org/download/>
* <https://www.archlinux.org/download/>
* <https://artixlinux.org/download.php>
* <https://www.gentoo.org/downloads/>

Another reason might be to have a Linux system rescue toolkit available as a bootable medium for
administrating or repairing your system and data after a bad crash. In this case you might be
interested in the SystemRescueCD ISO:

* <https://www.system-rescue.org/Installing-SystemRescue-on-a-USB-memory-stick/>


---
## Install an ISO image into an USB drive...

!!! Warning "Warning"
    **Installing an ISO image into an USB drive will wipe the content of the drive!**

!!! Warning "Warning"
    **Installing an ISO image into any drive will wipe the content of the drive, so MAKE SURE your are
    installing the ISO image into the intended drive and not e.g. your system drive!**



### ...on Windows with `rufus`

If you are running Windows on your computer, a well recommended installation program is `rufus`, as
it is easy to use and supports both UEFI and Legacy BIOS booting methods.

* Download [`rufus`](https://rufus.ie/) and install it on Windows.

    !!! Warning "Warning"
        Make sure you downloaded `rufus` here: <https://rufus.ie/> And **NOT** e.g. here:
        <http://rufususb.com/>

* Plug in your USB stick and wait a few seconds to allow enough time for the system to detect it.
* Execute `rufus` and select the USB stick in the drop-down list.
* Select the ISO image that you have downloaded.
* Select `MBR` partition scheme as it will be compatible with both BIOS and UEFI.
* Select `BIOS or UEFI` in target to get the best compatibility.
* Select `FAT32` file system as the UEFI boot process only works from FAT file systems.
* Click on the start button and wait until the operation is complete.

### ...on Linux with `usbimager` (graphical user interface)

A well recommended graphical tool for installing an ISO image to a memory stick on Linux is
[`usbimager`](https://gitlab.com/bztsrc/usbimager/-/releases) as it does the job well, it is very
compact, it comes with minimal dependencies and it can be executed without installation.

* Download `usbimager` (e.g. `v1.0.5`), and run it:
    ```console
    $ cd ~/downloads
    $ wget https://gitlab.com/bztsrc/usbimager/-/raw/binaries/usbimager_1.0.5-x86_64-linux-gtk.zip
    $ unzip usbimager_1.0.5-x86_64-linux-gtk.zip
    $ sudo ./bin/usbimager
    ```

* Select the ISO image you want to install in the first field (first row).
* Select your USB device with the menu button of the third row.
* Click on the "Write" button (second row).

!!! Note "Note"
    Depending on your Linux distribution, there is a lot of other graphical tools available, like
    `Startup Disk Creator`, `Media Writer`, `mkusb` etc. Feel free to try them if you want.

### ...on Linux with `dd` (command line interface)

See [how to install/clone an ISO image on a target drive with
`dd`](../files/dd.md#installingcloning-an-iso-image-on-a-target-drive) (here your target drive is
your USB device).

#### Hybrid ISO

**If** the USB drive doesn't boot, try to hybrid the ISO:

!!! Note ""

    === "emerge"
        ```console
        # emerge --ask sys-boot/syslinux  
        ```

    === "pacman"
        ```console
        # pacman -S syslinux
        ```

    === "apt"
        ```console
        # apt install syslinux-utils
        ```

    === "yum"
        ```console
        # yum install syslinux
        ```

    === "dnf"
        ```console
        # dnf install syslinux
        ```
        
```console
$ mv /path/to/image.iso /path/to/image-hybrid.iso
$ isohybrid /path/to/image-hybrid.iso

$ umount /media/usbdevice
$ lsblk -f # to identify the usb device (e.g. sdx)
# dd if=/path/to/image-hybrid.iso of=/dev/sdx bs=4M conv=fdatasync status=progress
```

### Renaming the USB partitions

After installing an ISO image into your USB drive, you might want to rename it. See
<https://wiki.archlinux.org/title/persistent_block_device_naming#by-label> in order to do so.


---
## Boot from USB

Finally, boot from the USB drive:

* <https://fossbytes.com/enter-bios-utility-uefi-settings-all-pc-boot-from-usb/>

!!! Warning ""
    **Most live images do not support Secure Boot!** You will need to [disable Secure
    Boot](https://web.archive.org/web/20220522042243/https://www.rodsbooks.com/efi-bootloaders/secureboot.html#disable)
    from the host computer before booting on the live image. Note that if you are using a live
    image to install a GNU/Linux distribution, then you can optionally [set up Secure
    Boot](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot) after
    completing the installation.


---
## Bonus: live Linux USB drive with persistent storage

* <https://www.howtogeek.com/howto/14912/create-a-persistent-bootable-ubuntu-usb-flash-drive/>
* <https://www.linuxuprising.com/2019/03/create-persistent-storage-live-usb-with.html>
* <https://www.kali.org/docs/usb/usb-persistence/>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
