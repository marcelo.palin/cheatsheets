---
tags:
  - Hardware
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# RyzenAdj

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

???+ Note "Reference(s)"
    * <https://github.com/FlyGoat/RyzenAdj>
    * <https://gitlab.com/ryzen-controller-team/ryzen-controller>
    * <https://fosspost.org/overclock-amd-ryzen-linux/>

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
