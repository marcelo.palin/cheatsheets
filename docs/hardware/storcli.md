---
tags:
  - Hardware
  - RAID
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# StorCLI

StorCLI is the successor of the MegaCLI and allows to manage and control LSI MegaRAID controllers.

???+ Note "Reference(s)"
    * <https://www.thomas-krenn.com/en/wiki/StorCLI_commands>
    * <https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=storcli>
    * <https://docs.broadcom.com/doc/12352476>
    * <https://support.nine.ch/articles/raid-configuration-using-storcli>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install


!!! Note ""

    === "apk"
        TODO

    === "apt"
        TODO

    === "dnf via EPEL"
        TODO

    === "dnf by hand"
        Download the latest version of StorCli (`7.1508` at the time of writing, check it on
        [repology](https://repology.org/project/storcli/versions), you can also find the latest zip
        file to download on the [StorCli AUR
        page](https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=storcli)):
        ```console
        $ wget https://docs.broadcom.com/docs-and-downloads/raid-controllers/raid-controllers-common-files/007.1508.0000.0000_Unified_StorCLI-PUL.zip
        $ unzip -d storcli 007.1508.0000.0000_Unified_StorCLI-PUL.zip
        $ sudo dnf localinstall ./Unified_storcli_all_os/Linux/storcli-007.1508.0000.0000-1.noarch.rpm
        ```

    === "emerge"
        ```console
        # emerge -a sys-block/storcli
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.storcli
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.storcli
            ```

    === "~~pacman~~ AUR (by hand)"
        TODO: <https://aur.archlinux.org/packages/storcli/>

    === "yum via EPEL"
        TODO

    === "yum by hand"
        Download the latest version of StorCli (`7.1508` at the time of writing, check it on
        [repology](https://repology.org/project/storcli/versions), you can also find the latest zip
        file to download on the [StorCli AUR
        page](https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=storcli)):
        ```console
        $ wget https://docs.broadcom.com/docs-and-downloads/raid-controllers/raid-controllers-common-files/007.1508.0000.0000_Unified_StorCLI-PUL.zip
        $ unzip -d storcli 007.1508.0000.0000_Unified_StorCLI-PUL.zip
        $ sudo yum localinstall ./Unified_storcli_all_os/Linux/storcli-007.1508.0000.0000-1.noarch.rpm
        ```

    === "xbps"
        ```console
        # xbps-install -S cronie
        ```

    === "zypper"
        ```console
        # zypper install cronie
        ```


---
## Use

See <https://www.thomas-krenn.com/en/wiki/StorCLI_commands>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
