---
tags:
  - Versioning
  - Git
  - CI/CD
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# GitLab Runner

GitLab Runner is an application that works with GitLab CI/CD to run jobs in a pipeline.

GitLab CI/CD enable to start continuous integration (CI) and optionally continuous
Delivery/Deployment (CD) on GitLab repositories (see <https://docs.gitlab.com/ee/ci/README.html>
for more details).

* Continuous Integration works by running scripts, after every push, in order to build, test, and validate the code changes.
* Continuous Delivery and Deployment consist of a step further CI, deploying your application to production at every push to the default branch of the repository.

!!! Tip
    GitLab CI/CD allow you to catch bugs and errors early in the development cycle, ensuring that
    all the code deployed to production complies with the code standards you established for your
    app.

???+ Note "Reference(s)"
    * <https://docs.gitlab.com/ee/ci/quick_start>
    * <https://gitlab.com/gitlab-org/gitlab-runner>
    * <https://docs.gitlab.com/runner/>
    * <https://docs.gitlab.com/ee/ci/yaml/>
    * <https://docs.gitlab.com/runner/commands/README.html>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Glossary](#glossary)
* [Install](#install)
    * [Troubleshooting](#troubleshooting)
    * [Update](#update)
    * [Remove](#remove)
    * [With a container](#with-a-container)
* [Config](#config)
* [Use](#use)
    * [`.gitlab-ci.yml` file](#gitlab-ciyml-file)

<!-- vim-markdown-toc -->

---
## Glossary

* `.gitlab-ci.yml`: A file at the root of your project repository, defining the scripts you want to run after every push, in order to build, test, and validate the code changes (see <https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html> for more details).
* **Job**: A Job is the most fundamental element of the `.gitlab-ci.yml` file, it defines the scripts to be executed and under what conditions they should be executed (see <https://docs.gitlab.com/ee/ci/jobs/> for more details).
* **Runner**: A runner is a lightweight agent that picks up a Job (through the coordinator API of GitLab CI/CD), runs the job, and sends the result back to the GitLab instance (see <https://docs.gitlab.com/ee/ci/runners/README.html> for more details).
* **GitLab Runner**: `gitlab-runner` is the software used to orchestrate runners, running this software on a dedicated computer might be a wise choice (see <https://gitlab.com/gitlab-org/gitlab-runner> for more details).
* **Pipeline**: A Pipeline is a component that defines in what order to run Jobs. A pipeline can regroup jobs in "stages",  which define when to run the jobs. For example, stages that run tests after stages that compile the code. Note that multiple Jobs in the same stage are executed in parallel, if there are enough concurrent runners (see <https://docs.gitlab.com/ee/ci/pipelines/> for more details).
    * If all jobs in a stage succeed, the pipeline moves on to the next stage.
    * If any job in a stage fails, the next stage is not (usually) executed and the pipeline ends early.

---
## Install

???+ Note "Reference(s)"
    * <https://docs.gitlab.com/runner/install/linux-manually.html>
    * <https://docs.gitlab.com/runner/install/linux-repository.html>

!!! Warning "Warning"
    GitLab Runner should be the same version as the GitLab server instance (see
    <https://docs.gitlab.com/runner/index.html#gitlab-runner-versions>)!

* Look at the version of the GitLab instance you are running : after connection to your GitLab instance -> click the question mark next to your profile picture (top right corner) -> click "help" -> you will find the version number at the top of that page, e.g. `13.7.6`

* Look at the closest version number here : <https://gitlab.com/gitlab-org/gitlab-runner/-/releases> (e.g. `13.7.0`)
```console
$ gitlab_runner_version="v13.7.0"
```

* Download the GitLab runner binary depending on your computer architecture:

!!! Note ""

    === "Linux x86-64"
        ```console
        $ sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/${gitlab_runner_version}/binaries/gitlab-runner-linux-amd64"
        ```

    === "Linux x86"
        ```console
        $ sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/${gitlab_runner_version}/binaries/gitlab-runner-linux-386"
        ```

    === "Linux arm"
        ```console
        $ sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/${gitlab_runner_version}/binaries/gitlab-runner-linux-arm"
        ```

    === "Linux arm64"
        ```console
        $ sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/${gitlab_runner_version}/binaries/gitlab-runner-linux-arm64"
        ```

    === "Linux s390x"
        ```console
        $ sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/${gitlab_runner_version}/binaries/gitlab-runner-linux-s390x"
        ```

    * Make sure that you didn't just downloaded an `.xml` or `.html` file:
      ```console
      $ cat /usr/local/bin/gitlab-runner
      ```
      If so, then `gitlab-runner` may not support this particular version: check
      [here](https://docs.gitlab.com/runner/install/index.html) to find a way to properly install
      it.

* Note that if you are using the `gitLab.com` instance, then you just have to install the latest
  `gitlab-runner` (which should be compatible):

!!! Note ""

    === "Linux x86-64"
        ```console
        $ sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
        ```

    === "Linux x86"
        ```console
        $ sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-386"
        ```

    === "Linux arm"
        ```console
        $ sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-arm"
        ```

    === "Linux arm64"
        ```console
        $ sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-arm64"
        ```

    === "Linux s390x"
        ```console
        $ sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-s390x"
        ```

    * Make sure that you didn't just downloaded an `.xml` or `.html` file:
      ```console
      $ cat /usr/local/bin/gitlab-runner
      ```
      If so, then `gitlab-runner` may not support this particular version: check
      [here](https://docs.gitlab.com/runner/install/index.html) to find a way to properly install
      it.

* Give it permissions to execute:
  ```console
  $ sudo chmod +x /usr/local/bin/gitlab-runner
  ```

* Create a GitLab CI user:
  ```console
  $ sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
  ```

* Install:
  ```console
  $ sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
  ```

* Run as service (e.g. with SystemD):
  ```console
  $ sudo systemctl enable gitlab-runner
  $ sudo systemctl start gitlab-runner
  $ sudo systemctl status gitlab-runner
  ```

### Troubleshooting

???+ Note "Reference(s)"
    * <https://gitlab.com/gitlab-org/gitlab/-/issues/28668>
    * <https://gitlab.com/gitlab-org/gitlab-ce/issues/61719>
    * <https://docs.gitlab.com/runner/install/linux-manually.html>
    * <https://docs.gitlab.com/runner/register/>

If you get the following message `FATAL: Failed to install gitlab-runner: Not supported system`,
this is probably because you are using a non SystemD distro. This is not a blocking problem,
according to <https://gitlab.com/gitlab-org/gitlab/-/issues/28668>, `gitlab-runner install` only
sets up a SystemD service in order to auto start and manage the runner.

E.g. on Gentoo, you can deal with this problem like so:

```console
$ sudo vi /etc/init.d/gitlab-runner

    > #!/sbin/openrc-run
    >
    > name="gitlab-runner"
    > command="/usr/local/bin/gitlab-runner"
    > command_args="run -u gitlab-runner -d /home/gitlab-runner"
    > command_background=true
    > pidfile="/var/run/gitlab-runner.pid"
    >
    > depend() {
    >     need net localmount
    > }

$ sudo chmod +x /etc/init.d/gitlab-runner
$ sudo rc-update add gitlab-runner
$ sudo rc-service gitlab-runner start
```

### Update

???+ Note "Reference(s)"
    * <https://docs.gitlab.com/runner/install/linux-manually.html>

* Stop the service (if the previous `gitlab-runner install` command has been run on a SystemD
  distro):
  ```console
  $ sudo gitlab-runner stop
  ```

* Stop the service (if the previous `gitlab-runner install` command has been run on a OpenRC
  distro):
  ```console
  $ sudo rc-service gitlab-runner stop
  ```

* Download the binary to replace the GitLab Runner executable:

!!! Note ""

    === "Linux x86-64"
        ```console
        $ sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/${gitlab_runner_version}/binaries/gitlab-runner-linux-amd64"
        ```

    === "Linux x86"
        ```console
        $ sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/${gitlab_runner_version}/binaries/gitlab-runner-linux-386"
        ```

    === "Linux arm"
        ```console
        $ sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/${gitlab_runner_version}/binaries/gitlab-runner-linux-arm"
        ```

    === "Linux arm64"
        ```console
        $ sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/${gitlab_runner_version}/binaries/gitlab-runner-linux-arm64"
        ```

    === "Linux s390x"
        ```console
        $ sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/${gitlab_runner_version}/binaries/gitlab-runner-linux-s390x"
        ```

* Give it permissions to execute:
  ```console
  $ sudo chmod +x /usr/local/bin/gitlab-runner
  ```

* Start the service (if the previous `gitlab-runner install` command has been run on a SystemD
  distro):
  ```console
  $ sudo gitlab-runner start
  ```

* Start the service (if the previous `gitlab-runner install` command has been run on a OpenRC
  distro):
  ```console
  $ sudo rc-service gitlab-runner stop
  ```

### Remove

* Unregister a GitLab runner from GitLab server (see
  <https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1316>):
  ```console
  $ sudo gitlab-runner unregister -u <url> -t <token>
  $ sudo gitlab-runner restart
  ```

* Delete a GitLab runner locally (see <https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1316>):
  ```console
  $ sudo gitlab-runner verify --delete -u <url> -t <token>
  $ sudo gitlab-runner restart
  ```

* Remove completely `gitlab-runner` (after unregistering and deleting GitLab runners):
  ```console
  $ sudo gitlab-runner uninstall
  $ sudo userdel -r gitlab-runner
  $ sudo rm /usr/local/bin/gitlab-runner
  ```

### With a container

**WIP**

Alternatively, an officially maintained GitLab Runner container can be used (e.g. with Docker):
<https://docs.gitlab.com/runner/install/docker.html>

Prerequisite(s):

* [Docker](../virtualization/docker.md) (note that [Podman](../virtualization/podman.md) can be
  used instead)

```console
$ docker pull gitlab/gitlab-runner:latest

$ docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```

With the bellow alias, you can continue with the [config](#config) and [use](#use) sections:
```console
$ alias gitlab-runner="docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner"
```

!!! Tip ""
    If you want to open a bash shell into the `gitlab-runner` container, then you can use this
    command:
    ```console
    $ docker ps
    $ docker exec -it gitlab-runner bash
    ```

---
## Config

After installing GitLab Runner, register a new runner to your GitLab project (see
<https://docs.gitlab.com/runner/register/>):
```console
$ sudo gitlab-runner register
    > ...
    > #######################
    > # example output/input:
    > #######################
    > ...
    > # Runtime platform
    > # Running in system-mode.
    >
    > # Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
    > https://gitlab.com/
    > # Please enter the gitlab-ci token for this runner:
    > ydARH00VBaeya5sPMc33
    > # Please enter the gitlab-ci description for this runner:
    > short-description
    > # Please enter the gitlab-ci tags for this runner (comma separated):
    > tag1,tag2,tag3
    > # Registering runner... succeeded
    > # runner=ydARHDqV
    > # Please enter the executor: docker-ssh+machine, kubernetes, custom, docker-ssh, parallels, ssh, docker, shell, virtualbox, docker+machine:
    > shell
    > # Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

!!! Tip "GitLab token"
    You can find the GitLab token info here:
    Connect to your GitLab instance (e.g. <https://gitlab.com>) → open your repository → select the
    "Settings" tab (on the left panel) → select the "CI/CD" sub-tab (on the left panel) → in
    the "Runners" section of the page: click on "Expand" → you will find your registration token
    here

!!! Tip "GitLab tags"
    Tags will enable you to select which job can use the registered runner.
    These tags can be re-configured in GitLab GUI later.
    See
    <https://docs.gitlab.com/ee/ci/runners/#use-tags-to-limit-the-number-of-jobs-using-the-runner>
    for more details.

!!! Tip "GitLab executors"
    * Shell: Enables to run builds on the machine where the `gitlab-runner` is installed.
    * SSH: Enables to run build on other machine accessible through SSH from the machine where
      `gitlab-runner` is installed.
    * Docker: Enables to run build in docker containers.
    * ...
    See <https://docs.gitlab.com/runner/executors/README.html> for more details.

!!! Tip "GitLab shared runners"
    You might not be interested in the shared runners, you can disable them like so: from your
    project select: Settings -> CI/CD -> Runners (expand) -> disable shared runners for this
    project

---
## Use

* help:
  ```console
  $ sudo gitlab-runner help
  ```

* Register a new GitLab runner (see <https://docs.gitlab.com/runner/register/index.html>):
  ```console
  $ sudo gitlab-runner register
  ```

* Verify your runners:
  ```console
  $ sudo gitlab-runner verify
  ```

* List GitLab runner(s):
  ```console
  $ sudo gitlab-runner list
  ```

* Reconfigure `gitlab-runner` (see
  <https://docs.gitlab.com/runner/configuration/advanced-configuration.html>):
  ```console
  $ sudo vi /etc/gitlab-runner/config.toml
      > ...
  $ sudo systemctl restart gitlab-runner
  ```

* Unregister a GitLab runner from GitLab server (see
  <https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1316>):
  ```console
  $ sudo gitlab-runner unregister -u <url> -t <token>
  $ sudo gitlab-runner restart
  ```

* Delete a GitLab runner locally (see <https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1316>):
  ```console
  $ sudo gitlab-runner verify --delete -u <url> -t <token>
  $ sudo gitlab-runner restart
  ```

* Remove completely `gitlab-runner` (after unregistering and deleting GitLab runners):
  ```console
  $ sudo gitlab-runner uninstall
  $ sudo userdel -r gitlab-runner
  $ sudo rm /usr/local/bin/gitlab-runner
  ```

### `.gitlab-ci.yml` file

After installing and configuring a GitLab Runner, a `.gitlab-ci.yml` file is needed at the root of
your repository in order to enable CI and optionally CD.

* See <https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html> for more details about the
  `.gitlab-ci.yml` file.

* See <https://docs.gitlab.com/ee/ci/yaml/README.html> for more details about the `.gitlab-ci.yml`
  syntax.

Here is a little (and commented) `.gitlab-ci.yml` file example :
```console
stages:
    - Build
    - Test

build_code:
    stage: Build
    tags:
        - plop # any runner tagged with "epics" might run this job
    # the following "before_script" should be executed right after fetching or cloning the repo
    before_script:
        - export ENV_VAR="toto"
        - sed -i '/plop/a plip' /path/to/a/config-file
        - echo "configuration before build: DONE"
    # the following "script" should be executed right after the previous "before_script"
    script:
        - make clean && make && echo ">>> build OK" || echo ">>> build KO"
    #artifacts: # see <https://docs.gitlab.com/ee/ci/yaml/README.html#artifacts>
    #   paths:
    #       - /path/to/plop

unit_tests:
    stage: Test
    tags:
        - plop
    script:
        - cd tests/gtest
        - ENV_VAR1="test1" ENV_VAR2="test2" cmake .
        - ./tests
```

For more examples: see <https://docs.gitlab.com/ee/ci/examples/README.html>

!!! Tip "Tip"
    Note that you can check the syntax of your `.gitlab-ci.yml` file from your GitLab project:
    Connect to your GitLab instance (e.g. <https://gitlab.com>) → open your repository → select the
    "CI/CD" tab (on the left panel) → click on the "CI Lint" button (top right of the page) →
    paste the content of your `.gitlab-ci.yml` file and click on "Validate"


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
