---
tags:
  - Versioning
  - SVN
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Subversion

SVN, is a versioning software and a revision control system distributed under an open source
license.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Subversion>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        ```console
        # emerge -a dev-vcs/subversion
        ```

    === "pacman"
        ```console
        # pacman -S subversion
        ```

    === "apt"
        ```console
        # apt install subversion
        ```

    === "yum"
        ```console
        # yum install subversion
        ```

    === "dnf"
        ```console
        # dnf install subversion
        ```

TODO


---
## Config

TODO


---
## Use

* Check out a working copy from a repository:
```console
$ svn checkout http://some_app.code.com/svn/projectname/
$ svn co http://some_app.code.com/svn/projectname/
```

    * ... With a different username:
    ```console
    $ svn checkout --username plop http://some_app.code.com/svn/projectname/
    ```

* Bring changes from the repository into your working copy. Otherwise, it synchronizes the working
  copy to the revision given by the `--revision` (`-r`) option:
```console
$ svn update # w/o revision, it brings your working copy up to date with the HEAD
$ svn up -r42 # with revision (-r), it synchronizes the working copy to that revision
```

* Print the status of working copy files and directories:
```console
$ svn status # prints only localy modified items
$ svn status --show-updates # -u, prints working revision and server out-of-date info
$ svn status --verbose # -v, prints full revision info on everu item
$ svn status --quiet # -q, prints only summary information about local items
```

* Add files, directories, or symbolic links in your working copy for addition to the repository:
```console
$ svn add test
```

* Undo `$ svn add` (without reverting local edits):
```console
$ svn rm --keep-local . # . or * or a file name etc.
```

* Delete an item from a working copy or the repository:
```console
$ svn delete test0
$ svn remove test1
$ svn del test2
$ svn rm test3
```

* revert all modifications to exactly match the repository into your working copy (at the root of
  the SVN project):
```console
$ svn revert --recursive .
```

* Send changes from your working copy to the repository:
```console
$ svn commit -m "commit with this commit message"
$ svn ci -m "commit with this commit message"
```

* Display commit log messages:
```console
$ svn log
$ svn log path/to/folder/or/file # to get commit messages associated to an item
```

* Show author and revision information inline for the specified file:
```console
$ svn blame path/to/file
```

* Displays the differences between two revisions or paths:
```console
$ svn diff
$ svn di
```

* Resolve conflicts on working copy files or directories: After updating, if SVN indicates that a
  conflict has been discovered, it should be postpone (`-p` option), than edited in order remove
  the conflict markers and resolve the conflict. Finally, the resolve command shall be used to
  inform SVN about the conflict resolution:
```console
$ svn resolve --accept=working test # resolution after conflict on file "test"
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
