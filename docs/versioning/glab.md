---
tags:
  - Versioning
  - Git
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `glab`

`gLab` is an open source GitLab CLI tool. Work with issues, merge requests, watch running pipelines
directly from your CLI among other features. Inspired by [`gh`](./gh.md), the official GitHub CLI
tool.

!!! Warning "Prerequisite(s)"
    * [SSH](./../networks/ssh.md)
    * [SSH with Git](./git.md#ssh)

???+ Note "Reference(s)"
    * <https://github.com/profclems/glab>
    * <https://glab.readthedocs.io/en/latest/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

In order to install `glab`, run the following (you might want to check the content of
<https://raw.githubusercontent.com/profclems/glab/trunk/scripts/install.sh> before executing it):
```console
$ curl -s https://raw.githubusercontent.com/profclems/glab/trunk/scripts/install.sh | sudo sh
```

---
## Config

```console
$ glab auth login
  >
  > ? What GitLab instance do you want to log into?
  >   GitLab Self-hosted Instance
  >
  > ? GitLab hostname:
  >   drf-gitlab.cea.fr
  >
  > ? Paste your authentication token:
  >
  >   Generate a Personal Access Token here: https://drf-gitlab.cea.fr/-/profile/personal_access_tokens
  >   (named "glab", without expiration date, and selecting *at least* "api" and "write_repository"
  >   scopes)
  >   
  >   After clicking "Create personal access token", copy your new personal access token (at the top of
  >   the page), then paste it into the terminal asking your authentication token.
  >
  > ? Choose default git protocol
  >   SSH
```

If needed, see [how to install, configure and use SSH](./../networks/ssh.md), and see [how to
configure SSH with Git](./git.md#ssh).

Then, set the default editor used by `glab` (e.g. `vi`):
```console
$ glab config set --global editor vi
```

---
## Use

Now, in the console, when in a folder tracked by Git, the following commands can be run:

* List issues of a Git project:
```console
$ glab issue list
```

* Check the CI of a Git project:
```console
$ glab pipeline ci view
```

See <https://glab.readthedocs.io/en/latest/intro.html#usage> for more commands.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
