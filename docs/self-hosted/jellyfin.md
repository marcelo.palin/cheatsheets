---
tags:
  - Self-hosted
  - Media Players
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Jellyfin

???+ Note "Reference(s)"
    * <https://jellyfin.org/docs/>
    * <https://jellyfin.org/docs/general/quick-start.html>
    * <https://github.com/jellyfin/jellyfin>
    * <https://wiki.archlinux.org/title/Jellyfin>
    * <https://www.reddit.com/r/jellyfin/comments/erccwn/jellyfin_on_docker_for_dummies/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Clients](#clients)

<!-- vim-markdown-toc -->

**TODO**


---
## Install

**TODO**


---
## Config

**TODO**


---
## Use

**TODO**

### Clients

See <https://wiki.archlinux.org/title/Jellyfin#Clients>.

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
