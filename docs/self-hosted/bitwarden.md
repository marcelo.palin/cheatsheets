---
tags:
  - Self-hosted
  - Security
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


!!! Warning
    Last thorough review: `12/01/2022`
---


# Bitwarden

> With [Vaultwarden](https://github.com/dani-garcia/vaultwarden).

**TODO**

The Bitwarden CLI is a powerful, fully featured tool for accessing and managing your Vault. Most
features that you find in other Bitwarden client applications (Desktop, Browser Extension, etc.)
are available from the CLI.

???+ Note "Reference(s)"
    * <https://bitwarden.com/help/article/cli/>
    * <https://github.com/bitwarden/cli>
    * <https://matrix.to/#/#vaultwarden:matrix.org>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
    * [Bitwarden CLI](#bitwarden-cli)
* [Config](#config)
    * [Bitwarden CLI](#bitwarden-cli-1)
        * [ZSH Shell Completion](#zsh-shell-completion)
* [Use](#use)
    * [Bitwarden CLI](#bitwarden-cli-2)

<!-- vim-markdown-toc -->

---
## Install

**TODO**

### Bitwarden CLI

**WIP**

```console
$ mkdir -p ~/app/bin-apps/bitwarden
$ wget https://vault.bitwarden.com/download/?app=cli&platform=linux
$ mv index.html\?app=cli\&platform=linux bw.zip
$ unzip bw.zip
$ chmod +x bw
$ rm bw.zip
```

---
## Config

### Bitwarden CLI

???+ Note "Reference(s)"
    * <https://bitwarden.com/help/article/cli/#config>

*   If you are using a self-hosted instance of Bitwarden, then you might want to link Bitwarden CLI
    to it:
    ```console
    $ bw config server https://your.bw.domain.com
    ```

#### ZSH Shell Completion

Bitwarden CLI includes support for ZSH shell completion. To setup shell completion, use one of the
following methods:

1.  Vanilla ZSH:

    Add the following line to your `${ZDOTDIR:-${HOME}}/.zshrc` (or wherever) file:
    ```console
    $ eval "$(bw completion --shell zsh); compdef _bw bw;"
    ```

2.  Vanilla (vendor completions):

    Run the following command:
    ```console
    $ bw completion --shell zsh | sudo tee /usr/share/zsh/vendor-completions/_bw
    ```

3.  [`zinit`](https://github.com/zdharma-continuum/zinit):

    Run the following commands:
    ```console
    $ bw completion --shell zsh > ~/.local/share/zsh/completions/_bw
    $ zinit creinstall ~/.local/share/zsh/completions
    ```


---
## Use

### Bitwarden CLI

⚠️ **WIP** ⚠️

*   [Login](https://bitwarden.com/help/article/cli/#log-in):
    ```console
    $ bw login your@mail.com # then enter your master password
    ```

    Then, don't forget to export the session key that has been printed, e.g. like so:
    ```console
    $ export BW_SESSION="5PBYGU+5yt3RHcCjoeJKx/wByU34vokGRZjXpSH7Ylo8w=="
    ```

!!! Tip
    The first time you login you might be asked for your API key `client_secret`. See how to get
    this API key [here](https://bitwarden.com/help/article/cli-auth-challenges/).


*   [Lock](https://bitwarden.com/help/article/cli/#unlock):

    The `BW_SESSION` environment variable is only tied to the active terminal session, so closing
    your terminal window is equivalent to locking your Vault. You can also destroy an active
    session key to lock your Vault by running:
    ```console
    $ bw lock
    ```

*   [Unlock](https://bitwarden.com/help/article/cli/#unlock):

    Using an API Key or SSO to log in will require you to follow-up the login command with an
    explicit `bw unlock` if you will be working with Vault data directly.

    Unlocking your Vault generates a session key which acts as a session-specific decryption key
    used to interact with data in your Vault. The session key must be used to perform any command
    that touches Vault data (e.g. `list`, `get`, `edit`). Generate a new session key at any time
    using:
    ```console
    $ bw unlock
    ```

*   [List/Search](https://bitwarden.com/help/article/cli/#list):
    ```console
    $ bw list items --search "characters to search for"
    ```

*   [Get](https://bitwarden.com/help/article/cli/#get):
    ```console
    $ bw get item "characters to search for"
    ```

    You can get a lot more things than an `item`. You can also get `password`, `username`, `uri`,
    `notes`, `totp`, `exposed`, `attachment`, `folder`, `collection`, `organization`,
    `org-collection`, `template`, and `fingerprint`.

*   [Sync](https://bitwarden.com/help/article/cli/#sync):

    The `sync` command downloads your encrypted vault from the Bitwarden server. This command is
    most useful when you have changed something in your Bitwarden Vault on another client
    application (e.g. Web Vault, Browser Extension, Mobile App) since logging in on the CLI.
    ```console
    $ bw sync
    ```

    You can pass the `--last` option (`$ bw sync --last`) to return only the timestamp (ISO 8601) of
    the last time a sync was performed.

    !!! Tip
        It’s important to know that the `sync` command only performs a pull from the server. Data
        is automatically pushed to the server any time you make a change to your Vault (e.g. with
        the `create`, `edit`, `delete` commands).

*   [Update](https://bitwarden.com/help/article/cli/#update):

    The update command checks whether Bitwarden CLI is running the most recent version. **It does
    not automatically update the CLI for you**:
    ```console
    $ bw update
    ```

*   [Status](https://bitwarden.com/help/article/cli/#status):

    The status command returns status information about the Bitwarden CLI, including configured
    server URL, timestamp for the last sync (ISO 8601), user email and ID, and the Vault status:
    ```console
    $ bw status
    ```

*   [Generate](https://bitwarden.com/help/article/cli/#generate):

    * Generate a complex password of 16 character:
    ```console
    $ bw generate --lowercase --uppercase --number --special --length 16
    ```

    * Generate a complex passphrase of 8 words separated with the `-` character:
    ```console
    $ bw generate --passphrase --words 8 --separator -
    ```


⚠️ **WIP** ⚠️


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
