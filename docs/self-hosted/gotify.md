---
tags:
  - Self-hosted
  - Messengers
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `gotify`

???+ Note "Reference(s)"
    * <https://gotify.net/>
    * <https://gotify.net/docs/index>
    * <https://github.com/gotify>
    * <https://github.com/gotify/cli>
    * <https://github.com/ztpnk/gotify-dunst>
    * <https://news.ycombinator.com/item?id=19347848>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
