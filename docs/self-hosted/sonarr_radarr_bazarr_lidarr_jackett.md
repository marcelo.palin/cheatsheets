---
tags:
  - Self-hosted
  - Download Managers
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Sonarr, Radarr, Bazarr, Lidarr and Jackett

**TODO**

???+ Note "Reference(s)"
    * <https://sonarr.tv/#home>
    * <https://github.com/Sonarr/Sonarr>
    * <https://radarr.video/#home>
    * <https://github.com/Radarr/Radarr>
    * <https://www.bazarr.media/>
    * <https://github.com/morpheus65535/bazarr>
    * <https://lidarr.audio/>
    * <https://github.com/lidarr/Lidarr>
    * <https://github.com/Jackett/Jackett>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
    * [Sonarr](#sonarr)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

### Sonarr

Sonarr prerequisites:

* `mono` (3.6+ but 3.10+ is recommended)
* `mediainfo` (for processing files on import)
* `sqlite3` (database)

See <https://download.sonarr.tv/v2/master/mono/>.

**TODO**

---
## Config

**TODO**

---
## Use

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
