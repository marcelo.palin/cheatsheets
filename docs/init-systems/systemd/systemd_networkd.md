---
tags:
  - SystemD
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `systemd-networkd`

`systemd-networkd` is a system daemon that manages network configurations. It detects and
configures network devices as they appear; it can also create virtual network devices. This service
can be especially useful to set up complex network configurations for a container managed by
[`systemd-nspawn`](./systemd_nspawn.md) or for virtual machines. It also works
fine on simple connections.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Systemd-networkd>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

TODO

---
## Config

TODO

---
## Use

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
