---
tags:
  - Drives
  - Disks
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Partitions

Disk partitioning or disk slicing is the creation of one or more regions on a storage, so that each
region can be managed separately.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Partitioning>
    * <https://geekpeek.net/resize-filesystem-fdisk-resize2fs/>
    * <https://www.howtogeek.com/116742/how-to-create-a-separate-home-partition-after-installing-ubuntu/>
    * <https://www.howtogeek.com/114503/how-to-resize-your-ubuntu-partitions/>

!!! Tip "UEFI/GPT vs BIOS/MBR"
    Difference between UEFI/GPT and BIOS/MBR:
    <https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Disks#Partition_tables>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [GPT `fdisk` (`gdisk`, `cgdisk`, `sgdisk`, and `fixparts`)](#gpt-fdisk-gdisk-cgdisk-sgdisk-and-fixparts)
* [`fdisk`](#fdisk)
* [GNU parted](#gnu-parted)
    * [Gnome parted](#gnome-parted)

<!-- vim-markdown-toc -->

---
## GPT `fdisk` (`gdisk`, `cgdisk`, `sgdisk`, and `fixparts`)

* <https://wiki.archlinux.org/index.php/GPT_fdisk>
* <https://www.rodsbooks.com/gdisk/>
* <https://www.rodsbooks.com/gdisk/index.html>


---
## `fdisk`

* <https://wiki.archlinux.org/index.php/Fdisk>


---
## GNU parted

* <https://wiki.archlinux.org/index.php/Parted>
* <https://www.gnu.org/software/parted/parted.html>
* <https://www.gnu.org/software/parted/manual/>
* <https://rainbow.chard.org/2013/01/30/how-to-align-partitions-for-best-performance-using-parted/>
* <http://positon.org/resize-an-ext3-ext4-partition>
* <http://gparted-forum.surf4.info/>

### Gnome parted

> Gnome front end to GNU parted.

* <https://wiki.gentoo.org/wiki/User:Maffblaster/Drafts/Gparted>
* <https://www.linuxsecrets.com/archlinux-wiki/wiki.archlinux.org/index.php/GNU_Parted.html>
* <https://en.wikipedia.org/wiki/GParted>

!!! Tip "Tip"
    A sure way of modifying partitions, through a live CD-ROM/USB, without losing data:

    * <https://www.howtoforge.com/partitioning_with_gparted>
    * <https://askubuntu.com/questions/47409/shrink-a-partition-without-losing-data>

    **Don't forget to run `# fsck sdxN` after modifying a partition (e.g. partition `sdxN`) and
    before rebooting, in order to make sure there is no leftover disks errors.**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
