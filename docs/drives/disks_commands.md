---
tags:
  - Drives
  - Disks
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Disks

**TODO**: split this cheat sheets in multiple dedicated ones (one per command).

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/SSD>
    * <https://wiki.archlinux.org/index.php/Solid_state_drive>
    * <https://wiki.gentoo.org/wiki/NVMe>
    * <https://wiki.archlinux.org/index.php/Solid_state_drive/NVMe>
    * <https://wiki.gentoo.org/wiki/HDD>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [`udisksctl`](#udisksctl)
* [`df`](#df)
* [`du`](#du)
* [`lsblk`](#lsblk)
* [`ncdu`](#ncdu)
* [`mount`/`umount`](#mount-umount)

<!-- vim-markdown-toc -->

---
## `udisksctl`

!!! Note ""

    === "emerge"
        ```console
        # emerge -a sys-fs/udisks
        ```

    === "pacman"
        ```console
        # pacman -S udisks2
        ```

    === "apt"
        ```console
        # apt install udisks2
        ```

    === "yum"
        ```console
        # yum install udisks2
        ```

    === "dnf"
        ```console
        # dnf install udisks2
        ```

Turn off a disk **after unmounting it**:
```console
$ sudo udisksctl unmount -b /dev/disk/by-label/disklabal && sudo udisksctl power-off -b /dev/disk/by-label/disklabal
```

---
## `df`

* Check disks space:
```console
$ df -kh
```

**TODO**

---
## `du`

**TODO**


---
## `lsblk`

* Check disks partitions:
```console
$ lsblk -f # (no need for blkid)
```


---
## `ncdu`

* Browse disks space:
```console
$ ncdu
```

---
## `mount`/`umount`

* Mount an NTFS disk with `755` permissions for directories and `644` for files:
```console
$ sudo mount -t ntfs-3g /dev/... /media/... -o uid=1001,gid=1001,dmask=022,fmask=133
```

* Mount a CIFS disk (e.g. samba server?) (⚠️ Prerequisite package for Ubuntu: `cifs-utils`):
```console
$ sudo mount -t cifs "//server.address/path/to/plop" /mnt/mountpoint -o user=username,password=passphrase
```

* Unmount a disk:
```console
$ sudo umount /dev/disk/by-label/
or
$ sudo umount /media/disk/mount-location/
```

* Eject a disk:
```console
$ sudo umount /dev/disk/by-label/
or
$ sudo umount /media/disk/mount-location/
```

* Very safe way to unplug HDD by putting it in sleep mode first:
```console
$ sudo sync; sudo hdparm -Y /dev/disk/by-label/ # now one can unplug hdd
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
