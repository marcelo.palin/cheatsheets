---
tags:
  - Drives
  - Disks
  - File Systems
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# AutoFS

AutoFS is a program that uses the Linux kernel auto mounter to automatically mount file systems on
demand. It works with USB flash drives and external hard drives, network shares, etc.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Autofs>
    * <https://wiki.gentoo.org/wiki/AutoFS>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
