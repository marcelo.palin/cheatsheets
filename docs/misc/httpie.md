---
tags:
  - HTTP
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# HTTPie

HTTPie is a command-line HTTP client. Its goal is to make CLI interaction with web services as
human-friendly as possible. HTTPie is designed for testing, debugging, and generally interacting
with APIs and HTTP servers. The `http` and `https` commands allow for creating and sending
arbitrary HTTP requests. They use simple and natural syntax and provide formatted and colorized
output.

???+ Note "Reference(s)"
    * <https://github.com/httpie/httpie>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

**TODO**

---
## Config

**TODO**

---
## Use

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
