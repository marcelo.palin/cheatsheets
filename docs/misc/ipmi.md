---
tags:
  - Monitoring Programs
  - Management
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# IPMI

IPMI is a set of computer interface specifications for an autonomous computer subsystem that
provides management and monitoring capabilities independently of the host system's CPU, firmware
(BIOS or UEFI) and operating system. IPMI defines a set of interfaces used by system administrators
for out of band management of computer systems and monitoring of their operation.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/IPMI>
    * <https://www.admin-linux.fr/ipmi-intelligent-platform-management-interface/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
