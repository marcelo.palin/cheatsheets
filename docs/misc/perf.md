---
tags:
  - Performance Analyzing Programs
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `perf`

???+ Note "Reference(s)"
    * <https://linuxfr.org/news/hotspot-a-la-recherche-du-point-chaud>
    * <https://fr.wikipedia.org/wiki/Profilage_de_code>
    * <https://en.wikipedia.org/wiki/Profiling_(computer_programming)>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**

Check CPU limitation of your program:

```console
$ perf stat /path/to/program/to/run # see `man perf-stat`
```

for a more detailed output, but the measure will be less precise (because much more info is
monitored):

```console
$ perf stat -d /path/to/program/to/run # see `man perf-stat`
$ perf stat -dd /path/to/program/to/run # see `man perf-stat`
$ perf stat -ddd /path/to/program/to/run # see `man perf-stat`
```

> In order to stay more precise, but less frequently, one can use the `--interval` option.

* Print all symbolic event types:

```console
$ perf list
```

* Find the functions where your program takes most of its time/efforts: i.e. find the "hot spots".
  Perf record will only work with programs compiled ahead of time (C, C++, Rust, etc)

```console
$ perf record --call-graph=dwarf -F 100 <command-to-run> --arguments-of-the-command-to-run
```

* `perf report` : histogramme configurable des échantillons:  par binaire source, fonction, lide de
  code source, etc


* `perf script -F +pid > /tmp/test.perf` then load it into : <https://profiler.firefox.com> (then
  click on "Load profile from file")



---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
