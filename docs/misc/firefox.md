---
tags:
  - Web Browsers
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Firefox

Mozilla Firefox, or simply Firefox, is a free and open-source web browser developed by the Mozilla
Foundation and its subsidiary, Mozilla Corporation.

!!! Tip ""
    Nice alternative to consider: [librewolf](https://librewolf.net/)

???+ Note "Reference(s)"
    * <https://www.privacytools.io/>
    * <https://wiki.gentoo.org/wiki/Firefox>
    * <https://wiki.archlinux.org/index.php/Firefox>
    * <https://en.wikipedia.org/wiki/Firefox>

!!! Note "TODO"
    * <https://libre-ouvert.tuxfamily.org/index.php?article230/ma-configuration-de-firefox-en-cette-fin-2020>
    * <https://news.ycombinator.com/item?id=26254785>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
    * [Addons](#addons)
    * [Misc configurations](#misc-configurations)
    * [TODO: `searx` instead of `duckduckgo`?](#todo-searx-instead-of-duckduckgo)
* [Use](#use)
    * [Troubleshooting](#troubleshooting)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        Compiling the source version of Firefox can be a long process; a pre-compiled (binary)
        version does exist in the Gentoo portage repository: `firefox-bin` package.  Moreover, the
        "firefox" package to built firefox from source is only focused on stabilizing ESR versions.
        ```console
        # emerge -a firefox-bin
        ```
        But if you rather prefer the long way for the ESR version:
        ```console
        # emerge -a firefox
        ```

    === "pacman"
        ```console
        # pacman -S firefox
        ```

    === "apt"
        ```console
        # apt install firefox
        ```

    === "yum"
        ```console
        # yum install firefox
        ```

    === "dnf"
        ```console
        # dnf install firefox
        ```

Installing a web browser is a good opportunity to read carefully <https://www.privacytools.io/>
(especially to apply the sections on addons and `about:config` tweaks).

---
## Config

A sane Firefox configuration is suggested here: <https://www.privacytools.io/>

### Addons

* (`adnauseam`)
* `bitwarden`
* `cookie autodelete`
    * config:
        * about:addons -> Extensions -> `Cookie AutoDelete` -> `...` -> Preferences -> CAD Settings
            * Enable Support Firefox's Container Tabs
* `dark background and light text`
* `decentraleyes`
* `emoji cheat sheet`
* `firefox multi-account containers`
* `h t t p s everywhere`
* `invidition`
    * config
        * `about:addons` -> Extensions -> `Invidition` -> Preferences
            * Instance: `invidio.us`
            * Interface: Dark Mode (selected)
            * Interface: Thin Mode (selected)
* `languagetool`
* `grammalecte`
* (`noscript security suite`)
* `privacy badger`
* `privacy spy`
* (`privacy settings`)
* `qwant`
    * config:
        * Preferences -> Search -> Default Search Engine : `Qwant`
* `qwant lite`
    * config:
        * Preferences -> Search -> Default Search Engine : `Qwant Lite`
* `SQLite Manager` (<https://add0n.com/sqlite-manager.html?version=0.2.7&type=install>)
* `ublock origin`
* `useragent switcher`
* `vimium`
* `wayback machine`
* `temporary containers`
	* config:
		* about:addons -> Extensions -> Temporary Containers -> "..." -> Preferences
            * General: "Automatic Mode" selected
            * General: "Random Container Color" selected
            * General: "Container Icon" = "circle"
            * General: "Container Number" = "Keep counting until browser restart"
            * General: "Delete no longer needed Temporary Containers" = "After the last tab in it closes"
* `terms of service; didn’t read: be informed`
* `tree style tab`
    * config:
        * about:addons -> Extensions -> Tree Style Tab -> Preferences
            * "Unlock Expert Options" selected
            * Appearance -> Theme -> Photon
            * Toolbar Icon Color -> for Dark Theme
            * Tree Behavior -> "When a new tree appears, collapse others automatically" (unselected)
            * Tree Behavior -> "When a tab gets focus, expand its tree and ..." (unselected)
* (`uMatrix`)

### Misc configurations

* see <https://www.privacytools.io/browsers/#browser>

* see <https://www.privacytools.io/browsers/#webrtc>

* see <https://www.privacytools.io/browsers/#about_config>

* Preferences -> Customize...
    * Toolbar disposition:
        * `Back`"
        * `Forward`
        * Main search bar
        * `Reload`
        * `Downloads`
        * `Add-ons`,
        * `Wayback Machine`
        * Flexible Space
        * `Bitwarden`
        * `uBlock Origin`
        * `HTTPS Everywhere`
        * `Cookie AutoDelete`
        * `Decentraleyes`
        * `Privacy Badger`
        * `Multi-account Containers`
        * `PrivacySpy`
        * Flexible Space
        * `User-Agent Switcher`
        * `Emoji Cheatsheet`
        * `Vimium`
        * `Invidition`
        * `LanguageTool`
        * `Dark Background and Light Text`
    * "Title Bar" unselected (bottom left)
    * "Drag Space" unselected (bottom left)
    * Themes (bottom left) -> Dark
    * Density (bottom left) -> Compact

* Preferences -> General -> Settings - Network Settings (scroll all the way down) -> Select "Enable
  DNS over HTTPS"

* For small screens (e.g. 13 or 14 inches): about:config -> `layout.css.devPixelsPerPx = 1.3`
  (default was -1)

* Remove tab bar: <https://medium.com/@Aenon/firefox-hide-native-tabs-and-titlebar-f0b00bdbb88b>
    * about:config -> `toolkit.legacyUserProfileCustomizations.stylesheets : True` -> Menu -> Help ->
      Troubleshooting Information -> find "Profile Directory" path (e.g.
      /home/user/.mozilla/firefox/lkw7qu5i.default-release) -> create "chrome" directory in that
      directory -> add the following `userChrome.css` file into the "chrome" directory:
      ```console
      #titlebar {margin-bottom: -30px !important;}
      #tabbrowser-tabs {visibility: collapse !important;}
      ```
      or?
      ```console
      #titlebar {margin-bottom: -30px !important;}
      #TabsToolbar { visibility: collapse !important; }
      ```

### TODO: `searx` instead of `duckduckgo`?

* <https://www.youtube.com/watch?v=Rkv4rrdFBU0>
* <https://www.youtube.com/watch?v=oufXi3e-VuA>

---
## Use

* Run Firefox:
```console
$ firefox
or
$ firefox-bin
```

### Troubleshooting

* <https://bugzilla.mozilla.org/show_bug.cgi?id=1711039> -> is full uninstall and reinstall enough?


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
