---
tags:
  - Document Converter
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `unoconv`

Convert any document from and to any LibreOffice supported format.

???+ Note "Reference(s)"
    * `$ man unoconv`

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Warning "Warning"
    `unoconv` depends on [LibreOffice](https://repology.org/project/libreoffice/versions)

!!! Note ""

    === "emerge"
        ```console
        # emerge -a app-office/unoconv
        ```

    === "pacman"
        ```console
        # pacman -S unoconv
        ```

    === "apt"
        ```console
        # apt install unoconv
        ```

    === "yum"
        ```console
        # yum install unoconv
        ```

    === "dnf"
        ```console
        # dnf install unoconv
        ```

---
## Use

* Convert a `.xlsx` file to a `.csv` (by creating a file with the same name but with the `.csv`
  extension):
```console
$ unoconv -f csv file-name.xlsx
```

* Convert a `.csv` file to a `.xlsx` (by creating a file with the same name but with the `.xlsx`
  extension):
```console
$ unoconv -f csv file-name.xlsx
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
