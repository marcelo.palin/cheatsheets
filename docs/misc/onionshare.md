---
tags:
  - File sharing
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# OnionShare

OnionShare is an open source tool that lets you securely and anonymously share files, host
websites, and chat with friends using the Tor network.

???+ Note "Reference(s)"
    * <https://github.com/micahflee/onionshare/wiki>
    * <https://github.com/micahflee/onionshare>
    * <https://onionshare.org/>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "pacman"
        ```console
        # pacman -S onionshare
        ```

    === "apt"
        ```console
        # apt install onionshare
        ```


---
## Config

Change the default data directory (`data_dir`) location:
```console
$ mkdir -p ~/.local/share/onionshare
$ vi ~/.config/onionshare/onionshare.json
    > ...
  ~ > "data_dir": "/home/user/.local/share/onionshare",
    > ...
```

If you want to set a password:
```console
$ vi ~/.config/onionshare/onionshare.json
    > ...
  ~ > "password": "your-secret-password",
    > ...
```


---
## Use

To share a file, just run:
```console
$ onionshare /path/to/file/or/directory/to/share
```

Then, share the `*.onion` link to the downloader. This link shall be open with the [tor
browser](https://repology.org/project/torbrowser-launcher/versions). If a username and a password
are asked (even if none has been set), the default username is `onionshare` without password.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
