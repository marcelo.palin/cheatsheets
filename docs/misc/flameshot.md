---
tags:
  - Screenshot
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# flameshot

flameshot is a simple screenshot software.

???+ Note "Reference(s)"
    * <https://github.com/flameshot-org/flameshot/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "apk"
        ```console
        # apk add flameshot
        ```

    === "apt"
        ```console
        # apt install flameshot
        ```

    === "dnf"
        ```console
        # dnf install flameshot
        ```

    === "emerge"
        ```console
        # emerge -a media-gfx/flameshot
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.flameshot
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.flameshot
            ```

    === "pacman"
        ```console
        # pacman -S flameshot
        ```

    === "yum"
        ```console
        # yum install flameshot
        ```

    === "xbps"
        ```console
        # xbps-install -S flameshot
        ```

    === "zypper"
        ```console
        # zypper install flameshot
        ```


---
## Config

* Open the graphical configuration menu:
    ```console
    $ flameshot config -h
    $ flameshot config
    ```

* You can also edit some of the settings (like overriding the default colors) in the configuration
  file: `${XDG_CONFIG_HOME:-${HOME/.config}}/flameshot/flameshot.ini`.


---
## Use

* Print help:
    ```console
    $ flameshot -h
    ```

* Capture with GUI:
    ```console
    $ flameshot gui
    ```

* Capture with GUI with custom save path:
    ```console
    $ flameshot gui -p ~/myStuff/captures
    ```

* Capture with GUI after 2 seconds delay (can be useful to take screenshots of mouse hover tooltips, etc.):
    ```console
    $ flameshot gui -d 2000
    ```

* Fullscreen capture with custom save path (no GUI) and delayed:
    ```console
    $ flameshot full -p ~/myStuff/captures -d 5000
    ```

* Fullscreen capture with custom save path copying to clipboard:
    ```console
    $ flameshot full -c -p ~/myStuff/captures
    ```

* Capture the screen containing the mouse and print the image (bytes) in PNG format:
    ```console
    $ flameshot screen -r
    ```

* Capture the screen number 1 and copy it to the clipboard:
    ```console
    $ flameshot screen -n 1 -c
    ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
