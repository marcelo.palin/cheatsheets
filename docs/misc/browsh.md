---
tags:
  - Web Browsers
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `browsh`

A fully interactive, real-time, and modern text based browser rendered to TTY and browsers.

???+ Note "Reference(s)"
    * <https://www.brow.sh/>
    * <https://github.com/browsh-org/browsh>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "apk"
        TODO

    === "apt"
        TODO

    === "dnf"
        TODO

    === "emerge (with overlay)"
        ```console
        # emerge --ask --noreplace eselect-repository
        # eselect repository enable guru
        # emaint sync -r guru
        # echo '*/*::guru ~amd64' >> /etc/portage/package.accept_keywords
        # emerge -a www-client/browsh
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.browsh
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.browsh
            ```

    === "~~pacman~~ AUR (by hand)"
        Install with [AUR](../distros/arch-based/aur.md):
        ```console
        $ mkdir -p ~/apps/aur-apps
        $ cd ~/apps/aur-apps
        $ git clone https://aur.archlinux.org/browsh-bin.git
        $ cd browsh-bin
        $ makepkg -is
        ```

    === "yum"
        TODO

    === "xbps"
        TODO

    === "zypper"
        TODO


---
## Config

**TODO**


---
## Use

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
