---
tags:
  - Media Players
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `spotifyd` and `spotify-tui`

!!! Note "Prerequisite(s)"
    * [System logger](../logs/system_loggers.md)
    * [Spotify premium account](https://www.spotify.com/us/premium/)

???+ Note "Reference(s)"
    * <https://open.spotify.com>
    * <https://github.com/Spotifyd/spotifyd>
    * <https://github.com/Rigellute/spotify-tui>
    * <https://www.youtube.com/watch?v=TaPWqXFtce8>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Troubleshooting](#troubleshooting)

<!-- vim-markdown-toc -->

---
## Install

Install [`spotifyd`](https://repology.org/project/spotifyd/versions) and
[`spotify-tui`](https://repology.org/project/spotify-tui/versions) (`spt`, e.g. with
[AUR](../distros/arch-based/aur.md): <https://aur.archlinux.org/spotify-tui.git>)


---
## Config

`spotifyd` config:
``` console
$ vi ~/.config/spotifyd
    > [global]
    > # Your Spotify account name.
    > username = "username"
    >
    > # Your Spotify account password.
    > password = "password"
    >
    > # A command that gets executed and can be used to
    > # retrieve your password.
    > # The command should return the password on stdout.
    > #
    > # This is an alternative to the `password` field. Both
    > # can't be used simultaneously.
    > password_cmd = "command_that_writes_password_to_stdout"
    >
    > # If set to true, `spotifyd` tries to look up your
    > # password in the system's password storage.
    > #
    > # This is an alternative to the `password` field. Both
    > # can't be used simultaneously.
    > use_keyring = true
    >
    > # The audio backend used to play the your music. To get
    > # a list of possible backends, run `spotifyd --help`.
    > backend = "alsa"
    >
    > # The alsa audio device to stream audio to. To get a
    > # list of valid devices, run `aplay -L`,
    > device = "alsa_audio_device"  # omit for macOS
    >
    > # The alsa control device. By default this is the same
    > # name as the `device` field.
    > control = "alsa_audio_device"  # omit for macOS
    >
    > # The alsa mixer used by `spotifyd`.
    > mixer = "PCM"
    >
    > # The volume controller. Each one behaves different to
    > # volume increases. For possible values, run
    > # `spotifyd --help`.
    > volume_controller = "alsa"  # use softvol for macOS
    >
    > # A command that gets executed in your shell after each song changes.
    > on_song_change_hook = "command_to_run_on_playback_events"
    >
    > # The name that gets displayed under the connect tab on
    > # official clients. Spaces are not allowed!
    > device_name = "device_name_in_spotify_connect"
    >
    > # The audio bitrate. 96, 160 or 320 kbit/s
    > bitrate = 160
    >
    > # The directory used to cache audio data. This setting can save
    > # a lot of bandwidth when activated, as it will avoid re-downloading
    > # audio files when replaying them.
    > #
    > # Note: The file path does not get expanded. Environment variables and
    > # shell placeholders like $HOME or ~ don't work!
    > cache_path = "cache_directory"
    >
    > # If set to true, audio data does NOT get cached.
    > no_audio_cache = true
    >
    > # Volume on startup between 0 and 100
    > initial_volume = 90
    >
    > # If set to true, enables volume normalisation between songs.
    > volume_normalisation = true
    >
    > # The normalisation pregain that is applied for each song.
    > normalisation_pregain = -10
    >
    > # The port `spotifyd` uses to announce its service over the network.
    > zeroconf_port = 1234
    >
    > # The proxy `spotifyd` will use to connect to spotify.
    > proxy = "http://proxy.example.org:8080"
    >
    > # The displayed device type in Spotify clients.
    > # Can be unknown, computer, tablet, smartphone, speaker, tv,
    > # avr (Audio/Video Receiver), stb (Set-Top Box), and audiodongle.
    > device_type = "speaker"
```

How to get setup `spotify-tui`:

1. Go to the Spotify dashboard - <https://developer.spotify.com/dashboard/applications>.
2. Click `Create a Client ID` and create an app.
3. Now click `Edit Settings`.
4. Add <http://localhost:8888/callback> to the Redirect URIs.
5. You are now ready to authenticate with Spotify!

Run `spt` and answer the questions related to the previous `spotify-tui` setup.

Afterwards, check `spotify-tui` config:
```console
$ cat ~/.config/spotify-tui/client.yml
```


---
## Use

* First run `spotifyd`: `$ spotifyd`.

* Then either run `spotify-tui`: `$ spt`, or listen directly on <https://open.spotify.com>.

### Troubleshooting

* "Couldn't initialize logger" error:
  ```console
  error `thread 'main' panicked at 'Couldn't initialize logger`
  ```
    * a [system logger](../logs/system_loggers.md) might not be set up.
    * Hack around: add the `--no-daemon` option


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
