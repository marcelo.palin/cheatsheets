---
tags:
  - Flashcards
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Anki

Anki is a free and open-source flashcard program that utilizes spaced repetition. Spaced repetition
has been shown to increase rate of memorization.

???+ Note "Reference(s)"
    * <https://ankiweb.net/>
    * <https://github.com/ankitects/anki>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install from sources](#install-from-sources)
* [Config](#config)
* [Use](#use)
    * [`apy`](#apy)
    * [Tips, Tricks and Troubleshooting](#tips-tricks-and-troubleshooting)
        * [Dark cards](#dark-cards)

<!-- vim-markdown-toc -->

---
## Install from sources

TODO


---
## Config

TODO


---
## Use

TODO

### `apy`

See:

* <https://github.com/lervag/apy/>
* <https://github.com/lervag/apy/issues/1>

**WIP**

Install `apy`:
```console
$ cd ~/projects
$ git clone https://github.com/lervag/apy.git
$ cd apy
$ pip install . --user
$ ~/.local/bin/apy --help
```

### Tips, Tricks and Troubleshooting

#### Dark cards

Sometimes, some cards' images will appear dark when night mode is activated (probably due to the
use of transparent background for the images of some decks).

In this case, one can invert the images of a deck by inserting the following code into "browse ->
cards -> styling": 

```
.night_mode img {

 filter: invert(1); -webkit-filter:invert(1);

}
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
