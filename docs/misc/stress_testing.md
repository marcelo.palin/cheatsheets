---
tags:
  - Performance Analyzing Programs
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Stress testing

TODO

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Stress_testing>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [`stress-ng`](#stress-ng)

<!-- vim-markdown-toc -->

---
## `stress-ng`

TODO

???+ Note "Reference(s)"
    * <https://wiki.ubuntu.com/Kernel/Reference/stress-ng>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
