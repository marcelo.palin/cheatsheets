---
tags:
  - Media Players
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# mpv

mpv is a free and open source command line media player. It is based on MPlayer2, which in turn is
based on the original MPlayer. Although there are still many similarities to its ancestors, mpv
should generally be treated as a completely different program. It supports a wide variety of video
file formats, audio and video codecs, and subtitle types.


???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Mpv>
    * <https://wiki.gentoo.org/wiki/Mpv>
    * <https://github.com/mpv-player/mpv/blob/master/DOCS/mplayer-changes.rst>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

TODO

---
## Config

TODO

---
## Use

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
