---
tags:
  - Games
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Steam

Steam is a digital distribution platform, which offers DRM, multiplayer gaming, video streaming and
social networking services.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Steam>
    * <https://wiki.archlinux.org/index.php/Steam>
    * <https://fosspost.org/tutorials/enable-steam-play-on-linux-to-run-windows-games>
    * <https://github.com/ValveSoftware/Proton/>
    * <https://www.protondb.com/>
    * <https://github.com/GloriousEggroll/proton-ge-custom>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
    * [Avoid steam dotfile madness (still in progress)](#avoid-steam-dotfile-madness-still-in-progress)
* [Use](#use)
    * [Proton](#proton)
        * [Enable Proton](#enable-proton)
        * [Force Proton](#force-proton)
        * [Configure Proton](#configure-proton)
    * [Proton Glorious Egroll (Proton-GE)](#proton-glorious-egroll-proton-ge)
    * [Protontricks](#protontricks)
    * [AMDGPU vs AMDGPU-PRO](#amdgpu-vs-amdgpu-pro)
* [Troubleshooting](#troubleshooting)
    * [Steam crash with Proton](#steam-crash-with-proton)

<!-- vim-markdown-toc -->

---
## Install

Make sure `tmpfs` is well mounted in your `fstab` (if not, add this line and reboot):
```console
# vi /etc/fstab

    > # to limit the size add e.g. size=768M to opts (default is 50%)
    >
    > #<fs>      <mountpoint>    <type> <opts>              <dump/pass>
    > shm        /dev/shm        tmpfs  nodev,nosuid,noexec 0 0
```

!!! Note ""

    === "Gentoo kernel"
        Make sure you activated the `tmpfs` kernel option `CONFIG_TMPFS` in your [kernel
        config](../distros/gentoo-based/gentoo_kernel.md#kernel-config):
        ```console
        $ cd /usr/src/linux
        # make nconfig # or `# make menuconfig`

            # Enable tmpfs support
            # Double check here: <https://wiki.gentoo.org/wiki/Steam#Kernel>
            # Double check here: <https://wiki.gentoo.org/wiki/Tmpfs#Installation>
            #
            > File systems  --->
            >      Pseudo filesystems  --->
            >           [*] Tmpfs virtual memory file system support (former shm fs) # Symbol: TMPFS [=y]
            >           [ ] Optional drivers
        ```

        !!! Warning "Warning"
            After configuring the kernel don't forget to do a [kernel make and
            rebuild](..//distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!

Now install steam and it's dependencies:

!!! Note ""

    === "emerge"
        ```console
        # emerge --ask --noreplace dev-vcs/git # emerge steam dependency
        # wget -P /etc/portage/repos.conf/ https://raw.githubusercontent.com/anyc/steam-overlay/master/steam-overlay.conf
        # emaint sync --repo steam-overlay
        # emerge --ask games-util/steam-launcher
        ```

    === "pacman"
        TODO

    === "apt"
        TODO

    === "yum"
        TODO

    === "dnf"
        TODO


---
## Config

### Avoid steam dotfile madness (still in progress)

Steam will create by default those dotfiles:

* `~/.steam`
* `~/.steampath` -> `$HOME/.steam/sdk32/steam`
* `~/.steampid` -> `$HOME/.steam/steam.pid`

Additionally, it will also create a home file:

* `~/steam.desktop`

I don't know what is the purpose of this last file (it might be just a desktop shortcut), but
apparently it can be moved without blowing up steam:
```console
$ mv steam.desktop .config
```


---
## Use

Run steam:
```console
$ steam
```

### Proton

???+ Note "Reference(s)"
    * <https://github.com/ValveSoftware/Proton>

Proton is a compatibility tool developed by Valve, for Steam, based on Wine and additional
components. It allow games which are exclusive to Windows to run on the Linux operating system.
(see compatibility list: <https://www.protondb.com/>).

#### Enable Proton

Proton needs to be enabled on Steam client:

* Steam > Settings > Steam Play

(You can enable Steam Play for games that have and have not been white listed by Valve in that
dialog)

#### Force Proton

If needed, to force enable Proton or a specific version of Proton for a game, right click on the
game, click Properties > General > Force the use of a specific Steam Play compatibility tool, and
select the desired version. Doing so can also be used to force games that have a Linux port to use
the Windows version.

#### Configure Proton

You can also configure some launch options for a game, right click on the game, click Properties >
General > SET LAUNCH OPTIONS... See
<https://github.com/ValveSoftware/Proton/#runtime-config-options>

Some various launch options:
```console
    > SET LAUNCH OPTIONS: PROTON_USE_WINED3D=1

    > SET LAUNCH OPTIONS: PROTON_LOG=1 %command%

    > SET LAUNCH OPTIONS: -nolauncher -nosplash -world=empty -skipintro

    > SET LAUNCH OPTIONS: PROTON_FORCE_LARGE_ADDRESS_AWARE=1 PROTON_USE_D9VK=1 PROTON_NO_ESYNC=1 %command% -nolauncher -nosplash -world=empty -skipintro

    Skip the game launcher and force BattlEye install:
    > SET LAUNCH OPTIONS: -noLauncher -useBE # to

    With dwm (<https://www.reddit.com/r/linux_gaming/comments/1j85oi/any_dwm_linux_gamer_how_do_you_play_games_with/>):
    > SET LAUNCH OPTIONS: -sw -noborder

    DXVK_HUD=fps,version This is a DXVK environment variables that generates on HUD on screen
    telling you what version of DXVK you are running and what your FPS are. This is useful imo.
    Here is an example i use for Hellblade again. Once again entered into the game properties as
    launch options from steam:

    > SET LAUNCH OPTIONS: DXVK_HUD=version %command%

    > SET LAUNCH OPTIONS: setxkbmap us && %command% ; setxkbmap fr
```

!!! Tip "dwm launch options"
    For dwm user: `%command% -sw -noborder` might be good launch options.

!!! Tip "Mordhau launch options"
    For Mordhau: `VK_ICD_FILENAMES="/opt/amdgpu-pro/etc/vulkan/icd.d/amd_icd64.json" %command% -sw
    -noborder`
    * getting the following problem for now: <https://github.com/ValveSoftware/Proton/issues/2629#issuecomment-623485348>

!!! Tip "Halo MCC launch options"
    `VK_ICD_FILENAMES="/opt/amdgpu-pro/etc/vulkan/icd.d/amd_icd64.json" %command% -sw -noborder`
    (see <https://github.com/ValveSoftware/Proton/issues/2907>)

!!! Tip "TESO launch options"
    `VK_ICD_FILENAMES="/opt/amdgpu-pro/etc/vulkan/icd.d/amd_icd64.json" %command% -sw -noborder`

    If you are stuck on the "loading" screen, this might be due to this bug
    <https://bugs.gentoo.org/727262> : ca-certificates might need to be downgraded. On Gentoo you
    can downgrade it this way:
    ```console
    $ cd /etc/portage
    $ sudo touch package.mask
    $ echo ">app-misc/ca-certificates-20190110.3.43" >> /etc/portage/package.mask
    # emerge -DuNv world
    ```

!!! Tip "Elite Dangerous launch options"
    See <https://www.protondb.com/users/287799351>
    See <https://gist.github.com/Eeyoresyndrome/cdaa34e4335b386feac8f194a01da5e2>

    Launch options:
    `VK_ICD_FILENAMES="/opt/amdgpu-pro/etc/vulkan/icd.d/amd_icd64.json" %command% -sw -noborder`

    Tricks:
    * Version of Proton:
    * Downgrade `dev-libs/nss` prior to 3.57:
    * Restore wine/proton state by removing prefix and launching the game:
      ```console
      $ export WINEPREFIX="$HOME/.local/share/Steam/steamapps/compatdata/<APPID>/pfx"
      or
      $ export WINEPREFIX="/media/tera/Steam/steamapps/compatdata/<APPID>/pfx"

      $ rm -rI "$WINEPREFIX"
      ```
    * Launch the game and let it fail.
    * Download last release of `wine-mono`: <https://github.com/redmcg/wine-mono/releases>
    * Find Elite Dangerous game id: `$ protontricks -s "Elite Dangerous"`, it should be 359320
    * Run `protontricks -c 'msiexec /i /media/tera/wine-mono-5.1.1.2_ED-x86.msi' 359320`
    * DEPRECATED:
        * Run `protontricks -c 'wine64 uninstaller' 359320` and uninstall any existing version of
          'Wine Mono Runtime' (by selecting it and clicking 'Remove'). Then Click 'Install...' and
          select the last release of `wine-mono-*.msi` just downloaded.


!!! Tip "Elite Dangerous alternative launch options"
    See <https://www.protondb.com/users/287799351>
    See <https://gist.github.com/Eeyoresyndrome/cdaa34e4335b386feac8f194a01da5e2>

    Launch options:
    `VK_ICD_FILENAMES="/opt/amdgpu-pro/etc/vulkan/icd.d/amd_icd64.json" %command% -sw -noborder`

    Tricks:
    * Version of Proton: `Proton-5.9-GE-8-ST`
    * Downgrade `dev-libs/nss` prior to 3.57:
    * Launch the game and let it fail.
    * Run `protontricks -c 'wine64 uninstaller' 359320` and uninstall any existing version of 'Wine
      Mono Runtime' (by selecting it and clicking 'Remove').
    * Run `$ protontricks 359320 -q dotnet472 win7` which can take a long time to complete...

### Proton Glorious Egroll (Proton-GE)

???+ Note "Reference(s)"
    * <https://github.com/GloriousEggroll/proton-ge-custom>

Proton-GE is a custom Proton build with the most recent release of vanilla wine, with `ffmpeg`
enabled for `faudio` by default, and all of Proton's patches ported over to be applied to wine, as
well as `wine-staging` and `vkd3d`.

Install Proton-GE (e.g. `5.4-GE-3`):
```console
$ wget https://github.com/GloriousEggroll/proton-ge-custom/releases/download/5.4-GE-3/Proton-5.4-GE-3.tar.gz

$ tar -zxvf Proton-5.4-GE-3.tar.gz
$ rm Proton-5.4-GE-3.tar.gz

$ mkdir -p ~/.steam/root/compatibilitytools.d
$ mv Proton-5.4-GE-3 ~/.steam/root/compatibilitytools.d/
```

And restart steam. Now you should able to force the use of Proton-GE, the same way you can force
the use of proton (see previous [Force Proton](#force-proton) section).

!!! Question ""
    Is `wine-mono` needed ?
    see <https://wiki.winehq.org/Mono>

### Protontricks

Protontricks is a simple wrapper that does winetricks things for Proton enabled games.

???+ Note "Reference(s)"
    * <https://github.com/Matoking/protontricks>

!!! Note ""

    === "emerge"
        ```console
        # emerge -a protontricks
        ```

The basic usage is as follows:

* Find your game's App ID by searching for it
```console
$ protontricks -s <GAME NAME>
```

* Run winetricks for the game
```console
$ protontricks <APPID> <ACTIONS>
```

* Run a custom command within game's installation directory
```console
$ protontricks -c <COMMAND> <APPID>
```

* Run the protontricks GUI
```console
$ protontricks --gui
```

* Print the protontricks help message
```console
$ protontricks --help
```

* Restore wine/proton state by removing prefix and relaunching the game:
```console
$ export WINEPREFIX="$HOME/.local/share/Steam/steamapps/compatdata/<APPID>/pfx"
or
$ export WINEPREFIX="/media/tera/Steam/steamapps/compatdata/<APPID>/pfx"

$ rm -rI "$WINEPREFIX" # then restart the game
```

### AMDGPU vs AMDGPU-PRO

!!! Note ""
    If you are using a Nvidia graphic card, this section doesn't concern you.

Users whose main use for their graphics card is gaming and other general home use graphical
hardware acceleration should use [open source AMDGPU
driver](https://wiki.gentoo.org/wiki/AMDGPU-PRO).

Thus, the [closed source AMDGPU-PRO driver](https://wiki.gentoo.org/wiki/AMDGPU-PRO) is mostly
useful to users who want to play games or use applications that rely on compatibility profiles, or
want to run anything that uses Vulkan with advanced shaders, or want to fully exploit OpenCL.

!!! Tip "Tip"
    So, in order to play to some games through compatibility tools like Proton: AMDGPU-PRO will be
    needed. Fortunately, it can be used per game, without being installed globally, like described
    below.

**How to use AMDGPU-PRO per game**:

* Find and download your AMDGPU-PRO drivers here: <https://www.amd.com/en/support> ("Search for
  your product" and "Submit")

* Download the Ubuntu x86 64-Bit latest version (if you are not on Debian, CentOS or RHEL, in this
  case download one of those version), e.g. `amdgpu-pro-20.20-1089974-ubuntu-20.04.tar.xz`.

* Unpack:
```console
$ tar xvfJ amdgpu-pro-20.20-1089974-ubuntu-20.04.tar.xz
```

* Unpack the Vulkan drivers deb file for 64-bit:
```console
$ ar x amdgpu-pro-20.20-1089974-ubuntu-20.04/vulkan-amdgpu-pro_20.20-1089974_amd64.deb
```

* Unpack the `data.tar.xz` file which was inside the deb file:
```console
$ tar xvfJ data.tar.xz
```

* Move the extracted folder `opt/amdgpu-pro` from the archive to `/opt/amdgpu-pro`:
```console
# mv opt/amdgpu-pro /opt
```

* Start steam (or the Vulkan based game) with the following command:
  ```console
  $ VK_ICD_FILENAMES=/opt/amdgpu-pro/etc/vulkan/icd.d/amd_icd64.json /usr/bin/steam
  ```
  **OR**
  Launch a game with this option
  `VK_ICD_FILENAMES="/opt/amdgpu-pro/etc/vulkan/icd.d/amd_icd64.json"`

!!! Note "Note"
    See Phoronix benchmarks here: <https://www.phoronix.com/scan.php?page=search&q=AMDGPU-PRO> for
    more comparisons.


---
## Troubleshooting

If any problem is encountered while playing:

* <https://wiki.gentoo.org/wiki/Steam#Client_troubleshooting>
* <https://wiki.archlinux.org/index.php/Steam/Troubleshooting>

### Steam crash with Proton

Proton isn't always stable, sometimes a game crashes: it might crash it's window, steam or the
graphic server...

If steam or the graphic server crashed, you might want to kill any `wine` related services left:
```console
$ pkill wineserver & pkill rpcss.exe & pkill explorer.exe & pkill svchost.exe & pkill winedevice.exe & pkill plugplay.exe & pkill services.exe & pkill bwrap`
```
Make sure they all have been killed by checking with `htop`.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
