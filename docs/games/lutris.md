---
tags:
  - Games
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Lutris

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Lutris>
    * <https://en.wikipedia.org/wiki/Lutris>
    * <https://lutris.net/>
    * <https://lutris.net/games>
    * <https://lutris.net/games/star-citizen/>
    * <https://www.youtube.com/watch?v=hMW_ruH2iHY>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
