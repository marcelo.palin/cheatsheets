---
tags:
  - Shells
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# McFly

McFly is a shell history search tool with an intelligent search engine that takes into account your
working directory and the context of recently executed commands. McFly's suggestions are
prioritized in real time with a small neural network.

???+ Note "Reference(s)"
    * <https://github.com/cantino/mcfly>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        ```console
        # emerge -a app-shells/mcfly
        ```

    === "pacman"
        ```console
        # pacman -S mcfly
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.mcfly
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.mcfly
            ```

    === "other"
        ```console
        $ curl -LSfs https://raw.githubusercontent.com/cantino/mcfly/master/ci/install.sh | sh -s -- --git cantino/mcfly
        ```

---
## Config

!!! Note ""

    === "bash"
        ```console
        $ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
            > ...
          + >
          + > # McFly
          + > eval "$(mcfly init bash)"
          + > # export MCFLY_KEY_SCHEME=vim
          + > # export MCFLY_FUZZY=2 # see https://github.com/cantino/mcfly/issues/183
          + > # export MCFLY_RESULTS=50 # change max number of results shown (default is 10)
        
        $ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
        ```

    === "zsh"
        ```console
        $ vi $HOME/.zshrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
            > ...
          + >
          + > # McFly
          + > eval "$(mcfly init zsh)"
          + > # export MCFLY_KEY_SCHEME=vim
          + > # export MCFLY_FUZZY=2 # see https://github.com/cantino/mcfly/issues/183
          + > # export MCFLY_RESULTS=50 # change max number of results shown (default is 10)
        
        $ source $HOME/.zshrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
        ```

    === "fish"
        ```console
        $ vi $HOME/.config/fish/config.fish
            > ...
          + >
          + > # McFly
          + > mcfly init fish | source
          + > # set -gx MCFLY_KEY_SCHEME vim
          + > # set -gx MCFLY_FUZZY 2
        
        $ source $HOME/.config/fish/config.fish
        ```

---
## Use

**TODO**

```console
$ mcfly -h
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
