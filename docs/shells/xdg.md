---
tags:
  - Shells
  - XDG
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# XDG Base Directory Specification

The XDG Base Directory Specification defines where some files should be looked for by defining one
or more base directories relative to which files should be located.

This specification defines a set of environment variables pointing programs to directories in which
their data or configuration should be stored. This can be useful in order [to try to take back control
of our home
directory](https://web.archive.org/web/20210807080152/https://0x46.net/thoughts/2019/02/01/dotfile-madness/).

???+ Note "Reference(s)"
    * <https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html>
    * <https://specifications.freedesktop.org/basedir-spec/latest/>
    * <https://wiki.archlinux.org/index.php/XDG_user_directories>
    * <https://wiki.archlinux.org/index.php/XDG_Base_Directory>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

Pre configure `xdg`:
```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever (⚠️  create it if not already present ⚠️ )
    > ...
  + >
  + > # XDG
  + > export XDG_CONFIG_HOME="$HOME/.config"
  + > export XDG_CACHE_HOME="$HOME/.cache"
  + > export XDG_DATA_HOME="$HOME/.local/share"
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
```

Install `xdg-user-dirs`:

!!! Note ""

    === "emerge"
        ```console
        $ sudo emerge -a xdg-user-dirs
        ```

    === "pacman"
        ```console
        $ sudo pacman -S xdg-user-dirs
        ```

    === "apt"
        ```console
        $ sudo apt install xdg-user-dirs
        ```

    === "yum"
        ```console
        $ sudo yum install xdg-user-dirs
        ```

    === "dnf"
        ```console
        $ sudo dnf install xdg-user-dirs
        ```

---
## Config

Configure your defaults folders and your defaults locales:
```console
$ xdg-user-dirs-update
$ vi $HOME/.config/user-dirs.dirs
    > ...
    > XDG_DESKTOP_DIR="$HOME/desktop"
    > XDG_DOWNLOAD_DIR="$HOME/downloads"
    > XDG_TEMPLATES_DIR="$HOME/templates"
    > XDG_PUBLICSHARE_DIR="$HOME/public"
    > XDG_DOCUMENTS_DIR="$HOME/documents"
    > XDG_MUSIC_DIR="$HOME/music"
    > XDG_PICTURES_DIR="$HOME/pictures"
    > XDG_VIDEOS_DIR="$HOME/videos"
    > ...

$ vi $HOME/.config/user-dirs.locale
    > ...
    > en_US ISO-8860-1
    > en_US.UTF-8 UTF-8
    > fr_FR ISO-8859-1
    > fr_FR@euro ISO-8859-15
    > fr_FR.UTF-8 UTF-8
    > ...
```


---
## Use

* Print list of available applications:
    ```console
    $ ls -la /usr/share/applications
    ```

* Print local mime config:
    ```console
    $ cat $HOME/.config/mimeapps.list
    ```

* Print default application openning HTTP and HTTPS links:
    ```console
    $ xdg-mime query default x-scheme-handler/http
    $ xdg-mime query default x-scheme-handler/https
    ```

* Change default application openning HTTP and HTTPS links to firefox:
    ```console
    $ xdg-mime default firefox.desktop x-scheme-handler/http
    $ xdg-mime default firefox.desktop x-scheme-handler/https
    ```

* Change default application openning `.pdf` files to evince (or fallback to firefox):
    ```console
    $ xdg-mime default org.gnome.Evince.desktop;firefox.desktop application/pdf
    ```

* Change default application openning `.jpeg` and `.png` files to feh (or fallback to firefox):
    ```console
    $ xdg-mime default feh.desktop;firefox.desktop image/jpeg
    $ xdg-mime default feh.desktop;firefox.desktop image/png
    ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
