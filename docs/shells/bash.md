---
tags:
  - Shells
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Bash

Bash is a command line shell/programming language (written by Brian Fox for the GNU Project). Its
name alludes to its predecessor, the long deprecated Bourne shell. Bash can be run on most Unix
like operating systems, including GNU/Linux. Bash is the default command line shell on most
GNU/Linux distributions.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Bash>
    * <https://wiki.gentoo.org/wiki/Bash>
    * <https://en.wikipedia.org/wiki/Bash_%28Unix_shell%29>
    * `$ man bash`

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Bash tips](#bash-tips)
* [Bash scripts references](#bash-scripts-references)

<!-- vim-markdown-toc -->

---
## Bash tips

* `Ctrl-R`: reverse search
* `Ctrl-S`: forward search
* `Ctrl-U`: clean line (also work when entering passwords)
* `Ctrl-L`: clean terminal
* `Ctrl-A`: move cursor at the beginning of the line
* `Ctrl-C`: cancel current line

---
## Bash scripts references

* <https://web.archive.org/web/20220512171601/https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/>
* <https://web.archive.org/web/20220512171557/https://sipb.mit.edu/doc/safe-shell/>
* <https://google.github.io/styleguide/shellguide.html>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
