---
tags:
  - Servers
  - VPS
  - Domain Names
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# OVH

[OVH](https://ovh.com), is a French cloud computing company which offers VPS, dedicated servers and
other web services.

This cheat sheet will focus on various tips related to some OVH services.

???+ Note "Reference(s)"
    * [IPV6 configuration](https://docs.ovh.com/gb/en/vps/configuring-ipv6/#persistent-application-on-debian-and-its-derivatives-ubuntu-crunchbang-steamos-etc)
    * <https://docs.ovh.com/gb/en/domains/web_hosting_how_to_edit_my_dns_zone/>
    * <https://web.archive.org/web/20190514120914/https://www.yabage.me/2014/05/09/update-ovh-dynhost-address-from-linux/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [DNS zone](#dns-zone)
* [DynHost](#dynhost)

<!-- vim-markdown-toc -->

---
## DNS zone

See <https://docs.ovh.com/gb/en/domains/web_hosting_how_to_edit_my_dns_zone/#dns-records> to
understand type of DNS record in the DNS zone.

---
## DynHost

!!! Note
    DynHost is the name given by OVH to the service allowing to regularly update the IP address
    attributed to a domain name (for those who knows DynDNS, it corresponds to the same service
    offered by DynDNS). It is targeted for people who have an Internet subscription with dynamic IP
    addressing, wishing to access their computer network from outside or hosting any server to the
    Internet.

With DynHost, you can point a sub-domain to a dynamic IP address that will be updated in your
associated OVH DNS each time it changes.

In order to update the DNS from your server, a simple script is needed:

* As a first prerequisite, the [`bind*`](https://repology.org/project/bind/versions) package will
  be needed (in order to get the `dig` tool):

!!! Note "Install `dig`"

    === "emerge"
        ```console
        #emerge -a net-dns/bind-tools # for the `dig` tool
        ```

    === "pacman"
        ```console
        # pacman -S bind
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.bind
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.bind
            ```

    === "apt"
        ```console
        # apt install bind9
        ```

    === "yum"
        ```console
        # yum install bind
        ```

    === "dnf"
        ```console
        # dnf install bind
        ```

* As a second prerequisite, you need to create dedicated credentials associated to your OVH DynHost
  like describe
  [here](https://web.archive.org/web/20211107005620/https://docs.ovh.com/us/en/domains/hosting_dynhost/).
  Remember those credentials, they will be needed in the bellow script (`LOGIN` and `PASSWORD`
  variables).

* Now, you can simply create the following script (don't forget to change the `OVH_DYN_HOST`,
  `LOGIN` and `PASSWORD` variables - inside the script - according to your needs):
```console
$ sudo vi /opt/update-ovh-dyn-host-your-sub-domain.name.ovh
    > #!/bin/bash
    >
    > #⚠️ Prerequisites: bash, realpath, dig, curl, date
    >
    > OVH_DYN_HOST='your-sub-domain.name.ovh'
    > LOGIN='your-sub-domain-login'
    > PASSWORD='your-sub-domain-password'
    >
    > SCRIPT_LOC="$(realpath $0)"
    > PATH_LOG=/var/log/update-ovh-dyn-host-"$OVH_DYN_HOST".log
    >
    > OVH_DYN_HOST_IP=$(dig +short $OVH_DYN_HOST A)
    > CURRENT_IP=$(curl --ipv4 --silent --max-time 5 "ifconfig.io" 2>/dev/null)
    > CURRENT_DATETIME=$(date -R)
    >
    > valid_ipv4() {
    >     ip=$1
    >     if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]
    >     then
    >         return 0
    >     else
    >         return 1
    >     fi
    > }
    >
    > if [[ -z $OVH_DYN_HOST_IP ]] || ! valid_ipv4 "$OVH_DYN_HOST_IP"
    > then
    >     echo "[$CURRENT_DATETIME] $SCRIPT_LOC: not updating $OVH_DYN_HOST OVH DynHost, because a wrong OVH DynHost IP has been retrieved!" >> $PATH_LOG
    >     return 1
    > fi
    >
    > if [[ -z $CURRENT_IP ]] || ! valid_ipv4 "$CURRENT_IP"
    > then
    >     echo "[$CURRENT_DATETIME] $SCRIPT_LOC: not updating $OVH_DYN_HOST OVH DynHost, because a wrong (current) host IP has been retrieved!" >> $PATH_LOG
    >     return 1
    > fi
    >
    > if [ "$OVH_DYN_HOST_IP" != "$CURRENT_IP" ]
    > then
    >     UPDATE_REQUEST=$(curl -m 5 -L --location-trusted --user "$LOGIN:$PASSWORD" "https://www.ovh.com/nic/update?system=dyndns&hostname=$OVH_DYN_HOST&myip=$CURRENT_IP")
    >     echo "[$CURRENT_DATETIME] $SCRIPT_LOC: updating $OVH_DYN_HOST OVH DynHost IPv4 from $OVH_DYN_HOST_IP to $CURRENT_IP - request result: $UPDATE_REQUEST" >> $PATH_LOG
    > else
    >     echo "[$CURRENT_DATETIME] $SCRIPT_LOC: no OVH DynHost update needed for $OVH_DYN_HOST !" >> /dev/null # this message doesn't need to be logged
    > fi
```

* Make sure the script is executable and is owned by `root`:
```console
$ sudo chmod +x /opt/update-ovh-dyn-host-your-sub-domain.name.ovh
$ sudo chown root:root /opt/update-ovh-dyn-host-your-sub-domain.name.ovh
```

* Add it to the `root` user `crontab` (e.g. in order to run it every minute):
```console
$ sudo crontab -e # run the script every minute
    > # * * * * *  command_or_script_to_execute
    > # - - - - -
    > # | | | | |
    > # | | | | +- day of week (0 - 7) (where sunday is 0 and 7)
    > # | | | +--- month (1 - 12)
    > # | | +----- day (1 - 31)
    > # | +------- hour (0 - 23)
    > # +--------- minute (0 - 59)
    > ...
    > */1 * * * * /opt//opt/update-ovh-dyn-host-your-sub-domain.name.ovh
```

!!! Tip
    If you still don't have `cron` installed, you might want to look at my [`cron`](../admin/cron.md)
    cheat sheet.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
