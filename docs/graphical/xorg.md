---
tags:
  - Graphical
  - Xorg
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Xorg

Xorg (or X) is a free and open source implementation of the [X Window System (or
X11)](https://en.wikipedia.org/wiki/X_Window_System), which allows users to have a graphical
environment.

Those wiki pages might help to find out what configuration suits you the best:

* <https://wiki.gentoo.org/wiki/Xorg/Guide>
* <https://wiki.gentoo.org/wiki/Radeon>
* <https://wiki.gentoo.org/wiki/AMDGPU>
* <https://wiki.gentoo.org/wiki/Nouveau>
* <https://wiki.gentoo.org/wiki/NVidia/nvidia-drivers>

???+ Note "Reference(s)"
    * <https://www.x.org/wiki/>
    * <https://en.wikipedia.org/wiki/X_Window_System>
    * <https://en.wikipedia.org/wiki/X.Org_Server>
    * <https://gitlab.freedesktop.org/xorg/xserver>
    * <https://en.wikipedia.org/wiki/X.Org_Foundation>
    * <https://wiki.gentoo.org/wiki/Xorg/Guide>
    * <https://wiki.gentoo.org/wiki/Xorg>
    * <https://wiki.gentoo.org/wiki/Xorg.conf>
    * <https://wiki.archlinux.org/title/Xorg>
    * <https://cyber.dabamos.de/unix/x11/>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [TODO](#todo)
* [Install](#install)
* [Config](#config)
    * [Intel](#intel)
    * [AMD](#amd)
    * [Nvidia](#nvidia)
* [Use](#use)
    * [Troubleshooting](#troubleshooting)
        * [`(EE) no screens found` and `(EE) Failed to load module 'nvidia'` error](#ee-no-screens-found-and-ee-failed-to-load-module-nvidia-error)
        * [Unsupported high key code 372](#unsupported-high-key-code-372)
        * [CONFIG_CHECKPOINT_RESTORE: is not set when it should be.](#config_checkpoint_restore-is-not-set-when-it-should-be)
    * [X11 forwarding](#x11-forwarding)
    * [Useful X11 apps](#useful-x11-apps)
    * [Testing the video card](#testing-the-video-card)
    * [Multiple graphic cards](#multiple-graphic-cards)
    * [Multiseat](#multiseat)

<!-- vim-markdown-toc -->

---
## TODO

* Replace mesa by ROCm like described here: <https://wiki.gentoo.org/wiki/OpenCL#AMD> ? Replace or
  just add with the appropriate use flags? And how to use `ocl-icd`
  <https://wiki.gentoo.org/wiki/OpenCL#ICD> ?

* <https://www.gentoo.org/support/news-items/2020-04-22-opencl-upgrade-file-collisions.html>
* <https://wiki.gentoo.org/wiki/OpenCL>
* <https://wiki.gentoo.org/wiki/OpenCL#AMD>
* <https://github.com/RadeonOpenCompute/ROCm>
* <https://rocmdocs.amd.com/en/latest/Installation_Guide/Installation-Guide.html>
* Note: I'm using AMDGPU <https://wiki.gentoo.org/wiki/AMDGPU>

```console
In order to take advantage of OpenCL you will need a runtime for your hardware.
Currently included in Gentoo are:

* open:

    * dev-libs/intel-neo - integrated Intel GPUs from Broadwell onwards. 64-bit only;

    * dev-libs/rocm-opencl-runtime - AMD GPUs supported by the amdgpu kernel driver.
      Image support still requires a proprietary extension [1]. 64-bit only;

    * media-libs/mesa[opencl] - some older AMD GPUs; see [2]. 32-bit support;

* proprietary:

    * dev-libs/amdgpu-pro-opencl - AMD Polaris GPUs. 32-bit support;

    * dev-util/intel-ocl-sdk - Intel CPUs (*not* GPUs). 64-bit only;

    * x11-drivers/nvidia-drivers[uvm] - Nvidia GPUs; specific package versions
      required for older devices [3]. 32-bit support.

[1] dev-libs/hsa-ext-rocr
[2] <https://dri.freedesktop.org/wiki/GalliumCompute/>
[3] <https://www.nvidia.com/en-us/drivers/unix/legacy-gpu/>
```

```console
Messages for package media-libs/mesa-19.3.5:

CONFIG_CHECKPOINT_RESTORE: is not set when it should be.
Please check to make sure these options are set correctly.
Failure to do so may cause unexpected problems.
```

I removed the following portage mask:
```console
    !! The following installed packages are masked:
    - dev-libs/rocm-opencl-driver-2.9.0::gentoo (masked by: package.mask)
    /usr/portage/profiles/package.mask:
    # Craig Andrews <candrews@gentoo.org> (2020-03-03)
    # Deprecated upstream in favor of dev-libs/rocm-comgr
    # Masked for removal in 30 days. Bug #711398
    
    For more information, see the MASKED PACKAGES section in the emerge
    man page or refer to the Gentoo Handbook.
```
    * The bug is <https://bugs.gentoo.org/711398>
    * The bug has been closed so `rocm-opencl-driver` has never been removed
    * Still, is it interesting to replace `dev-libs/rocm-opencl-driver` by `dev-libs/rocm-comgr`?

Installed ROCm like described in crypto mining cheat sheet and here:
<https://wiki.gentoo.org/wiki/OpenCL#AMD>. Add USE flag `-opencl` in `/etc/portage/package.use/mesa`
(and re compile mesa after that).

Now
```console
$ clinfo # OK
$ rocminfo # OK
```

It looks OK.

TODO: <https://wiki.gentoo.org/wiki/OpenCL#Implementation_validation>


---
## Install

!!! Note ""

    === "Gentoo kernel"
        A correct [kernel config](../distros/gentoo-based/gentoo_kernel.md#kernel-config) is
        needed:
        ```console
        $ cd /usr/src/linux
        # make nconfig # or `# make menuconfig`
        
            # Base config for any drivers
            #############################
            # Double check here: <https://wiki.gentoo.org/wiki/Xorg/Guide#Input_driver_support>
            #
        
            # Enable evdev in the kernel:
            #
            > Device Drivers --->
            >    Input device support --->
            >    <*>  Event interface # Symbol: INPUT_EVDEV [=y]
        
            # Disable legacy framebuffer support and enable basic console FB support:
            #
            > Device Drivers --->
            >    Graphics support --->
            >       Frame Buffer Devices --->
            >          <*> Support for frame buffer devices ---> # Symbol: FB [=y]
            #
            # Disable all drivers (all "*** Frame buffer hardware drivers ***"), including VGA,
            # Intel, NVIDIA, and ATI, except EFI-based Framebuffer Support, if you are using UEFI.
        
            # Enable basic console support. KMS uses this.
            #
            > Device Drivers --->
            >    Graphics support --->
            >       Console display driver support --->
            >          <*>  Framebuffer Console Support # Symbol: FRAMEBUFFER_CONSOLE [=y]
            #############################
        
            # Complementary config for open source amdgpu drivers
            #####################################################
            # Double check here: <https://wiki.gentoo.org/wiki/AMDGPU#Kernel>
            #
        
            > Processor type and features  --->
            >     [*] MTRR (Memory Type Range Register) support # Symbol: MTRR [=y]
            >
            > Device Drivers  --->
            >     Graphics support  --->
            >         <*/M> Direct Rendering Manager (XFree86 4.1.0 and higher DRI support) ---> # Symbol: DRM [=y]
            >               [*]   Enable legacy fbdev support for your modesetting driver # Symbol: DRM_FBDEV_EMULATION [=y]
            >         <   > ATI Radeon # Symbol: DRM_RADEON [=n]
            >         <M/*> AMD GPU # Symbol: DRM_AMD_GPU [=y]
            >
            >               [ /*] Enable amdgpu support for SI parts # Symbol: DRM_AMDGPU_SI
            #                     (only needed for Southern Islands GPUs with the amdgpu driver)
            >
            >               [ /*] Enable amdgpu support for CIK parts # Symbol: DRM_AMDGPU_CIK
            #                     (only needed for Sea Islands GPUs with the amdgpu driver)
            >
            >               ACP (Audio CoProcessor) Configuration  --->
            >                   [*] Enable AMD Audio CoProcessor IP support # Symbol: DRM_AMD_ACP [=n]
            #                       (only needed for APUs)
            >
            >               Display Engine Configuration  --->
            >                   [*] AMD DC - Enable new display engine # Symbol: DRM_AMD_DC [=y]
            >                   [ /*] DC support for Polaris and older ASICs # not found
            #                        (only needed for Polaris, Carrizo, Tonga, Bonaire, Hawaii)
            >
            >                   [ /*] AMD FBC - Enable Frame Buffer Compression # not found
            >                   [ /*] DCN 1.0 Raven family # not found
            #                        (only needed for Vega RX as part of Raven Ridge APUs)
            >
            >         <*/M> HSA kernel driver for AMD GPU devices # Symbol: HSA_AMD
            >     <*/M> Sound card support  ---> # Symbol: SOUND [=y]
            >         <*/M> Advanced Linux Sound Architecture  ---> # Symbol: SND [=y]
            >             [*]   PCI sound devices ---> # Symbol: SND_PCI [=y]
            >                   HD-Audio  --->
            >                       <*> HD Audio PCI # Symbol: SND_HDA_INTEL [=y]
            >                       [*] Support initialization patch loading for HD-audio # Symbol: SND_HDA_PATCH_LOADER [=y]
            >                       <*> whatever audio codec your soundcard needs (your can even select every "Build*"
            >                       <*> Build HDMI/DisplayPort HD-audio codec support # Symbol: SND_HDA_CODEC_HDMI [=y]
            >                   (2048) Pre-allocated buffer size for HD-audio driver # Symbol: SND_HDA_PREALLOC_SIZE [=2048]
            #
            # ⚠️
            # In addition you will have to incorportate the firmware:
            # <https://wiki.gentoo.org/wiki/AMDGPU#Incorporating_firmware>
            # ⚠️
            #####################################################
        
            # Complementary config for open source nvidia drivers (nouveau)
            ###############################################################
            # Double check here: <https://wiki.gentoo.org/wiki/Nouveau#Kernel>
            #
            > Device Drivers  --->
            >    Graphics support  --->
            >       <*> Direct Rendering Manager (XFree86 4.1.0 and higher DRI support) ---> # Symbol: DRM [=y]
            >          <*> Enable legacy fbdev support for your modesetting driver # Symbol: DRM_FBDEV_EMULATION [=y]
            >       <M/*> Nouveau (NVIDIA) cards # Symbol: DRM_NOUVEAU
            ###############################################################
        
            # Complementary config for proprietary nvidia drivers
            #####################################################
            # Double check here: <https://wiki.gentoo.org/wiki/NVIDIA/nvidia-drivers#Kernel>
            #
        
            # Enable loadable module support:
            > [*] Enable loadable module support ---> # Symbol: MODULES [=y]
        
            # Enable Memory Type Range Register (MTRR) support:
            > Processor type and features --->
            >    [*] MTRR (Memory Type Range Register) support # Symbol: MTRR [=y]
        
            # Enable VGA Arbitration:
            > Device Drivers --->
            >    Graphics support --->
            >       [*] VGA Arbitration # Symbol: VGA_ARB [=y]
        
            # Enable IPMI message handler
            >    Character devices --->
            >       [*] IPMI top-level message handler # Symbol IPMI_HANDLER [=y]
        
            # Disable support for the in-kernel framebuffer driver
            # (wich conflicts with the proprietary nvidia driver):
            > Device Drivers --->
            >    Graphics support --->
            >        Frame buffer Devices --->
            >            <*> Support for frame buffer devices ---> # Symbol: FB [=y]
            >            < >   nVidia Framebuffer Support # Symbol: FB_NVIDIA [=n]
            >            < >   nVidia Riva support # Symbol: FB_RIVA [=n]
        
            # Make sure nouveau driver is disabled:
            > Device Drivers --->
            >    Graphics support --->
            >        < > Nouveau (nVidia) cards # Symbol: DRM_NOUVEAU [=n]
        
            # Enable simple framebuffer (workaround of the proprietary conflict):
            > Bus options (PCI etc.)  --->
            >    [*] Mark VGA/VBE/EFI FB as generic system framebuffer # Symbol: X86_SYSFB [=y]
            > Device Drivers --->
            >    Graphics support --->
            >         Frame buffer Devices --->
            >             [*] Simple framebuffer support # Symbol: FB_SIMPLE [=y]
        
            # linux-firmware support (⚠️ optional proprietary blob).
            ########################
            # This might be needed along with some proprietary drivers.
            # Double check here: <https://wiki.gentoo.org/wiki/Linux_firmware#Kernel>
        
            > Device Drivers  --->
            >   Generic Driver Options  --->
            >     Firmware loader --->
            >        -*- Firmware loading facility # Symbol: FW_LOADER [=y]
            >
            >        #####
            >        # ⚠️ this option might need to be fileld ⚠️
            >        # for example, see <https://wiki.gentoo.org/wiki/AMDGPU#Incorporating_firmware>
            >        () Build named firmware blobs into the kernel binary # Symbol: EXTRA_FIRMWARE [=...]
            >        #####
            >
            >        (/lib/firmware) Firmware blobs root directory # Symbol: EXTRA_FIRMWARE_DIR [=/lib/firmware]
            ########################
        ```

        !!! Warning "Warning"
            After configuring the kernel don't forget to do a [kernel make and
            rebuild](..//distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!

!!! Note ""

    === "emerge - portage"
        In some cases, proprietary drivers (binary blobs) are needed. If so, installing
        linux-firmware might be needed for better compatibility:
        ```console
        # emerge -a linux-firmware # <https://wiki.gentoo.org/wiki/Linux_firmware>
        ```
        Configure your `make.conf` file accordingly:
        ```console
        # vi /etc/portage/make.conf # e.g. with nvidia proprietary
            > ...
            > video_cards="nvidia"  # for nvidia graphic card proprietary drivers
            > ...
        
        # emerge --ask --changed-use --deep @world # update the system
        
        # emerge -a x11-base/xorg-drivers
        # emerge -a x11-base/xorg-server
        
        # env-update
        # source /etc/profile
        
        # gpasswd -a username video # give video access to username, this user should logout and log back in
        ```

    === "pacman"
        Install the `xorg-server` package and `xorg-xinit` (for `startx`):
        ```console
        # pacman -S xorg-server xorg-xinit
        ```
        See <https://wiki.archlinux.org/index.php/Xorg> for more details


---
##  Config

### Intel

!!! Note ""

    === "Arch-based"
        If `# lspci | grep -e VGA -e 30` indicates an intel card, then install the intel driver:
        ```console
        # pacman -S xf86-video-intel
        ```

### AMD

!!! Note ""

    === "Arch-based"
        * If `# lspci | grep -e VGA -e 30` indicates an amdgpu card, then install the amdgpu
          driver:
        ```console
        # pacman -S xf86-video-amdgpu
        ```
        * If `# lspci | grep -e VGA -e 30` indicates an ati card, then install the ati driver:
        ```console
        # pacman -S xf86-video-ati
        ```

### Nvidia

!!! Note ""

    === "Gentoo-based"
        Enable opengl and/or? opencl, make sure the xorg server is not running during these
        changes:
        ```console
        # nvidia-xconfig
        # eselect opengl set nvidia
        and/or?
        # eselect opencl set nvidia
        ```


---
## Use

Now try to start the x server:
``` console
$ touch ~/.xinitrc
$ startx
    > ...
$ echo $?
    > 0
```

If no windows manager is installed: the screen should blink... And should goes back to the tty.

The Xorg server should have prompt something like this (you can also find it here:
`/var/log/Xorg.0.log`):
``` console
    > ...
    > Server terminated successfully (0). Closing log file.
```

If it prompted something else, troubleshooting might be necessary:
<https://wiki.gentoo.org/wiki/Xorg/Guide>

If the Xorg server just *blinked*, this might be because the `.xinitrc` file does not start a
windows manager. A windows manager, e.g.  [`dwm`](./suckless_tools.md#dwm), should be installed and
executed from `.xinitrc`:
``` console
$ echo "exec dwm" >> ~/.xinitrc # here dwm needs to be installed before startx again
```

### Troubleshooting

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/NVIDIA/nvidia-drivers#Troubleshooting>

#### `(EE) no screens found` and `(EE) Failed to load module 'nvidia'` error

If Xorg can't find your screen: `(EE) no screens found`, **and** also tells you that it can't load
your Nvidia module: `(EE) Failed to load module "nvidia" (module does not exist, 0)` in the Xorg
log file `/var/log/Xorg.0.log`, **but** `$ lsmod | grep "nvidia"` does return results like those:
``` console
$ lsmod | grep "nvidia"
    > nvidia_drm             45056  2
    > nvidia_modeset       1073152  3 nvidia_drm
    > nvidia              19902464  84 nvidia_modeset
```

This might happen after a `nvidia-drivers` update: just find the new path to your Nvidia drivers
(e.g. `/usr/lib64/xorg/modules`) and add this path to your `Xorg.conf` file:
``` console
# vi /etc/X11/xorg.conf
    > ...
    > Section "Files"
    >     ModulePath      "/usr/local/lib/xorg/modules"
    >     ModulePath      "/usr/lib/xorg/extra-modules"
    >     ModulePath      "/usr/lib/xorg/modules"
    >     ModulePath      "/usr/lib64/xorg/modules"
    > EndSection
    > ...
```

???+ Note "Reference(s)"
    * <https://forums.gentoo.org/viewtopic-t-378516-highlight-warning+module+nvidia.html>
    * <https://github.com/NVIDIA/nvidia-docker/issues/296>

#### Unsupported high key code 372

If you spot the following X11 error after `starx`: `Unsupported high keycode 372`, then see
<https://www.mythtv.org/wiki/Remapping_remote_control_key_codes_greater_than_255>

#### CONFIG_CHECKPOINT_RESTORE: is not set when it should be.

???+ Note "Reference(s)"
    * <https://gitweb.gentoo.org/repo/gentoo.git/commit/?id=ee5e5804a9a10db5fd95022efe5e9217b56dd6d1>

If using mesa with AMDGPU, one should enable `CONFIG_CHECKPOINT_RESTORE=y` by [configuring the
kernel](../distros/gentoo-based/gentoo_kernel.md#kernel-config).

### X11 forwarding

On the remote computer:

* install `xauth` (`xorg-xauth`) and `xhost` (`xorg-xhost`) packages
* in `/etc/ssh/sshd_config`:
    * set `X11Forwarding` to `yes`
    * verify that `AllowTcpForwarding` and `X11UseLocalhost` options are set to `yes` (defaults
      values)
    * verify `X11DisplayOffset` is set to `10` (default value)
* restart the `sshd` daemon.

On the client computer:

* in `/etc/ssh/ssh_config`:
    * set `ForwardX11` to `yes`
* install the `xauth` (`xorg-xauth`) package
    * enable the `ForwardX11` option by either specifying the `-X` switch on the command line for
      opportunistic connections, or by setting `ForwardX11` to `yes` in the client's configuration.
    * if GUI is drawing badly or you receive errors, then enable the `ForwardX11Trusted` option
      (`-Y` switch on the command line) this will prevent X11 forwarding from being subjected to
      the X11 SECURITY extension controls

Test by running `xclock`:
```console
$ ssh -X user@server-address xclock
or
$ ssh -X user@server-address
    >$ xclock
```

### Useful X11 apps

**TODO**: create dedicated sheet cheats for each app below

* `xprop` to display window and font properties in an X server:
```console
$ xprop
```

* `xset`, e.g. to disable screen saver blinking:
```console
$ xset s off
```

* `xkill` to kill any x app by clicking on it:
```console
$ xkill
```

* `xzoom` to zoom in:
```console
$ xzoom
```

* `xev` to get X11 input key codes:
```console
$ xev
```

* [`x2x`](./x2x.md) to allows the keyboard and mouse on one ("from") X display to be used to
  control another ("to") X display.

* Also see a lot of cool X11 tools
  [here](https://web.archive.org/web/20211210165829/https://cyber.dabamos.de/unix/x11/) and
  [here](https://web.archive.org/web/20211225153047/https://kkovacs.eu/cool-but-obscure-unix-tools).

### Testing the video card

A windows manager should be installed and the Xorg server should be running before trying running
those tests:
``` console
# emerge x11-apps/mesa-progs # install mesa programms to test the video card drivers

$ glxinfo | grep direct #
$ glx glxgears          # to test direct rendering (should be yes) and fps

$ vblank_mode=0 glxgears

$ glxinfo|egrep "OpenGL vendor|OpenGL renderer" # to the current active chip

# emerge -a x11-app/setxkdbm # e.g. 'setxkbmap fr' when starting x
```

**For more advanced GPU testing, see** <https://blog.microlinux.fr/gpu-benchmark-linux/>

### Multiple graphic cards

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Xorg#More_than_one_graphics_card>
    * <https://wiki.archlinux.org/title/Xorg_multiseat>

### Multiseat

Multiseat is a certain setup where N users work simultaneously on one computer. This is achieved by
having N monitors and N keyboards (and optionally N mice).

TODO

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Multiseat>
    * <https://wiki.archlinux.org/index.php/Xorg_multiseat>


Compiling `xorg-server` with the `xephyr` USE flag will allow to use the `xephyr` tool, is it
possible to use it to get Multiseat with one graphics card and two monitors working like here:
<https://bbs.archlinux.org/viewtopic.php?id=209727>

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Xephyr>
    * <https://wiki.debian-fr.xyz/Multiposte_avec_une_carte_graphique>
    * ? <https://nims11.wordpress.com/2012/06/24/nested-x-servers-with-xephyr/>

And compiling it with `xnest`?


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
