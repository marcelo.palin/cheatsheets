---
tags:
  - Sound Systems
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# PulseAudio

TODO

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/PulseAudio>
    * <https://wiki.archlinux.org/title/PulseAudio/Examples>
    * <https://wiki.archlinux.org/title/PulseAudio/Troubleshooting>
    * <https://wiki.gentoo.org/wiki/PulseAudio>
    * <https://www.freedesktop.org/wiki/Software/PulseAudio/>
    * <https://www.freedesktop.org/wiki/Software/PulseAudio/FAQ/>
    * <https://cgit.freedesktop.org/pulseaudio/pulseaudio/>
    * <https://gavv.github.io/articles/pulseaudio-under-the-hood/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Examples](#examples)
    * [Troubleshooting](#troubleshooting)

<!-- vim-markdown-toc -->

---
## Install

TODO

---
## Config

TODO

---
## Use

TODO

### Examples

See <https://wiki.archlinux.org/title/PulseAudio/Examples>.

### Troubleshooting

See <https://wiki.archlinux.org/title/PulseAudio/Troubleshooting>.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
