---
tags:
  - Security
  - Linux Security Modules
  - Mandatory Access Control
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# SELinux

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/SELinux>
    * <https://wiki.gentoo.org/wiki/SELinux>
    * <https://wiki.archlinux.org/index.php/SELinux>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
