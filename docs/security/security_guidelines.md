---
tags:
  - Security
  - Guidelines
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Security guidelines

???+ Note "Reference(s)"
    * <https://wonderfall.space/linux-securite/>
    * <https://www.it-connect.fr/details-durcissement-sysctl-reseau-linux/>
    * <https://www.ssi.gouv.fr/uploads/2016/01/linux_configuration-fr-v1.2.pdf>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
