---
tags:
  - Security
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# OpenSSL

🚧 WIP 🚧

* `$ man update-ca-trust` 🔥
* <https://wiki.archlinux.org/title/OpenSSL>

alternative : <https://wiki.gentoo.org/wiki/LibreSSL>


```console
$ pacman -S ca-certificates
$ pacman -S openssl
```

The downloaded file, which usually has a .crt or .pem suffix, can be validated through the openssl
command. To view certificate information (including the serial number):
```console
$ openssl x509 -in DigiCertHighAssuranceEVRootCA.crt -text
```

To view the thumbprint:
```console
$ openssl x509 -in DigiCertHighAssuranceEVRootCA.crt -noout -sha1 -fingerprint
```

Formats

Certificates come in many different file formats, often use inconsistent or incorrect file
extensions, and tools might expect inputs in specific formats. Try not to assume too much based
purely on the file extension. Start with a tool like less or file to try to figure out what format
it's in.

DER format is a binary format meant only for tools to read. It is usually possible to convert it to
a PEM format that contains the same information, but as plain text. Some tools only accept PEM, so
try converting it:
```console
$ openssl x509 -inform der -in "DER file" -out "PEM file"
```

Once it's in the PEM format, view it:
```console
$ openssl x509 -hash -text -noout -in "PEM file"
```

How to install certificates:
```console
$ man update-ca-trust # or `$ man update-ca-update` (depending on your distro)
$ cd /etc/ca-certificates/trust-source/anchors
$ sudo cp /path/to/certif.pem .
$ sudo update-ca-trust extract # or `$ sudo update-ca-update extract` (depending on your distro)
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
