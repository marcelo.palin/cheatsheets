---
tags:
  - Security
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# crowdsec

**TODO**

- <https://www.crowdsec.net/>

free account allows to get a dynamic list of banned IPs. A lot of major ISPs and other small and
big enterprises are contributing to this list


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
