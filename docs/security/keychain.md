---
tags:
  - Security
  - Privacy
  - Cryptography
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# keychain

Keychain helps you to manage SSH and GPG keys in a convenient and secure manner. It acts as a
front-end to `ssh-agent` and `ssh-add`, but allows you to easily have one long running ssh-agent
process per system, rather than the norm of one ssh-agent per login session. This dramatically
reduces the number of times you need to enter your passphrase. With keychain, you only need to
enter a passphrase once every time your local machine is rebooted.

???+ Note "Reference(s)"
    * <https://www.funtoo.org/Funtoo:Keychain>
    * <https://github.com/funtoo/keychain>
    * <https://wiki.gentoo.org/wiki/Keychain>
    * <https://stackoverflow.com/a/24902046>

???+ Note "Alternative(s)"
    * See the [gpg-agent section](./gnupg.md#gpg-for-ssh-with-gpg-agent) of the associated [GnuPG
      cheat sheet](./gnupg.md) (see also <https://wiki.archlinux.org/title/GnuPG#gpg-agent> and
      <https://wiki.gentoo.org/wiki/GnuPG#Using_a_GPG_agent>)
    * See the [ssh-agent section](../network/ssh.md#ssh-agent) of the associated [SSH cheat
      sheet](../network/ssh.md) (see also <https://wiki.archlinux.org/title/SSH_keys#SSH_agents>
      and <https://wiki.gentoo.org/wiki/SSH#ssh-agent>)
    * Anything that implements the [D-Bus Secret Service
      API](https://specifications.freedesktop.org/secret-service/latest/), is a valid alternative.
    * [ssh-find-agent](https://github.com/wwalker/ssh-find-agent)
    * [envoy](https://github.com/vodik/envoy)
    * [seahorse](https://gitlab.gnome.org/GNOME/seahorse)
    * [GNOME keyring](https://wiki.archlinux.org/title/GNOME/Keyring)
    * [lssecret](https://gitlab.com/GrantMoyer/lssecret)
    * [secret-tool](https://man.archlinux.org/man/secret-tool.1.en)
    * [LXQT wallet](https://github.com/lxqt/lxqt_wallet)
    * [secretsd](https://github.com/grawity/secretsd)
    * [libsecret](https://wiki.gnome.org/Projects/Libsecret)
    * [gkeyring](https://github.com/kparal/gkeyring)
    * [gnome-keyring-cli](https://github.com/drafnel/gnome-keyring-cli)


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "apk"
        ```console
        # apk add keychain
        ```

    === "apt"
        ```console
        # apt install cronie
        ```

    === "dnf"
        ```console
        # dnf install cronie
        ```

    === "emerge"
        ```console
        # emerge -a net-misc/keychain
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.keychain
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.keychain
            ```

    === "pacman"
        ```console
        # pacman -S keychain
        ```

    === "yum"
        **TODO**

    === "xbps"
        ```console
        # xbps-install -S keychain
        ```

    === "zypper"
        ```console
        # zypper install keychain
        ```


---
## Config

Assuming you have `id_rsa` \ `id_rsa.pub` and `id_bis_rsa` \ `id_bis_rsa.pub` key pairs in your
`$HOME/.ssh/` directory, you can do the following:

```console
$ mkdir ${XDG_CONFIG_HOME:-${HOME/.config}}/keychain

$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
    > ...
  + >
  + > # keychain
  + > #
  + > # prerequisite:
  + > #   * keychain: https://repology.org/project/keychain/versions
  + > #
  + > # see:
  + > #   * https://www.funtoo.org/Funtoo:Keychain
  + > #
  + > eval `keychain --dir $XDG_CONFIG_HOME/keychain --agents ssh --eval id_rsa id_bis_rsa`
```


---
## Use

* Sometimes, it might be necessary to flush all cached keys in memory (but note that any agent(s)
  will continue to run):
    ```console
    $ keychain --clear
    ```

* List signatures of all active SSH keys, and exit (similar to `ssh-add -l`):
    ```console
    $ keychain --list
    ```

* Kill currently running agent processes.

    * Kill all agent processes and quit keychain immediately:
        ```console
        $ keychain --stop all
        ```

    * Kill agent processes other than the one keychain is providing.  Prior to keychain-2.5.0,
      keychain would do this automatically.  The new behavior requires that you specify it
      explicitly if you want it:
        ```console
        $ keychain --stop others
        ```

    * Kill keychain's agent processes, leaving other agents alone:
        ```console
        $ keychain --stop mine
        ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
