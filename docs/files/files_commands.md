---
tags:
  - Files
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Files commands

**TODO**: split this cheat sheet into dedicated ones (one per command)

???+ Note "Reference(s)"
    * <https://en.wikipedia.org/wiki/GNU_Core_Utilities>
    * <https://en.wikipedia.org/wiki/List_of_GNU_Core_Utilities_commands>
    * <https://en.wikipedia.org/wiki/List_of_Unix_commands>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [`cat`](#cat)
* [`chmod`](#chmod)
* [`chown`](#chown)
* [`chgrp`](#chgrp)
* [`cksum`](#cksum)
* [`cmp`](#cmp)
* [`cp`](#cp)
* [`dd`](#dd)
* [`du`](#du)
* [`df`](#df)
* [`file`](#file)
* [`fuser`](#fuser)
* [`ln`](#ln)
* [`ls`](#ls)
* [`mkdir`](#mkdir)
* [`mv`](#mv)
* [`pax`](#pax)
* [`pwd`](#pwd)
* [`rm`](#rm)
* [`rmdir`](#rmdir)
* [`shred`](#shred)
* [`split`](#split)
* [`tee`](#tee)
* [`touch`](#touch)
* [`type`](#type)
* [`umask`](#umask)

<!-- vim-markdown-toc -->

---
## `cat`

**TODO**

---
## `chmod`

**TODO**

---
## `chown`

**TODO**

---
## `chgrp`

**TODO**

---
## `cksum`

**TODO**

---
## `cmp`

**TODO**

---
## `cp`

**TODO**

---
## `dd`

**WIP**

* Erase the content of a disk (e.g. `/dev/sdx`) by overriding it with random characters:
```console
# dd status=progress if=/dev/urandom of=/dev/sdx
```

!!! Note "Note"
    The `coreutils` command `shred` is more appropriate for this.

* Erase the content of a disk (e.g. `/dev/sdx`) by overriding it with zeros:
```console
# dd status=progress if=/dev/zero of=/dev/sdx
```

!!! Note "Note"
    The `coreutils` command `shred` is more appropriate for this.


---
## `du`

* Disk usage:
```console
$ du -s /home/user/folder/ # print disk usage in human readable format
$ du -ah --exclude="*.txt" /home/user/folder/ # print disk usage in human readable format for all file exept *.txt
$ du -sh /directory/ # print summary of disk usage in human readable format
```

**TODO**

---
## `df`

* Disk file system:
```console
$ df -kh # print disk filesystem spaces in human readable format
```

**TODO**

---
## `file`

**TODO**

---
## `fuser`

**TODO**

---
## `ln`

**TODO**

---
## `ls`

**TODO**

* Sort sort newest files last:
```console
$ ls -snew # or ls --sort newest
```

---
## `mkdir`

**TODO**

---
## `mv`

**TODO**

---
## `pax`

**TODO**

---
## `pwd`

**TODO**

---
## `rm`

**TODO**

See shred (TODO add link to shred section below) to remove the content of a file and not just the
"link" to the file.

---
## `rmdir`

**TODO**


---
## `shred`

A **GNU Core Utility** (`coreutils`) command to overwrite a file to hide its contents, and
optionally delete it.

* Full shred documentation: <https://www.gnu.org/software/coreutils/shred>

* Overwrite the content of a file with random characters:
```console
$ shred file-to-shred.txt
```

* Overwrite the content of a file (with random chars), 10 times (instead of default 3 times):
```console
$ shred -n 10 file-to-shred.txt
```

* Overwrite the content of a file with zeros after overwriting it with random characters:
```console
$ shred -z file-to-shred.txt
```

* Overwrite the content of a file you don't own (with random chars):
```console
# shred -f file-to-shred.txt
```

* Delete a file after overwriting it's content (with random chars):
```console
$ shred -u file-to-shred.txt
```

* Overwrite the first bytes of a file (with random chars):
```console
$ shred -s 10 file-to-shred.txt  # shred first 10 bytes
$ shred -s 10K file-to-shred.txt # shred first 10 kilo-bytes
$ shred -s 10M file-to-shred.txt # shred first 10 mega-bytes
$ shred -s 10G file-to-shred.txt # shred first 10 giga-bytes
```

* Overwrite the content of a full disk (e.g. `/dev/sdx`) with zeros after overwriting it with
  random chars (10 times), in verbose mode:
```console
# shred -vfz -n 10 /dev/sdx
```

!!! Warning "Warning"
    shred relies on a very important assumption: that the file system overwrites data in place.
    This is the traditional way to do things, but many modern file system designs do not satisfy
    this assumption. The following are examples of file systems on which shred is not effective,
    or is not guaranteed to be effective in all file system modes:

    * log structured or journaled file systems, such as those supplied with AIX and Solaris (and
      JFS, ReiserFS, XFS, Ext3, etc.)

    * file systems that write redundant data and carry on even if some writes fail, such as
      RAID-based file systems

    * file systems that make snapshots, such as Network Appliance's NFS server

    * file systems that cache in temporary locations, such as NFS version 3 clients

    * compressed file systems

!!! Note "Note"
    Multiple algorithms exists to wipe your data: see
    [here](https://www.stellarinfo.com/article/7-algorithms-to-wipe-files-folders-permanently.php).

    But all data erasure methods are quite similar apart from the number of passes and what or how
    characters are written over existing data. There is actually no hard proof that those methods
    are more effective than a few passes of random scrubbing, like `shred` does (except for very
    specific hardware): see <https://en.wikipedia.org/wiki/Gutmann_method#Criticism>.


---
## `split`

**TODO**

---
## `tee`

**TODO**

---
## `touch`

**TODO**

---
## `type`

**TODO**

---
## `umask`

**TODO**



---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
