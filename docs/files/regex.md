---
tags:
  - Files
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Regular Expressions

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Special characters](#special-characters)

<!-- vim-markdown-toc -->

**TODO**

### Special characters

There are multiple types of regular expressions and the set of special characters depend on the
particular type. Some of them are described below. In all the cases special characters are escaped
by backslash `\`. E.g. to match `[` you write `\[` instead.

The characters which are special in some contexts, like `^` (special at the beginning of a
(sub-)expression) can be escaped in all contexts.

In shell if you do not enclose the expression between single quotes you have to additionally escape
the special characters for the shell in the already escaped regex. Example: Instead of `'\['` you
can write `\\[` (alternatively: `"\["` or `"\\["`) in Bourne compatible shells like bash but this
is another story.

* [BRE](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap09.html#tag_09_03)
    * POSIX: Basic Regular Expressions
    * Commands: `grep`, `sed`
    * Special characters: `.[\`
    * Special in some contexts: `*^$`
    * Escape a string: `"$(printf '%s' "$string" | sed 's/[.[\*^$]/\\&/g')"`

* [ERE](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap09.html#tag_09_04)
    * POSIX: Extended Regular Expressions
    * Commands: `grep -E`, `sed -r` (GNU `sed`), `sed -E` (BSD `sed`)
    * Special characters: `.[\(`
    * Special in some contexts: `*^$)+?{|`
    * Escape a string: `"$(printf '%s' "$string" | sed 's/[.[\*^$()+?{|]/\\&/g')"`


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
