---
tags:
  - Networks
  - Timing
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `ntp`

The NTP Project provides a reference implementation of the NTP protocol and implementation
documentation through a largely volunteer effort.

It is a common method to synchronize the software clock of a GNU/Linux system with internet time
servers. It is designed to mitigate the effects of variable network latency and can usually
maintain time to within tens of milliseconds over the public Internet. The accuracy on local area
networks is even better, up to one millisecond.

???+ Note "Reference(s)"
    * [The reference NTP implementation](https://en.wikipedia.org/wiki/Reference_implementation)
      (both client side and server side).
    * <https://wiki.archlinux.org/title/Network_Time_Protocol_daemon>
    * <https://support.ntp.org/bin/view/Support/GettingStarted>
    * <https://support.ntp.org/bin/view/Main/WebHome#The_NTP_Project>
    * <https://wiki.gentoo.org/wiki/Network_Time_Protocol>
    * <https://linuxhint.com/configure_network_time_protocol_archlinux/>
    * <https://en.wikipedia.org/wiki/Network_Time_Protocol>
    * <https://www.ntppool.org/en/>
    * <https://www.ntp.org/>
    * <https://doc.ntp.org>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "apk"
        ```console
        # apt add ntp
        ```

    === "apt"
        ```console
        # apt install ntp
        ```

    === "dnf"
        ```console
        # dnf install ntp
        ```

    === "emerge"
        ```console
        # emerge -a sys-process/ntp
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.ntp
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.ntp
            ```

    === "pacman"
        ```console
        # pacman -S ntp
        ```

        !!! Tip "For Artix users"
            * **If** using `dinit`:
            ```console
            # pacman -S ntp ntp-dinit
            ```
            * **If** using `openrc`:
            ```console
            # pacman -S ntp ntp-openrc
            ```
            * **If** using `runit`:
            ```console
            # pacman -S ntp ntp-runit
            ```
            * **If** using `s6`:
            ```console
            # pacman -S docker ntp-s6
            ```

    === "yum"
        ```console
        # yum install ntp
        ```

    === "xbps"
        ```console
        # xbps-install -S ntp
        ```

    === "zypper"
        ```console
        # zypper install ntp
        ```

---
## Config

!!! Note ""

    === "OpenRC"
        ```console
        # rc-update add ntp default
        # rc-service ntp start
        ```

    === "Runit"
        Depending on your `runit` implementation, either run:
        ```console
        # ln -s /etc/runit/sv/ntpd /service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/ntpd /var/service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/ntpd /run/runit/service
        ```
        In any case, finally run:
        ```console
        # sv up ntpd
        ```

    === "SysVinit"
        ```console
        # service ntp start
        # chkconfig ntp on
        ```

    === "SystemD"
        ```console
        # systemctl enable ntp
        # systemctl start ntp
        ```

The default `/etc/ntp.conf` config file should be OK for common use.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
