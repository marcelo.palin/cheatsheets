---
tags:
  - Networks
  - Timing
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# OpenNTPD

**TODO**

???+ Note "Reference(s)"
    * <https://www.openntpd.org/>
    * <https://github.com/openntpd-portable>
    * <https://wiki.gentoo.org/wiki/OpenNTPD>
    * <https://wiki.archlinux.org/title/OpenNTPD>
    * <https://en.wikipedia.org/wiki/OpenNTPD>
    * <https://www.openhub.net/p/ntpd>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->


---
## Install

**TODO**


---
## Config

**TODO**


---
## Use

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
