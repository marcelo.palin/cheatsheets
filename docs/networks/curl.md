---
tags:
  - Networks
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Curl

**TODO**

???+ Note "Reference(s)"
    * <https://www.youtube.com/watch?v=QJuz_O01mDg>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Use](#use)

<!-- vim-markdown-toc -->

---
## Use

```console
$ curl wttr.in          # get weather
$ curl wttr.in/Paris    # get weather in Paris
$ curl wttr.in/:help

$ curl cheat.sh         # get cheat sheets
$ curl cheat.sh/ls      # get ls cheat sheet

$ curl qrenco.de        # qr code generator
$ curl qrenco.de/input  # qr code generator for the string "input"

$ curl dict://dict.org/d:internet   # dictonary (e.g. search for the word "internet")

$ curl rate.sx          # get cryptocurrency prices
$ curl rate.sx/eth      # get etherum prices

$ curl -F'shorten=https://youtube.com' https://0x0.st   # shorten url (e.g. shorten "https://youtube.com")

$ curl parrot.live      # get the dancing parrot

$ echo https://www.gentoo.org/ | curl -F-=\<- qrenco.de # generate a QR code in the terminal
                                                        # <https://chispa.fr/sima78/index.php?post/2020/12/23/Generer-des-QR-Code-depuis-votre-terminal-Qrenco>
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
