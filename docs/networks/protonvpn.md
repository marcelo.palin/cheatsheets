---
tags:
  - Networks
  - VPN
---


<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# ProtonVPN

???+ Note "Reference(s)"
    * <https://github.com/ProtonVPN/linux-cli>
    * <https://github.com/ProtonVPN/linux-cli/blob/master/USAGE.md>
    * <https://protonvpn.com/support/linux-vpn-tool/>
    * <https://protonvpn.com/support/linux-vpn-setup/>
    * <https://wiki.archlinux.org/index.php/ProtonVPN>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
* [OpenVPN](#openvpn)
* [Tips](#tips)
    * [Run the VPN per application](#run-the-vpn-per-application)
    * [Configure the VPN to accept traffic from public IP and respond on the same channel](#configure-the-vpn-to-accept-traffic-from-public-ip-and-respond-on-the-same-channel)
        * [How to undo this ?](#how-to-undo-this-)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "Gentoo Kernel"
        A correct [kernel config](../distros/gentoo-based/gentoo_kernel.md#kernel-config) is
        needed:
        ```console
        $ cd /usr/src/linux
        # make nconfig # or `# make menuconfig`

            # Double check here:
            # <https://wiki.gentoo.org/wiki/OpenVPN#Kernel>

            > Device Drivers  --->
            >     [*] Network device support  ---> # Symbol: NETDEVICES [=y]
            >         [*] Network core driver support # Symbol: NET_CORE [=y]
            >         <*>   Universal TUN/TAP device driver support  # Symbol: TUN [=y]
        ```

        !!! Warning "Warning"
            After configuring the kernel don't forget to do a [kernel make and
            rebuild](..//distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!

!!! Note ""

    === "apk"
        TODO

    === "apt"
        TODO

    === "dnf"
        TODO

    === "emerge"
        ```console
        # vi /etc/portage/package.accept_keywords
            > ...
            > # last protonvpn-cli
            > net-vpn/protonvpn-cli ~amd64
            > ...
        # emerge -a net-vpn/protonvpn-cli
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.protonvpn-cli
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.protonvpn-cli
            ```

    === "~~pacman~~ AUR (by hand)"
        Install with [AUR](../distros/arch-based/aur.md):
        ```console
        $ mkdir -p ~/apps/aur-apps
        $ cd ~/apps/aur-apps
        $ git clone https://aur.archlinux.org/protonvpn-cli-ng.git
        $ cd protonvpn-cli-ng
        $ makepkg -is # --syncdeps to auto-install deps, --install to install after building
        ```

    === "yum"
        TODO

    === "xbps"
        TODO

    === "zypper"
        TODO


Initialize `protonvpn-cli`:
```console
# protonvpn init
```

!!! Warning ""
    During the initialization step, when being asked the username and the password, use your
    ProtonVPN - OpenVPN credentials (not your account username and password). I.e. go to [ProtonVPN
    website](https://account.protonvpn.com) -> login -> account -> OpenVPN / IKEv2.

---
## Config

Change `protonvpn-cli` configuration if needed:
```console
# protonvpn configure
```

---
## Use

* Get status:
```console
$ protonvpn status
```

* Connect to a random server:
```console
# protonvpn c -r
```

* Connect to the fastest server:
```console
# protonvpn c -f
```

* Connect to the fastest P2P server:
```console
# protonvpn c --p2p
```

* Connect to the fastest server in a specified country:
```console
# protonvpn c --cc [countrycode]
```

* Connect to the fastest "secure core" server:
```console
# protonvpn c --sc
```

* Reconnect to the last server used:
```console
# protonvpn r
```

* Disconnect the current session:
```console
# protonvpn disconnect, d
```

* Refresh OpenVPN configuration and server data:
```console
# protonvpn refresh
```

* Print example commands:
```console
# protonvpn examples
```

* Display version:
```console
# protonvpn --version
```

* Show help message:
```console
# protonvpn --help
```


---
## OpenVPN

???+ Note "Reference(s)"
    * <https://protonvpn.com/support/linux-vpn-setup/>

ProtonVPN can entirely be used through OpenVPN.

* Install prerequisite ([`resolvconf`](https://repology.org/project/resolvconf/versions)):

!!! Note ""

    === "pacman"
        ```console
        $ sudo pacman -S openresolv
        ```

    === "emerge"
        ```console
        $ sudo emerge -a net-dns/openresolv
        ```

    === "apt"
        ```console
        $ sudo apt install resolvconf
        ```

    === "yum"
        **TODO**
        ```console
        ```

    === "dnf"
        **TODO**
        ```console
        ```

* Create a dedicated config directory:
  ```console
  $ mkdir -p $HOME/.config/openvpn/protonvpn-server-configs
  ```

* Download the desired configuration files:
    * Go to the [ProtonVPN website](https://account.protonvpn.com)
    * Login
    * Downloads
    * OpenVPN configuration files
    * 1. Select platform: `GNU/Linux`
    * 2. Select protocol: `UDP` (recommended) or `TCP` if you experience slow VPN speeds
    * 3. Select config file and download: `Standard server configs`
    * Download all configurations in `$HOME/.config/openvpn/protonvpn-server-configs`

* Get your credentials:
    * Go to the [ProtonVPN website](https://account.protonvpn.com)
    * Login
    * Account
    * OpenVPN / IKEv2

* Install the ProtonVPN `update-resolv-conf` script:
  ```console
  $ sudo wget "https://raw.githubusercontent.com/ProtonVPN/scripts/master/update-resolv-conf.sh" -O "/etc/openvpn/update-resolv-conf"
  $ sudo chmod +x "/etc/openvpn/update-resolv-conf"
  ```

* Put you ProtonVPN credentials into a pass file:
  ```console
  $ vi $HOME/.config/openvpn/protonvpn-pass.conf
      > OpenVPN-IKEv2-username
      > OpenVPN-IKEv2-password
  ```

* Append the path to the pass file to all your downloaded `.ovpn` files:
  ```console
  $ cd $HOME/.config/openvpn/protonvpn-server-configs
  $ for file in *; do echo "auth-user-pass /home/username/.config/openvpn/protonvpn-pass.conf" >> "$file"; done
  ```

* Now you can connect to a ProtonVPN server like so (e.g. on server `fr-36`):
  ```console
  $ sudo openvpn ~/.config/openvpn/protonvpn-server-configs/fr-36.protonvpn.com.udp.ovpn
  ```


---
## Tips

### Run the VPN per application

See [`vpnify`](./vpnify.md).

### Configure the VPN to accept traffic from public IP and respond on the same channel

**TODO**: `/opt/update-duckdnsip`

When a VPN is set up on a server, how to access it via SSH ? Or how to still let the server be
available e.g. with Nginx ? I.e. how to accept traffic from public IP and respond on the same
channel, not with the VPN channel ?

Find the interface name you are using to connect to the internet:
```console
$ ifconfig
```
E.g. `wlp2s0` is my **interface**.


Find your `inet` (`/inet4`) on your interface:
```console
$ ip a show dev wlp2s0
    > ...
    > wlp2s0: ...
    >   ...
    >   inet 192.168.1.42/24 ...
    >   ...
```
E.g. my `inet` is `192.168.1.42/24`. `192.168.1.42` is the **local address**, 24 is the
**netmask**, and 192.168.1.0 is the **subnet**.  (NOTE: you can configure your internet router to
get a fixed `inet`)

Find your **gateway**:
```console
$ ip route show dev wlp2s0
```
The gateway is the address next to "default via ...". E.g. 192.168.1.1 is my **gateway**.

Add a table rule:
```console
# ip rule add from [LOCAL ADDRESS] table 128
```

Add routing rules:
```console
# ip route add table 128 to [SUBNET]/[NETMASK] dev [INTERFACE]
# ip route add table 128 default via [GATEWAY]
```

**NOTE**: These rules will only last until the system shuts down. Consider reapplying them when the
system reboots.

#### How to undo this ?

Show table:
```console
# ip rule show
# ip route show table 128
```
Remove table:
```console
# ip rule del table 128
# ip route flush table 128
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
