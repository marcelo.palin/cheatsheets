---
tags:
  - Networks
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Gopher

Gopher is an application layer internet communication protocol for accessing remote documents,
similarly to HTTP or [Gemini](./gemini.md). It is intended as a third alternative to those
protocols.

The design of the Gopher protocol and user interface is menu-driven, and presented an alternative
to the WWW in its early stages, but ultimately fell into disfavor, yielding to HTTP. The Gopher
ecosystem is often regarded as the effective predecessor of the WWW.

???+ Note "Reference(s)"
    * <https://en.wikipedia.org/wiki/Gopher_(protocol)>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
