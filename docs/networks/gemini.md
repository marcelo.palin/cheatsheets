---
tags:
  - Networks
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Gemini

Gemini is an application layer internet communication protocol for accessing remote documents,
similarly to HTTP or [Gopher](./gopher.md). It is intended as a third alternative to those
protocols. It comes with a special document format, commonly called `gemtext`, that allows linking
to other documents.

???+ Note "Reference(s)"
    * <https://gemini.circumlunar.space/>
    * <https://gemini.circumlunar.space/docs/>
    * <https://gemini.circumlunar.space/software/>
    * <https://en.wikipedia.org/wiki/Gemini_(protocol)>
    * <https://landchad.net/gemini>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Gemini to web proxy service](#gemini-to-web-proxy-service)

<!-- vim-markdown-toc -->

**TODO**

---
## Gemini to web proxy service

* <https://portal.mozz.us/gemini/gemini.circumlunar.space/>
* <https://proxy.vulpes.one/gemini/gemini.circumlunar.space>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
