---
tags:
  - Networks
  - Monitoring Programs
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `fping`

`fping` is a program to send ICMP echo probes to network hosts, similar to ping, but much better
performing when pinging multiple hosts.

???+ Note "Reference(s)"
    * <https://fping.org>
    * <https://github.com/schweikert/fping>
    * <https://man.archlinux.org/man/fping.8>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "pacman"
        ```console
        # pacman -S fping
        ```

    === "emerge"
        ```console
        # emerge -a net-analyzer/fping
        ```

    === "apt"
        ```console
        # apt install fping
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.fping
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.fping
            ```

    === "yum"
        ```console
        # yum install fping
        ```

    === "dnf"
        ```console
        # dnf install fping
        ```

---
## Use

* `fping` multiple IPs directly:
```console
$ fping 50.116.66.139 173.194.35.35 98.139.183.24
```

* list alive hosts within a subnet generated from a netmask:
```console
$ fping -a -g 192.168.1.0/24
```

* list alive hosts within a subnet generated from an IP range:
```console
$ fping -a -g 192.168.1.1 192.168.1.254
```

* list unreachable hosts within a subnet generated from a netmask:
```console
$ fping -u -g 192.168.1.0/24
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
