---
tags:
  - Networks
  - Security
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# SSH

SSH is a cryptographic network protocol for operating network services securely over an unsecured
network.

In addition to remote terminal access provided by the main `ssh` binary, the `ssh` suite of
programs has grown to include other tools such as `scp` (Secure Copy Program) and `sftp` (Secure
File Transfer Protocol).

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/OpenSSH>
    * <https://wiki.archlinux.org/index.php/SSH_keys>
    * <https://wiki.gentoo.org/wiki/SSH>
    * <https://www.funtoo.org/OpenSSH_Key_Management,_Part_1>
    * <https://www.funtoo.org/OpenSSH_Key_Management,_Part_2>
    * <https://www.funtoo.org/OpenSSH_Key_Management,_Part_3>
    * <https://infosec.mozilla.org/guidelines/openssh>
    * <https://tutox.fr/2020/04/16/generer-des-cles-ssh-qui-tiennent-la-route/>
    * <https://github.com/arthepsy/ssh-audit>
    * <https://github.com/jtesta/ssh-audit>
    * <https://linuxfr.org/users/gouttegd/journaux/utilisation-d-un-tpm-pour-l-authentification-ssh>
    * <https://stribika.github.io/2015/01/04/secure-secure-shell.html>


---
## Table of content

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [ssh-agent](#ssh-agent)
    * [`ssh` tunneling](#ssh-tunneling)
        * [Local forwarding](#local-forwarding)
        * [Remote forwarding](#remote-forwarding)
        * [Dynamic forwarding](#dynamic-forwarding)
    * [Security considerations](#security-considerations)
    * [Troubleshooting](#troubleshooting)
        * [OpenSSH RSA SHA-1 signatures](#openssh-rsa-sha-1-signatures)
    * [Misc](#misc)

<!-- vim-markdown-toc -->

---
## Install

Install `openssh`:

!!! Note ""

    === "emerge"
        ```console
        # emerge -a net-misc/openssh
        ```

    === "pacman"
        ```console
        # pacman -S openssh
        ```

        !!! Tip "For Artix users"
            * **If** using `openrc`:
            ```console
            # pacman -S cronie openssh-openrc
            ```
            * **If** using `runit`:
            ```console
            # pacman -S cronie openssh-runit
            ```
            * **If** using `s6`:
            ```console
            # pacman -S cronie openssh-s6
            ```

    === "apt"
        ```console
        # apt install openssh openssh-server
        ```

    === "yum"
        ```console
        # yum install openssh
        ```

    === "dnf"
        ```console
        # dnf install openssh
        ```

If you also want to host an SSH server (`sshd`, SSH daemon), then add it to your init system and
start it:

!!! Note ""

    === "OpenRC"
        ```console
        # rc-update add sshd default
        # /etc/init.d/sshd start
        ```

    === "Runit"
        Depending on your `runit` implementation, either run:
        ```console
        # ln -s /etc/runit/sv/sshd /service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/sshd /var/service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/sshd /run/runit/service
        ```
        In any case, finally run:
        ```console
        # sv up sshd
        ```

    === "SysVinit"
        ```console
        # service sshd start
        # chkconfig sshd on
        ```

    === "SystemD"
        ```console
        # systemctl enable sshd
        # systemctl start sshd
        ```


---
## Config

* Create an `ssh` key pair:
  ```console
  $ ssh-keygen -o -t rsa -b 4096 -C "user@mail.com" -f "/home/user/.ssh/ssh_key_name"
  ```

!!! Tip ""
    When creating the key pair, if asked to enter a passphrase: no passphrase might be just fine
    (e.g. for git use). In this case you can instead use this command:
    ```console
    $ ssh-keygen -o -t rsa -b 4096 -C "user@mail.com" -f "/home/user/.ssh/ssh_key_name" -N ""
    ```

* Add `ssh` key pair:
  ```console
  $ eval `ssh-agent -s`
  $ ssh-add ~/.ssh/ssh_key_name
  ```

* Check `ssh` key pair:
  ```console
  $ ssh-keygen -l -f ~/.ssh/id_rsa.pub
  ```

TODO: `motd`: <https://www.kali-linux.fr/astuces/comment-securiser-son-serveur-ssh>


---
## Use

!!! Note "Prerequisite(s)"
    Make sure `sshd` is running on the remote computer, e.g. on `SystemD` based distro:
    ```console
    remote-pc $ systemctl status sshd
    ```
    If `sshd` is not active and running, then start the `sshd` service, e.g. on `SystemD` based
    distro:
    ```console
    remote-pc $ sudo systemctl enable sshd
    remote-pc $ sudo systemctl start sshd
    ```

* Basic, password based, `ssh` connection (e.g. to remote user with 123.1.2.3 IP address):
```console
$ ssh remoteuser@123.1.2.3
```

* RSA (key pair) based `ssh` connection:

    * Copy and authorize the public key to the remote user (e.g. with 123.1.2.3 IP address):
      ```console
      $ ssh-copy-id -i /home/user/.ssh/ssh_key_name.pub remoteuser@123.1.2.3
      ```
      > If you have to manually copy the public key, see
      > <https://web.archive.org/web/20220223140333/https://linuxhandbook.com/add-ssh-public-key-to-server/>

    * Then configure `ssh` to use your `ssh_key_name`, e.g. with 123.1.2.3:
      ```console
      $ vi $HOME/.ssh/config
          > ...
        + >
        + > Host 123.1.2.3
        + >   IdentityFile ~/.ssh/ssh_key_name
        + >
          > ...
      ```

    * Finally, you should be able to connect to `123.1.2.3` without being asked for a password:
      ```console
      $ ssh remoteuser@123.1.2.3
      ```

    * If you still have to enter a password, make sure the target `sshd` server is well configured,
      e.g. check the `/etc/ssh/sshd_config` file and make sure the following options are configured
      appropriately:
        * `PubkeyAuthentication` **should not** be set to `no` (default is `yes`)
        * `ChallengeResponseAuthentication` should be set to `no`
        * `AuthorizedKeysFile` should be set to `.ssh/authorized_keys`
        * if `StrictModes` is set to `yes` (which is a sane default), then please make sure that
          the target `$HOME/.ssh` folder has the following permissions: `drwx------` (if not, run
          `$ chmod 700 $HOME/.ssh`), and please make sure that the target
          `$HOME/.ssh/authorized_keys` file has the following permissions: `-rw-------` (if not,
          run `$ chmod 600 $HOME/.ssh/authorized_keys`)

* Test `ssh` key:
  ```console
  $ ssh -T remoteuser@192.168.1.10
  ```

* Copy a file trough `ssh` on a specific port (e.g. port 2222):
  ```console
  $ scp -P 2222 remoteuser@192.168.1.10:/home/remoteuser/plop.file /home/localuser/test.file
  ```

* Copy a directory trough `ssh`:
  ```console
  $ scp -r remoteuser@192.168.1.10:/home/username/folder /home/localuser/folder
  ```

* Avoid `ssh` freeze after inactivity (see
  <https://serverfault.com/questions/575112/why-do-my-ssh-sessions-freeze-after-some-time>):
  ```console
  $ vi ~/.ssh/config
      > ...
      > Host *
      >     ...
    + >     ServerAliveInterval 60
      > ...
  ```

* Allow SSH agent forwarding in order to use your private and local SSH key remotely without
  worrying about leaving confidential data on the server you’re working with. See
  <https://web.archive.org/web/20221214073944/https://www.howtogeek.com/devops/what-is-ssh-agent-forwarding-and-how-do-you-use-it/> :
  ```console
  $ vi $HOME/.ssh/config
    > ...
    > Host *
    >   ...
    >   ForwardAgent yes
    > ...
  ```

### ssh-agent

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/SSH_keys#SSH_agents>
    * <https://wiki.gentoo.org/wiki/SSH#ssh-agent>

**TODO**

### `ssh` tunneling

#### Local forwarding

???+ Note "Reference(s)"
    * <https://linuxize.com/post/how-to-setup-ssh-tunneling/>
    * <https://phoenixnap.com/kb/ssh-port-forwarding>

* Forward remote port `42001`, of `10.10.0.1`, on your local port `10042`:
    ```console
    $ ssh -L 10042:127.0.0.1:42001 user_name@10.10.0.1
    ```

* Forward remote port `42001`, of `10.10.0.1`, on your local port `10042`, through multiple
  intermediate machines (multiple "hops"):
    ```console
    $ ssh -J gate1_user_name@gate1_ip_address,gate2_user_name@gate2_ip_address -L 10042:127.0.0.1:42001 user_name@10.10.0.1
    ```

* Forward remote ports `42001` and `42002` (of `10.10.0.1`) on your local ports `10042` and
  `20042`, through multiple intermediate machines (multiple "hops"):
    ```console
    $ ssh -J gate1_user_name@gate1_ip_address,gate2_user_name@gate2_ip_address -L 10042:127.0.0.1:42001 -L 20042:127.0.0.1:42002 user_name@10.10.0.1
    ```

* Forward remote ports `42001` and `42002` (of `192.168.0.1`, seen by `10.10.0.1` on another
  network interface) on your local ports `10042` and `20042`, through multiple intermediate
  machines (multiple "hops"):
    ```console
    $ ssh -J gate1_user_name@gate1_ip_address,gate2_user_name@gate2_ip_address -L 10042:192.168.0.1:42001 user_name@10.10.0.1
    ```

* Forward remote port `42001`, of `10.10.0.1`, on your local port `10042` (without allowing to
  execute remote command `-N`, and allowing to bind your local port `10042` to any available
  interface - not just `localhost` the loopback interface):
    ```console
    $ ssh -L 10042:127.0.0.1:42001 -N -o GatewayPorts=yes user_name@10.10.0.1
    ```

#### Remote forwarding

???+ Note "Reference(s)"
    * <https://linuxize.com/post/how-to-setup-ssh-tunneling/>
    * <https://phoenixnap.com/kb/ssh-port-forwarding>

* **TODO**
    ```console
    $ ssh -R ...
    ```

#### Dynamic forwarding

???+ Note "Reference(s)"
    * <https://linuxize.com/post/how-to-setup-ssh-tunneling/>
    * <https://phoenixnap.com/kb/ssh-port-forwarding>

* **TODO**
    ```console
    $ ssh -D ...
    ```

---
### Security considerations

* Use SSH key pairs whenever possible to authenticate.

* Set up a [fail2ban](./fail2ban.md) (or [similar
  alternative](https://alternativeto.net/software/fail2ban/?license=opensource)) service.

* Set up a [firewall](../networks/firewalls.md) service, excluding all unused open ports.

* Lock SSH access for the `root` user.

* Optionally don't use port 22 for SSH.

* When using `rsync` through SSH, consider using [restricted
  `rsync`](https://www.guyrutenberg.com/2014/01/14/restricting-ssh-access-to-rsync/).

---
### Troubleshooting

#### OpenSSH RSA SHA-1 signatures

When connecting to a SSH server, you might get a similar error:
```console
Unable to negotiate with 123.1.2.3 port 22: no matching host key type found. Their offer: ssh-rsa,ssh-dss
```

As of version 8.8, OpenSSH disables RSA signatures using the SHA-1 hash algorithm by default.
This change affects both the client and server components.

After upgrading to this version, you may have trouble connecting to older SSH servers that do not
support the newer RSA/SHA-256/SHA-512 signatures. Support for these signatures was added in OpenSSH
`7.2`.

As well, you may have trouble using older SSH clients to connect to a server running OpenSSH 8.8 or
higher. Some older clients do not automatically utilize the newer hashes. For example, PuTTY before
version 0.75 is affected.

To resolve these problems, you can upgrade your SSH client/server wherever possible. If this is not
feasible, support for the SHA-1 hashes may be re-enabled using the following config options:
```console
HostkeyAlgorithms +ssh-rsa
PubkeyAcceptedAlgorithms +ssh-rsa
```

Or using the `ssh` command with the following options:
```console
$ ssh -oHostKeyAlgorithms=+ssh-rsa -oPubkeyAcceptedAlgorithms=+ssh-rsa username@123.1.2.3
```

See <https://www.gentoo.org/support/news-items/2021-10-08-openssh-rsa-sha1.html>.

---
### Misc

* Play tron with `ssh`
  ```console
  $ ssh sshtron.zachlatta.com
  ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
