---
tags:
  - Networks
  - Security
  - Privacy
  - Tor
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Torsocks

Torsocks allows you to use most applications in a safe way with Tor. It ensures that DNS requests
are handled safely and explicitly rejects any traffic other than TCP from the application you're
using.

???+ Note "Reference(s)"
    * <https://github.com/dgoulet/torsocks>
    * <https://computerz.solutions/ssh-via-tor/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

TODO

---
## Config

TODO

---
## Use

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
