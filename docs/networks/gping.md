---
tags:
  - Networks
  - Monitoring Programs
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `gping`

`gping` is like ping, but with a graph.

???+ Note "Reference(s)"
    * <https://github.com/orf/gping>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "pacman"
        ```console
        # pacman -S gping
        ```

    === "emerge"
        TODO

    === "apt"
        ```console
        $ echo "deb http://packages.azlux.fr/debian/ buster main" | sudo tee /etc/apt/sources.list.d/azlux.list
        $ wget -qO - https://azlux.fr/repo.gpg.key | sudo apt-key add -
        $ sudo apt update
        $ sudo apt install gping
        ```

    === "yum"
        TODO

    === "dnf"
        TODO

---
## Use

```console
$ gping duckduckgo.org
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
