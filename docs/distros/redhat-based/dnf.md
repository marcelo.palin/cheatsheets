---
tags:
  - Linux Distributions
  - Distros
  - Red Hat Based
  - Package Management
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# DNF

DNF

**TODO**

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Pacman/Rosetta>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [system upgrade](#system-upgrade)
* [config](#config)
    * [Core Plugins for DNF](#core-plugins-for-dnf)
    * [EPEL](#epel)
    * [PowerTools](#powertools)

<!-- vim-markdown-toc -->


---
## system upgrade

!!! Warning "Warning"
    Back up your data before performing a system-wide upgrade as every system upgrade is
    potentially risky. As a precaution, you can prepare a [SystemRescueCD bootable
    media](../../hardware/bootable_media.md).

* Full upgrade of the system:
    ```console
    $ sudo dnf check-update
    $ sudo dnf upgrade --refresh
    ```
    And reboot your computer.


---
## config

### Core Plugins for DNF

The `dnf-plugins-core` package enhances DNF with multiple useful commands (see [associated
documentation](https://dnf-plugins-core.readthedocs.io/en/latest/)). You can install it like so:
```console
$ sudo dnf install dnf-plugins-core
```

### EPEL

EPEL represents "Extra Packages for Enterprise Linux". It is a repository that is found online. It
develops and manages extra packages of standard quality. This repository is developed for
Enterprise Linux, such as Oracle Linux (OL), CentOS, Scientific Linux (SL), and Red Hat Enterprise
Linux (RHEL). Using Fedora, these packages were developed and tested.

EPEL packages are mainly based on Fedora, which means that these packages will never replace or
conflict with packages in Enterprise Linux systems. EPEL shares so much of Fedora’s architecture,
including the Bugzilla instance, build system, mirror manager, updates manager, and more.

```console
$ sudo dnf install epel-release
$ sudo dnf repolist | grep epel && echo OK
```

### PowerTools

**⚠️ Prerequisites ⚠️**:

* [Core Plugins for DNF](#core-plugins-for-dnf)
* [EPEL](#epel)

PowerTools is a repository that acts as a container for libraries and developer tools. This
repository is available on RHEL/ CentOS but not enabled by default. Some of the most popular EPEL,
"Extra packages for Enterprise Linux", rely on PowerTools packages. You should install PowerTools
on your system if the EPEL repository is already enabled:

```console
$ sudo dnf config-manager --set-enabled powertools
$ sudo dnf repolist | grep powertools && echo OK
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
