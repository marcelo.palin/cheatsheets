---
tags:
  - Linux Distributions
  - Distros
  - Gentoo Based
  - Kernel
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Kernel management

This cheat sheet is about managing the official Gentoo kernel: `gentoo-sources`. No alternative
kernels (<https://wiki.gentoo.org/wiki/Kernel/Overview>) will be discussed here.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Category:Kernel>
    * <https://wiki.gentoo.org/wiki/Kernel>
    * <http://kroah.com/log/blog/2018/08/24/what-stable-kernel-should-i-use/>
    * <https://wiki.gentoo.org/wiki/Kernel/Overview>
    * <https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Kernel>
    * <https://wiki.gentoo.org/wiki/Kernel/Upgrade>
    * <https://wiki.gentoo.org/wiki/Kernel/Gentoo_Kernel_Configuration_Guide>
    * <https://wiki.gentoo.org/wiki/Kernel/Removal>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Kernel in world set](#kernel-in-world-set)
* [Kernel update](#kernel-update)
    * [Emerge new kernel version](#emerge-new-kernel-version)
    * [Backup the current kernel config and clean the kernel before update](#backup-the-current-kernel-config-and-clean-the-kernel-before-update)
    * [Kernel update procedure](#kernel-update-procedure)
* [Kernel config](#kernel-config)
* [Kernel make and rebuild](#kernel-make-and-rebuild)
* [GRUB2 update](#grub2-update)
* [Old kernel removal](#old-kernel-removal)
    * [Removing old or unused `gentoo-sources` (after switching to newer sources)](#removing-old-or-unused-gentoo-sources-after-switching-to-newer-sources)
    * [Removing kernel leftovers](#removing-kernel-leftovers)
* [Kernel troubleshooting](#kernel-troubleshooting)

<!-- vim-markdown-toc -->

---
## Kernel in world set

Make sure the current version of your kernel (e.g. `v4.19.72`) is part of your `world` set, this
way there is no risks for an update to accidentally overwrite it (especially when running `# emerge
--depclean`):
```console
# emerge -av --noreplace sys-kernel/gentoo-sources:4.19.72
# vi /var/lib/portage/world # check your current kernel version in the world file:
    > ...
    > sys-kernel/gentoo-sources
    > sys-kernel/gentoo-sources:4.19.72
    > ...
```

After updating to a newer kernel version (e.g. `v4.19.97`), and making sure your system is stable
and does work fine, you can remove the previous kernel from the `world` set and add the new one
instead:
```console
# emerge -av --deselect sys-kernel/gentoo-sources:4.19.72
# emerge -av --noreplace sys-kernel/gentoo-sources:4.19.97
# vi /var/lib/portage/world # check your current kernel version replacement in the world file:
    > ...
    > sys-kernel/gentoo-sources
    > sys-kernel/gentoo-sources:4.19.97
    > ...
```


---
## Kernel update

* See <https://wiki.gentoo.org/wiki/Kernel/Upgrade>

### Emerge new kernel version

```console
# emerge --ask --update --deep --with-bdeps=y --newuse sys-kernel/gentoo-sources
```

### Backup the current kernel config and clean the kernel before update

* Backup and delete most generated files:
```console
# mkdir -p /usr/src/.kernel_config_backups/
# cp /usr/src/linux/.config /usr/src/.kernel_config_backups/kernel_config_$(uname -r)_$(date +"%Y-%m-%d_%T")
$ cd /usr/src/linux
# make clean
```
(it's OK if there is no rule to make target...)

<br/>
**OR**
<br/>

* Backup, delete the current `.config` and all generated files, then recover `.config`:
```console
# mkdir -p /usr/src/.kernel_config_backups/
# cp /usr/src/linux/.config /usr/src/.kernel_config_backups/kernel_config_$(uname -r)_$(date +"%Y-%m-%d_%T")
$ cd /usr/src/linux
# make mrproper
# cp /usr/src/.kernel_config_backups/kernel_config_`uname -r` /usr/src/linux/.config
```
(it's OK if there is no rule to make target...)

!!! Tip "Tip"
    *Eventually*, you may consider using `# make distclean` for removing editor backup files, patch
    leftover files and the like.

### Kernel update procedure

Select the new kernel, change directory into it and copy your kernel `.config` file in it:
```console
$ eselect kernel list
# eselect kernel set n # set to the new kernel

$ cd /usr/src/linux # ⚠️ important : necessary to move into the new kernel's directory

# cp /usr/src/linux-x.x.x-gentoo-old/.config /usr/src/linux/.config
```

Now you have to convert your `.config` file for the new kernel version. The following configuration
is like the text based configuration with `make config`. In order to do this, either use `# make
olddefconfig` **or** use `# make oldconfig`.

* `olddefconfig` will keep all of the options from the old `.config` and automatically set the new
  options to their recommended default values:
    ```console
    # make olddefconfig
    ```

    **OR**

* `syncconfig` will let you decide by entering a value, for each new option, to either install it
  as *built-in* (`y` value), to install it as a *module* (`m` value), to *not install it* (`n`
  value), or to print more *info* (`?` value). The recommended/default value will be capitalized
  (e.g.  `[Y/n/m/?]`):
    ```console
    # make oldconfig
    ```

!!! Tip
    Use `make help` to see other conversion methods available.

<br/>

Compare the old and new `.config` to see what options have been added:
```console
$ diff <(cat .config | sort) <(cat .config.old | sort) | awk '/^<.*(=|Linux)/ { $1=""; print }'
```

Compare the old and new `.config` to see what options have been removed:
```console
$ diff <(cat .config | sort) <(cat .config.old | sort) | awk '/^>.*(=|Linux)/ { $1=""; print }'
```

!!! Warning "Warning"
    After updating the kernel, don't forget to refer to the below [Kernel make and
    rebuild](#kernel-make-and-rebuild) sub-section.

!!! Warning "Warning"
    Then, after rebuilding the kernel, don't forget to refer to the below [GRUB2
    update](#grub2-update) sub-section.


---
## Kernel config

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Kernel/Configuration>
    * <https://wiki.gentoo.org/wiki/Kernel/Gentoo_Kernel_Configuration_Guide>

```console
$ cd /usr/src/linux
# make nconfig # or `# make menuconfig` if you prefer
```

!!! Warning "Warning"
    After a kernel config, don't forget to refer to the below [Kernel make and
    rebuild](#kernel-make-and-rebuild) sub-section.


---
## Kernel make and rebuild

```console
$ cd /usr/src/linux

$ sudo make && sudo make modules_install # for faster compilation: add the "-jCPUs+1" option to the make command, e.g.: "$ sudo make -j9 && sudo make -j9 modules_install" for an 8 cores processor
```

!!! Warning "Warning"
    **At this point make sure that your boot directory is mounted!** Run `$ lsblk`, you should find
    a `/boot` mount point, if not: mount your boot directory (e.g. `# mount /dev/sda1 /boot`).

```console
# make install # (<https://wiki.gentoo.org/wiki/Kernel/Configuration#Setup>)
```

**If** you have any external kernel modules, such as binary kernel modules (e.g.
`x11-drivers/nvidia-drivers` or `sys-fs/zfs`), **then** they need to be rebuilt for each new kernel:
```console
# make modules_prepare # if the kernel has not been built previously
# emerge --ask @module-rebuild
```
> See <https://wiki.gentoo.org/wiki/Talk:Kernel/Upgrade#About_.27make_modules_prepare.27>
> See <https://wiki.gentoo.org/wiki/Kernel/Upgrade#Reinstalling_external_kernel_modules>

!!! Warning "Warning"
    **If** this [Kernel make and rebuild](#kernel-make-and-rebuild) was following the above [Kernel
    update procedure](#kernel-update-procedure) (when using the `# eselect kernel` command) then
    don't forget to refer to the below [GRUB2 update](#grub2-update) sub-section.

!!! Warning "Warning"
    **If** this [Kernel make and rebuild](#kernel-make-and-rebuild) was following the above [Kernel
    update procedure](#kernel-update-procedure) (when using the `# eselect kernel` command) then
    don't forget to also rebuild your [initramfs](https://wiki.gentoo.org/wiki/Initramfs) if you
    use one.

!!! Warning "Warning"
    **If** this [Kernel make and rebuild](#kernel-make-and-rebuild) was **not** following the above
    [Kernel update procedure](#kernel-update-procedure), then you might want to reboot and check
    that newly built kernel.


---
## GRUB2 update

If an `# eselect kernel set` has been made:
```console
# grub-mkconfig -o /boot/grub/grub.cfg
# reboot
```

!!! Tip "Tip"
    If your system does not reboot, you might want to boot on the the SystemRescueCd (see
    <http://www.system-rescue-cd.org/>) and `chroot` into your system in order to investigate.

!!! Warning "Warning"
    After a successful reboot, and after making sure that your system is stable and does work fine,
    don't forget to update your kernel in the `world` set: [Kernel in `world`
    set](#kernel-in-world-set). After that, you might want to refer to the below [Old kernel
    removal](#old-kernel-removal) sub-section.

---
## Old kernel removal

???+ Note "Reference(s)"
    See <https://wiki.gentoo.org/wiki/Kernel/Removal>

### Removing old or unused `gentoo-sources` (after switching to newer sources)

!!! Warning "Warning"
    Before removing old or unused `gentoo-sources`, don't forget to update your kernel in the `world`
    set: [Kernel in `world` set](#kernel-in-world-set).

```console
# emerge --ask --depclean gentoo-sources
```

### Removing kernel leftovers

!!! Warning "Warning"
    Before removing kernel leftovers, make sure that your system seems stable and does work fine
    enough to reboot and to eventually re-install a previous kernel version (if a recover becomes
    needed).

```console
# eclean-kernel -n 1 # keep only the last newest kernel around
```

**OR** (*manual removal*)

```console
# rm -rf /usr/src/linux-x.x.x-old

# rm -rf /lib/modules/x.x.x-old

# rm /boot/vmlinuz-x.x.x-old
# rm /boot/System.map-x.x.x-old
# rm /boot/config-x.x.x-old
```


---
## Kernel troubleshooting

TODO

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Kernel/Upgrade#Solving_build_problems>
    * <https://www.linuxquestions.org/questions/slackware-14/difference-between-make-mrproper-and-make-clean-473144/>

* `make clean` will remove most generated files: the object and kernel object files (ending in `.o`
  and `.ko`) and a few other things. Leave enough to build external modules

* `make distclean` removes editor backup files, patch leftover files and the like

* `make mrproper` will do everything make clean does, plus remove your `.config` file, the
  dependency files, and everything else that make config creates


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
