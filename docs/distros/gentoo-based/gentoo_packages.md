---
tags:
  - Linux Distributions
  - Distros
  - Gentoo Based
  - Package Management
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Gentoo packages management

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Handbook:X86/Portage/Tools>
    * <https://wiki.gentoo.org/wiki/Gentoo_Cheat_Sheet>
    * <https://wiki.gentoo.org/wiki/Talk:Gentoo_Cheat_Sheet>
    * <https://www.linuxtricks.fr/wiki/tableau-comparatif-des-outils-urpm-yum-zypper-emerge-et-apt-rosetta>
    * <https://www.gentoo.org/support/news-items/>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Packages basics](#packages-basics)
* [Packages investigation](#packages-investigation)
* [Packages updates](#packages-updates)
    * [Update packages repositories](#update-packages-repositories)
    * [Update a specific package](#update-a-specific-package)
    * [Update all system](#update-all-system)
    * [Special updates (after other installations or updates)](#special-updates-after-other-installations-or-updates)
* [Packages Logs](#packages-logs)
    * [Portage logs](#portage-logs)
    * [Gentoo news](#gentoo-news)
* [Packages sets](#packages-sets)
    * [`@selected` set packages (manually installed packages)](#selected-set-packages-manually-installed-packages)
    * [`@system` set packages](#system-set-packages)
    * [`@profile` set packages](#profile-set-packages)
    * [`@world` set packages](#world-set-packages)
* [Packages slots](#packages-slots)
* [Packages troubleshooting](#packages-troubleshooting)

<!-- vim-markdown-toc -->

---
## Packages basics

* Search a package:
```console
$ emerge -s package_name_containing_this_string # --search
$ emerge -S package_name_containing_this_description # --searchdesc
```

* Install a package:
```console
# emerge -av package_to_install_asking_confirmation # --ask --verbose
$ emerge -p package_pretending_being_installed_to_see_dependencies # --pretend
```

* Remove a package (all system wide configuration files and all dependencies, except `/home`
  configuration files):
```console
# emerge -aC package_to_remove # --ask --unmerge
```

* List all installed packages:
```console
$ equery list "*" # or `qlist -I`
```

* Downgrade a package (e.g. mesa should be `7.10.3`):
```console
$ cd /etc/portage
$ sudo touch package.mask
$ echo ">media-libs/mesa-7.10.3" >> /etc/portage/package.mask
# emerge -DuNv world
```


---
## Packages investigation

`equery` is a wonderful tool to investigate any package:
```console
$ man 1 equery
$ equery -h
```


---
## Packages updates

### Update packages repositories

```console
# emaint sync -a # sync all repos in /etc/portage/repo.conf and repos.conf (if auto-sync = yes)
or
# emerge-webrsync # if firewall prevent rsync from contacting the mirrors
or
# eix-sync # update, just like 'emaint sync', but also update the eix base
```

### Update a specific package

```console
# emerge -avuDN package_name
```

### Update all system

* Update all repositories:
    ```console
    # emaint sync -a # sync all repos in /etc/portage/repo.conf and repos.conf (if auto-sync = yes)
    ```

    !!! Warning
        If, at the end of the `emaint sync -a` command, you get a message like the following one:
        ```console
        ...
        Action: sync for repo: gentoo, returned code = 0
        Action: sync for repo: some-overlay-repo, returned code = 0
        Action: sync for repo: some-other-overlay-repo, returned code = 0
        Action: sync for repo: some-failing-overlay-repo, returned code = 1
        ...
        ```
        Then you will notice that the repository `some-failing-overlay-repo` has not been
        successfully synced (`returned code = 1`).

        In that case, *if you see this kind of error message*:
        ```console
        ...
        >>> Syncing repo 'some-failing-overlay-repo' into '/var/db/repos/some-failing-overlay-repo'...
            /usr/bin/git fetch origin
            Updating ...
            error: Your local changes to the following files would be overwritten by merge:
            ...
            <list of files>
            ...
            Please commit your changes or stash them before you merge.
            Aborting
        ...
        ```
        Then you can fix it like so:
        ```console
        $ cd /var/db/repos/some-failing-overlay-repo
        $ sudo git reset --hard
        $ sudo emaint sync -r some-failing-overlay-repo
        ```
        See <https://forums.gentoo.org/viewtopic-t-1077536-start-0.html>.

    !!! Warning
        *If the previous command says so*, then update portage:
        ```console
        # emerge --oneshot sys-apps/portage
        ```

    !!! Tip
        The `emerge --sync` command is a compatibility command. Sync operations should now be
        performed using the new `emaint sync` module. This new `emaint sync` module has greater
        functionality and flexibility. See `man emerge` and `man emaint`.

* Run the global update:

    !!! Note ""

        === "allowing kernel update"
            ```console
            # emerge -avutDN --with-bdeps=y @world # --ask --verbose --update --tree --deep --newuse
            ```

            !!! Warning
                If you want to allow the Linux kernel package (`gentoo-sources`) to be updated, then
                you might also want to [make sure the current version of your kernel is part of your
                `world` set](./gentoo_kernel.md#kernel-in-world-set).

        === "without allowing kernel update"
            In this case, just add the `--exclude gentoo-sources` flag:
            ```console
            # emerge -avutDN --exclude gentoo-sources --with-bdeps=y @world # --ask --verbose --update --tree --deep --newuse
            ```

    !!! Tip
        After the global update command (`# emerge -avuDN ...`), the following message might
        appear: `IMPORTANT: N config files in '/etc' need updating.`. In this case run the
        [`dispatch-conf`](./dispatch_conf.md) command, and re-run the global update.

    !!! Tip
        If you get a "Failed to emerge" error during the global update, then re run
        `# emaint sync -a` and `# emerge -avuDN ...` again. If the problem remains, it might be
        because a package requires a lot of memory in order to compile, try again after closing
        every big running programs (if any) like Steam, Firefox, etc. If the problem still
        remains, further investigation will be needed...

    !!! Warning
        After updating the system...

        * make sure you didn't missed any important update message by reading the [packages
          Logs](#packages-logs)
        * make sure you didn't missed any news: `$ eselect news read`. You can also read them all,
          here: <https://www.gentoo.org/support/news-items/>

* Now, update all new packages that are still built against their old version (**run it a few times
  if it still says** `!! existing preserved libs...`):
  ```console
  # emerge @preserved-rebuild
  ```

    !!! Tip
        If the `# emerge @preserved-rebuild` command complains with the following message: `!!
        existing preserved libs...` then run it a few more time until it stops complaining.

        If it still complains (even after running it a few times), e.g.
        (<https://forums.gentoo.org/viewtopic-t-1057608.html>):
        ```console
        !! existing preserved libs:
        >>> package: sys-libs/db-4.8.30-r2
         *  - /usr/lib64/libdb-4.8.so
         *      used by /usr/lib64/sasl2/libsasldb.so.3.0.0 (dev-libs/cyrus-sasl-2.1.26-r9)
         *      used by /usr/sbin/saslauthd (dev-libs/cyrus-sasl-2.1.26-r9)
         *      used by /usr/sbin/sasldblistusers2 (dev-libs/cyrus-sasl-2.1.26-r9)
        Use emerge @preserved-rebuild to rebuild packages using these libraries
        ```
        Then just `un-emerge` and `emerge` back the package that causes difficulties:
        ```console
        # emerge --unmerge dev-libs/cyrus-sasl-2.1.26-r9
        # emerge --oneshot dev-libs/cyrus-sasl
        ```

* Clean the system by removing/rebuilding packages:

    !!! Warning
        If you allowed the Linux kernel package (`gentoo-sources`) to be updated, then you might
        want to [make sure the current version of your kernel is part of your `world`
        set](./gentoo_kernel.md#kernel-in-world-set).

    ```console
    # emerge -p --depclean # list all packages that would be removed after deepclean
    # emerge --depclean # remove orphaned dependencies, only after a full update
    # revdep-rebuild # rebuild pkgs that were linked to now-removed pkgs
    ```

    !!! Tip
        **If** the previous command complains about "broken orphaned files", it's probably because
        you installed something from source that put files into `/usr` after a `$ sudo make
        install`. E.g.  after installing `rtags` (<https://github.com/Andersbakken/rtags.git>):
        ```console
        !! Broken orphaned files: No installed package was found for the following:
            * /usr/local/bin/rp
            * /usr/local/bin/rc
            * /usr/local/bin/rdm
        ```

        **Then** if you know the origin of those file (e.g. `rtags` case): it's fine, let those
        file be.  You can add them to `/etc/revdep-rebuild/99revdep-rebuild` to ignore them in the
        future:
        ```console
        # vi /etc/revdep-rebuild/99revdep-rebuild
            > ...
            > LD_LIBRARY_MASK="libodbcinst.so libodbc.so libjava.so libjvm.so /usr/local/bin/rp /usr/local/bin/rc /usr/loca/bin/rdm"
            > ...
        ```
        **Except** if you don't know the origin of those files! In this case further investigation
        is needed!

* Finally, clean/remove source code of old packages:
  ```console
  # eclean -d distfiles
  ```

!!! Warning
    If a global update (`# emerge -avuDN ...` command) has been run and that new `gentoo-sources`
    (new kernel) have been installed: take a look at how to [update the
    kernel](./gentoo_kernel.md#kernel-update).


### Special updates (after other installations or updates)

* upgrade GCC : <https://wiki.gentoo.org/wiki/Upgrading_GCC>

* update `zim` and `zim` modules:
```console
$ zimfw upgrade
$ zimfw update
```

* update `haskell`:
```console
# kaskell-updater
```

* update `dein`:
```console
$ nvim
    > :call dein#update()
```

* update `protonvpn-cli`:
```console
# pvpn --update
```

* update `fasd`:
```console
$ cd /gitapp/path/to/fasd
$ git pull
$ source $ZDOTDIR/.zshrc
```

* update `rustup`:
```console
$ rustup update
```

* update all cargo packages (need to install `install-update` first: `$ cargo install
  install-update`")
```console
$ cargo install-update -a
```


---
## Packages Logs

### Portage logs

You can check the daily logs like so:
```console
$ bat /var/log/portage/elog/summary.log # or "$ cat /var/log/portage/elog/summary.log | less"
$ bat *$(date +%Y%m%d)* # or "$ cat *$(date +%Y%m%d)* | less"
```

### Gentoo news

You can read the Gentoo news like so:
```console
$ eselect news list
$ eselect news read
$ man news.eselect
```

You can also read them all here: <https://www.gentoo.org/support/news-items/>


---
## Packages sets

* `@system` set: the system set, contains the packages required for a standard Gentoo to run
  properly.

* `@profile` set: the profile set, contains the packages required for a profile.

* `@selected` set: The selected set contains the packages the admin has **explicitly** installed.
  With a couple of exceptions, Portage will register those specified packages in the
  `/var/lib/portage/world` and `world_sets` files.

* `@world`: The world set, encompasses the `@system` set, `@profile` set and the `@selected` set.
  Whenever updating, portage will update to the last version the packages in the `@world` set (if
  no specific version has been specified). A package in this set might sometimes be called a "main
  package", in opposition with its "dependency packages" which are outside of this set (a
  dependency package is updated whenever it needs to be by portage, to the right version). Every
  package emerged without the `--oneshot` option (or `-1`) will end up in the `@world` set. You
  don't want a "dependency packages" to be here, because this package might be updated to the wrong
  version and break it's associated "main package".

* More information about those sets and all the others:
    * <https://wiki.gentoo.org/wiki//etc/portage/sets>
    * <https://wiki.gentoo.org/wiki/Package_sets>

### `@selected` set packages (manually installed packages)

* List `@selected` set packages:
  ```console
  $ emerge --pretend @selected
  ```
  or
  ```console
  $ cat /var/lib/portage/world
  $ cat /var/lib/portage/world_sets
  ```

* Emerge a package, from outside the `@world` set, to the `@selected` set without recompiling the
  package:
```console
# emerge --ask --noreplace package-name
```

* Emerge a package with a specific version (e.g. v `x.y.z`) to the `@selected` set:
```console
# emerge --ask --noreplace package-name:x.y.z
```
This way, this package won't be cleaned/removed if a newer version is emerged, both will be
there.

### `@system` set packages

* List `@system` set packages:
```console
$ emerge --pretend @system
```

### `@profile` set packages

* List `@system` set packages:
```console
$ emerge --pretend @profile
```

### `@world` set packages

* List `@world` set packages:
```console
$ emerge --pretend @world
```

* Emerge a package outside of the `@world` set (as if it is a dependency):
```console
# emerge --ask --oneshot package-name
```

* Remove package from the `@world` set without rerunning `emerge`:
```console
# vi /var/lib/portage/world # remove the package from this list
```


---
## Packages slots

Packages can support having multiple versions installed simultaneously. This is useful for
libraries which may have changed interfaces between versions. This feature is called slotting.

Most packages have no need for slotting. These packages specify `SLOT="0"`. This is not the same as
specifying an empty slot (`SLOT=""`), an empty slot means "disable slotting entirely", and should
not be used.

Print available slots of a package:
```console
$ equery list -p package-name
    > [-P-] [  ] app-thing/package-name-0.1.1:1.1
    > [-P-] [  ] app-thing/package-name-0.1.2:1.2
    > [IP-] [  ] app-thing/package-name-0.2.1:2
    > [-P-] [ -] app-thing/package-name-9999:0
```

Install a previous version of a package:
```console
# emerge -ap \=app-thing/package-name-1.1 # use '\=' or '=' as prefix depending on your shell
```


---
## Packages troubleshooting

```console
# revdep-rebuild -v # check for and rebuild missing libraries

# equery d package_name # print packages that depend on a specific package
# equery g --depth=1 package_name # print dependencies of a specific package
# equery f --tree package_name # print installed files by a specific package

# eix package_name # get information about a package

# e-file vim # tells wich not installed package provides a commdand (e.g. vim)

# equery b 'wich vim' # tells which installed package provides a command

# emerge -e @world # requests to reinstall everything you asked the package manager to have on the system, and everything that is needed, directly or indirectly, by those things (if a package is not listed, it is because it is no longer needed): see <https://forums.gentoo.org/viewtopic-t-1088610-start-0.html>
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
