---
tags:
  - Linux Distributions
  - Distros
  - NixOS Based
  - NixOS
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---



## How to update when using flakes

* Go to <https://github.com/NixOS/nixpkgs/tree/master/nixos/doc/manual/release-notes>, and look for
  the latest release note: the latest one that is **passed**, e.g. [release 23.05 ("Stoat",
  2023.05/31)](https://github.com/NixOS/nixpkgs/blob/master/nixos/doc/manual/release-notes/rl-2305.section.md)
  that has been done in may 2023.

* Read carefully the release note, in particular: pay attention to the "Backward Incompatibilities" section!

* Update your packages:

    ```console
    # vi /etc/nixos/flake.nix
        > {
      ~ >   inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05"
        >   ...

    # nixos-rebuild test
    ```

* Make sure that any manual migration steps required has been followed (according to the latest
  release note, e.g. [release 23.05 ("Stoat",
  2023.05/31)](https://github.com/NixOS/nixpkgs/blob/master/nixos/doc/manual/release-notes/rl-2305.section.md)).

* Update `stateVersion` in `/etc/nixos` like described here:
  <https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion>:

    ```console
    # vi /etc/nixos/flake.nix
        >   ...
      ~ >   system.stateVersion = "23.05"; 
        >   ...

    # nixos-rebuild test
    # nixos-rebuild boot

    # reboot

    # cd /etc/nixos
    # git add *
    # git commit -m "feat: update nixos to release 23.05"
    # git push
    ```

* Check versions updates:

    ```console
    $ nix profile diff-closures --profile /nix/var/nix/profiles/system 
    ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
