---
tags:
  - Linux Distributions
  - Distros
  - NixOS Based
  - NixOS
  - Installation Guide
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


!!! Warning " "
    This document does not replace the [official NixOS installation guide](https://nixos.org/manual/nixos/stable/)<br/>
---


# NixOS installation

**TODO**

!!! Note "Note"
    In this cheat sheet, one or multiple Linux distributions (distros) can already be installed on
    other partitions: they will be preserved and still available through `grub`.

!!! Note "Note"
    In this cheat sheet, the installation process is compatible with a virtual machine (VM). In this
    case, you might be interested in those cheat sheets:

    * [Qemu KVM](../../virtualization/qemu_kvm.md)
    * [Virtualbox](../../virtualization/virtualbox.md)

!!! Note "Note"
    In this cheat sheet, UEFI won't be used as a bootloader, like described here:
    <https://wiki.archlinux.org/index.php/EFISTUB>, the bootloader used here is GRUB2 (or `grub` for
    short) but it could be anything else you want.

???+ Note "Reference(s)"
    * <https://nixos.org/manual/nixos/stable/>
    * <https://nixos.org/download.html#nixos-iso>
    * <https://nixos.org/learn.html>
    * <https://nixos.wiki/wiki/Cheatsheet>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [1.0 Prerequisites](#10-prerequisites)
* [2.0 Preparing the disks: partitioning, formatting and mounting](#20-preparing-the-disks-partitioning-formatting-and-mounting)
    * [GPT (not MBR) and (U)EFI (not BIOS) partitioning, formatting and mounting](#gpt-not-mbr-and-uefi-not-bios-partitioning-formatting-and-mounting)
    * [MBR (not GPT) and BIOS (not (U)EFI) partitioning, formatting and mounting](#mbr-not-gpt-and-bios-not-uefi-partitioning-formatting-and-mounting)

<!-- vim-markdown-toc -->


---
## 1.0 Prerequisites

* Download the [latest NixOS stable ISO image](https://nixos.org/download.html#nixos-iso).

* Then follow [this cheat sheet about bootable media](../../hardware/bootable_media.md) in order to
  install the ISO image on a USB drive and **boot on it with UEFI!**

* The installation media contains a basic NixOS installation. When it’s finished booting, it should
  have detected most of your hardware.

!!! Tip "Tip"
    After booting on the USB drive, you can easily get an Ethernet connection to internet, but
    don't forget to setup a proxy if needed, e.g. `$ export http_proxy="http://10.100.6.193:8080"`
    and `$ export https_proxy="http://10.100.6.193:8080"`. Remember that in this case: `ping` may
    not work!

!!! Tip "Tip"
    If the keyboard layout feels wrong, it can be changed like so:
    ```console
    # loadkeys fr # e.g. switch the keyboard layout to French (or any other language) if needed
    ```

!!! Tip "Tip"
    If you want to resize a partition in order to make some place for another partition (e.g. to
    install NixOS in dual-boot with another distro), then you can refer to [this cheat sheet about
    partitioning with `gnome-parted`](../../drives/partitions.md#gnome-parted) **after booting on
    the USB drive**.

!!! Note "Useful documentations for the installation"
    * <https://nixos.org/manual/nixos/stable/>

!!! Tip "Tip"
    The NixOS manual is available by running `$ nixos-help`.

!!! Tip "Tip"
    You are logged-in automatically as NixOS. The NixOS user account has an empty password so you
    can use `sudo` without a password: `$ sudo -i`.

!!! Tip "Tip"
    If you downloaded the graphical ISO image, you can run `$ systemctl start display-manager` to
    start the desktop environment. If the text is too small to be legible, try `$ setfont ter-v32n`
    to increase the font size.


---
## 2.0 Preparing the disks: partitioning, formatting and mounting

Here you have a choice to make: either go GPT+(U)EFI or go MBR+BIOS.

What I recommend is to go MBR+BIOS **only if** you have a *very* old computer (in this case make
sure it is not UEFI compatible) **or if** you use a virtual machine (VM). **Else** go GPT+(U)EFI.

!!! Warning "Warning"
    You might already have a Linux distro installed on another partition, in this case **make sure
    to NOT override it when partitioning**. As described at the end of this cheat sheet, the
    `os-prober` tool will find this partition and make it also available through `grub`.

### GPT (not MBR) and (U)EFI (not BIOS) partitioning, formatting and mounting

Here, the tool used to partition the disks is `gdisk`:

* [partitions](../../drives/partitions.md)
* [`gdisk`](https://wiki.archlinux.org/index.php/Gdisk)
* [`gdisk` examples](https://debian-facile.org/doc:systeme:gdisk)

Check which disk to partition with the `# lsblk` command. Here, the disk to partition will be
called `/dev/sdx`.

Two or three partitions will be created on `sdx`:

* `sdx1` : UEFI ESP boot (512 Mo)

* `sdx2` : Linux Swap (OPTIONAL when ram > 8-16 Go, but necessary to hibernate, in this case you
  need as much swap as ram)

* `sdx3` : Linux File system main (home + root)

But first, **backup your partition table**:
```console
# sfdisk -d /dev/disk > disk.dump
```

!!! Tip "Tip"
    If you want to restore your partition table later, run: `# sfdisk /dev/disk < disk.dump`

Then proceed:
```console
# gdisk /dev/sdx # gpt partitioning (alternative : parted)
    > ? # list all commands
    > p # list all partitions
    > d # delete a partition

    > o # create a new empty GPT partition table
        > Y # validate new empty GPT (⚠️ delete all pre-existing partitions)

    > n # create a new partition (boot partition)
        > # enter to leave default part number (1 for sdx1)
        > # enter to leave default first sector
        > +512M # specify a 512Mo partition size
        > ef00 # specify an EFI System boot partition type

    > n # create new partition (optional swap)
        > # enter to leave default part number (2 for sdx2)
        > # enter to leave default first sector
        > +16384M # specify a 16Go (ram size) partition size
        > 8200 # specify a Linux Swap partition type

    > n # create new partition
        > # enter to leave default part number (3 for sdx3)
        > # enter to leave default first sector
        > # enter to get all the space left
        > 8300 #specify a Linux Filesystem partition type

    > p # print the table to ensure that it is correct

    > w # write the table, validate all previous actions and quit
        > Y # validate new table
```

Format and mount the partitions:
```console
# mkfs.fat -F 32 /dev/sdx1          # fat32 for EFI System boot
# mkfs.ext4 -L MAIN /dev/sdx3       # ext4 for Linux Filesystem

# mkswap -L SWAP /dev/sdx2          # ONLY IF a swap partion has been previously created
# swapon /dev/sdx2                  # mount swap

# mkdir -p /mnt/artix
# mount /dev/sdx3 /mnt/artix        # mount main partition

# mkdir -p /mnt/artix/boot
# mount /dev/sdx1 /mnt/artix/boot   # mount boot partition
```

### MBR (not GPT) and BIOS (not (U)EFI) partitioning, formatting and mounting

Here, the tool used to partition the disks is `fdisk`:

* [partitions](../../drives/partitions.md)
* [`fdisk`](https://wiki.archlinux.org/index.php/Fdisk)

Check which disk to partition with the `# lsblk` command (for a virtual machine it will probably be
`/dev/sda`, or `/dev/vda`), here the disk to partition will be called `/dev/sdx`.

One or two partitions will be created on `sdx`:

* `sdx1` : Linux Swap (OPTIONAL when ram > 8-16 Go, but necessary to hibernate, in this case you
  need as much swap as ram)

* `sdx2` : Linux File system main (home + root)

But first, **backup your partition table**:
```console
# sfdisk -d /dev/disk > disk.dump
```

!!! Tip "Tip"
    If you want to restore your partition table later, run: `# sfdisk /dev/disk < disk.dump`

Then proceed:
```console
# fdisk /dev/sdx
    > m # help screen

    > o # create a new empty MBR partition table

    > n # create a new partition: the Linux Filesystem main partition
        > # press enter to leave default partition type: primary
        > # press enter to leave default partition number: 1
        > # press enter to leave default first sector: 2048
        > -8G # OPTIONAL leave 8Go for the SWAP or just press enter to leave default last sector
        > Y # remove signature if any

    > a # toogle the bootable flag
        > # press enter to leave default bootable partition: 1

    > n # [OPTIONAL] create a new partition: the SWAP partition
        > # press enter to leave default partition type: primary
        > # press enter to leave default partition number: 2
        > # press enter to leave default first sector
        > # press enter to leave default last sector

        > t # change the partition type of the SWAP from default "Linux" to "Linux swap / Solaris"
        > 2 # select the partition number of the SWAP
        > 82 # enter the hex code of the "Linux swap / Solaris"

    > w # write the table to the disk and exit
```

Format and mount the partitions:
```console
# mkfs.ext4 -L MAIN /dev/sdx1 # ext4 for Linux Filesystem

# mkswap -L SWAP /dev/sdx2    # optional: ONLY IF a swap partion has been previously created
# swapon /dev/sdx2            # optional: ONLY IF a swap partion has been previously created

# mkdir -p /mnt/artix
# mount /dev/sdx1 /mnt/artix  # mount main partition
```

---

```console
$ nixos-help
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
