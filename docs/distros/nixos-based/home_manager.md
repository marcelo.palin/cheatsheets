---
tags:
  - Linux Distributions
  - Distros
  - NixOS
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Home Manager

Home Manager provides a basic system for managing a user environment using the Nix package manager
together with the Nix libraries found in `Nixpkgs`. It allows declarative configuration of user
specific (non global) packages and dotfiles.

???+ Note "Reference(s)"
    * <https://github.com/nix-community/home-manager>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

**TODO**

---
## Config

**TODO**

---
## Use

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
