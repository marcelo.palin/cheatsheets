---
tags:
  - System Administration
  - Sysadmin
  - Linux Distributions
  - Distros
  - Build Linux
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Linux From Scratch (LFS)

???+ Note "Reference(s)"
    * <http://www.linuxfromscratch.org/>
    * <https://en.wikipedia.org/wiki/Linux_From_Scratch>
    * <https://distrowatch.com/table.php?distribution=lfs>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
