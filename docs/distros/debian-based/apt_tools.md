---
tags:
  - Linux Distributions
  - Distros
  - Debian Based
  - Package Management
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# APT tools

APT, is a package management system used on most Debian based distros.

It should not be mistaken with `apt`, an APT tool which provides a high-level command line
interface for the package management system. It is intended as an end user interface and enables
some options better suited for interactive usage by default compared to more specialized tools like
`apt-get` and `apt-cache` (which are both considered as lower-level and more backwards compatible,
so better suited for scripting).

Do not mistake `apt` with `Aptitude`, which not only has a command line interface but also a
`ncurses` interface. `Aptitude` is considered a higher-level package manager that abstract more
low-level details.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Pacman/Rosetta>
    * <https://www.tecmint.com/linux-package-management/>
    * <https://www.tecmint.com/difference-between-apt-and-aptitude/>
    * <https://www.tecmint.com/useful-basic-commands-of-apt-get-and-apt-cache-for-package-management>
    * <https://help.ubuntu.com/kubuntu/desktopguide/C/apt-get.html>
    * <https://help.ubuntu.com/community/AptitudeSurvivalGuide>
    * <http://manpages.ubuntu.com/manpages/xenial/man8/apt.8.html>
    * <https://help.ubuntu.com/community/AptGet/Howto>


---
## Table of content

<!-- vim-markdown-toc GitLab -->

* [`apt`](#apt)
    * [Search package files](#search-package-files)
    * [Install deb package](#install-deb-package)
    * [Uninstall package](#uninstall-package)
* [`apt-get`](#apt-get)
* [`apt-cache`](#apt-cache)
* [Aptitude](#aptitude)

<!-- vim-markdown-toc -->

---
## `apt`

* Check whether `packagename` is installed:
```console
$ apt -qq list packagename
```

* Full system update/upgrade:
```console
# apt update
# apt list --upgradable
# apt full-upgrade
# apt --purge autoremove # removes deps and systemwide configuration files too
```

* Remove a package
```console
# apt purge package-to-remove
# apt --purge autoremove # removes deps and systemwide configuration files too
```

* List all installed packages:
```console
$ apt list --installed
```

* List manually installed packages (not their dependencies):
```console
$ grep " install " $HOME/.bash_history
```

### Search package files

Install `apt-file` utility:
```console
# apt install apt-file
```

Update it:
```console
# apt-file update
```

Search for a file
```console
$ apt-file search fontconfig.pc
```

### Install deb package

???+ Note "Reference(s)"
    * <https://itsfoss.com/install-deb-files-ubuntu/>

```console
# apt install path_to_deb_file
```

If you get dependencies errors when installing the deb package, you might want to force the
installation anyway:
```console
# apt install -f
```

### Uninstall package

```console
$ apt remove program_name
```


---
## `apt-get`

**TODO**


---
## `apt-cache`

**TODO**


---
## Aptitude

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
