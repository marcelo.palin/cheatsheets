---
tags:
  - Linux Distributions
  - Distros
  - Debian Based
  - Ubuntu
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Ubuntu management

???+ Note "Reference(s)"
    * <https://web.archive.org/web/20220530145941/https://dr-knz.net/ubuntu-without-snap.html>
    * <https://askubuntu.com/questions/1369159/how-to-remove-snap-completely-without-losing-firefox>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [How to remove `avahi` daemon from Ubuntu](#how-to-remove-avahi-daemon-from-ubuntu)
* [How to remove `snap` from Ubuntu](#how-to-remove-snap-from-ubuntu)
* [Switch from Gnome to Xfce (and vice versa)](#switch-from-gnome-to-xfce-and-vice-versa)
* [Switch from GDM to LightDM (and vice versa)](#switch-from-gdm-to-lightdm-and-vice-versa)
* [Troubleshooting](#troubleshooting)
    * [Headphone sound always muted](#headphone-sound-always-muted)

<!-- vim-markdown-toc -->

---
## How to remove `avahi` daemon from Ubuntu

`avahi` is the open-source implementation of `zeroconf`, used for automatic management of networks
without any centralized configuration. Generally, `avahi` is only useful in small local networks
(such as a home LAN).

The `avahi-daemon` process handles mDNS, which is used for name resolution and service discovery
within the local network. If you don't need its functions, `avahi` can be safely removed.

Local name resolution allows you to reach the computer by a host `name.local` name without any
centralized configuration. Since your server most likely has a DNS name, this function is
unnecessary.

Service discovery allows programs to find and advertise running services (for example, to allow
listing all SSH or PulseAudio servers in the LAN). This is convenient on a home network, but
entirely useless for a data center hosted server. This too can be safely removed like so:

```console
$ sudo systemctl stop avahi-daemon
$ sudo systemctl disable avahi-daemon
$ sudo vi /etc/default/avahi-daemon
    > ...
    > AVAHI_DAEMON_DETECT_LOCAL=0
    > ...

$ sudo apt purge avahi-daemon
$ sudo apt --purge autoremove
```

---
## How to remove `snap` from Ubuntu

* List snap packages ("snaps"):
    ```console
    $ snap list
    ```

* Remove all installed snaps:
    ```console
    $ sudo for p in $(snap list | awk '{print $1}'); do sudo snap remove $p; done
    ```

* Cleanup the `core` package:
    ```console
    $ sudo systemctl stop snapd
    $ for m in /snap/core/*; do sudo umount $m; done
    $ sudo snap remove core
    ```
    If anything remains, the steps below will take care of it.


* Uninstall `snapd`:
    ```console
    $ sudo apt autoremove --purge snapd
    ```

* Remove leftover directories:
    ```console
    $ rm -rf ~/snap
    $ sudo rm -rf /snap
    $ sudo rm -rf /var/snap
    $ sudo rm -rf /var/lib/snapd
    $ sudo rm -rf /var/cache/snapd
    ```

* Block future installs:
    ```console
    $ sudo vi /etc/apt/preferences.d/no-snapd.pref
        > Package: snapd
        > Pin: origin ""
        > Pin-Priority: -10
    ```


---
## Switch from Gnome to Xfce (and vice versa)

* Install Xfce for Ubuntu:
    ```console
    $ sudo apt install xubuntu-desktop # or `sudo apt install xfce4 xfce4-goodies` if you prefer a more "vanilla" Xfce
    ```

* Now, at login, in your display manager, you can switch from Gnome to Xfce

---
## Switch from GDM to LightDM (and vice versa)

* Install lightdm:
    ```console
    $ sudo apt install lightdm
    ```

* Select lightdm as default display manager and reboot:
    ```console
    $ sudo dpkg-reconfigure lightdm
    $ sudo reboot
    ```

    Or select gdm as default display manager and reboot:
    ```console
    $ sudo dpkg-reconfigure gdm3
    $ sudo reboot
    ```

---
## Troubleshooting

### Headphone sound always muted

If your headphone sound is always muted (for example after an update), even when trying to mix the
sound, e.g. with `pulsemixer` or with `pulseaudio` volume control, then try to restore your sound
card driver state from its configuration file: `$ alsactl restore`


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
