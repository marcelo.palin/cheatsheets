---
tags:
  - Android
  - Terminal
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `termux`

`termux` is an Android terminal emulator and Linux environment application that works directly with
no rooting or setup required. A minimal base system is installed automatically, additional packages
are available using the package manager (with a dedicated `termux` APT repository.

???+ Note "Reference(s)"
    * <https://wiki.termux.com/wiki/Main_Page>
    * <https://wiki.termux.com/wiki/FAQ>
    * <https://github.com/termux/>
    * <https://github.com/termux/termux-app>
    * <https://github.com/termux/termux-packages>
    * <https://f-droid.org/en/packages/com.termux/>
    * <https://f-droid.org/en/packages/com.termux.api/>
    * <https://f-droid.org/en/packages/com.termux.boot/>
    * <https://f-droid.org/en/packages/com.termux.widget/>

---
## table of contents

<!-- vim-markdown-toc GitLab -->

* [install](#install)
* [config](#config)
* [use](#use)
    * [basic `termux` commands](#basic-termux-commands)
    * [access to `termux` via ssh over local WiFi](#access-to-termux-via-ssh-over-local-wifi)
    * [access to `termux` via ssh over USB](#access-to-termux-via-ssh-over-usb)
    * [automatic and periodic backup via `rsync`](#automatic-and-periodic-backup-via-rsync)
    * [misc](#misc)
    * [TODO](#todo)

<!-- vim-markdown-toc -->

---
## install

* Install:
    * [`termux`](https://f-droid.org/en/packages/com.termux/)
    * [`termux:api`](https://f-droid.org/en/packages/com.termux.api/) (expose basic android
      functionalities)
    * [`termux:boot`](https://f-droid.org/en/packages/com.termux.boot/) (allows programs to be run
      at boot)
    * [`termux:widget`](https://f-droid.org/en/packages/com.termux.widget/) (adds shortcuts to
      `termux` scripts and commands on the home screen)

* In `termux`, install the `termux-api` package:
  ```console
  $ pkg termux-api
  ```

---
## config

* [Allow to access shared and external storage from
  `termux`](https://wiki.termux.com/wiki/Internal_and_external_storage)

* In `termux`, make sure `termux-api` can access SMS, contacts and location:
  ```console
  $ termux-sms-list
  $ termux-contact-list
  $ termux-location
  ```

* In your android home screen, swipe left to access your launcher's widget menu -> edit -> add
  widget -> `termux` widget (now your `termux` widgets will appear in the `termux` section)

---
## use

### basic `termux` commands

* search `termux` packages:
```console
$ pkg search package-name
```

* install `termux` packages:
```console
$ pkg install package-name
```

* update `termux` packages:
```console
$ pkg upgrade
```

* remove `termux` packages:
```console
$
```

### access to `termux` via ssh over local WiFi

* In `termux`, run `$ whoami` to get your user name

* In `termux`, run `$ passwd` to setup your user password

* In `termux`, run `$ ifconfig` to get your local IP (on the same WiFi that your GNU/Linux distro)

* In `termux`, install `openssh`: `$ pkg install openssh`

* In `termux`, run `$ sshd` in order to startup your ssh server (`$ pkill sshd` to stop it)

* In GNU/Linux, run `$ ssh -p 8022 user-name@local-ip` in order to connect

* OPTIONAL: In `termux`, create widgets shortcuts to start and stop `sshd`:
  ```console
  $ mkdir -p $HOME/.shortcuts/
  $ vi $HOME/.shortcuts/tasks/start-sshd
    > sshd
  $ vi $HOME/.shortcuts/tasks/kill-sshd
    > pkill sshd
  ```

### access to `termux` via ssh over USB

!!! Warning "Prerequisite"
    * [ADB](./adb.md)

* The procedure is almost the same than for [accessing `termux` via ssh over local
  WiFi](#access-to-termux-via-ssh-over-local-wifi), **but** in GNU/Linux: instead of running `$ ssh
  -p 8022 user-name@local-ip` run the following:
  ```console
  $ sudo /opt/android-sdk/platform-tools/adb forward tcp:8022 tcp:8022
  $ ssh -p 8022 user-name@127.0.0.1
  ```

### automatic and periodic backup via `rsync`

* In `termux`, install `rsync` and `openssh`:
  ```console
  $ pkg install openssh rsync
  ```

* In `termux`, generate a RSA key pair and share the public key with your server:
  ```console
  $ ssh-keygen -o -t rsa -b 4096 -f "/$HOME/.ssh/ssh_android_rsa_key" -N ""
  $ ssh-copy-id -i $HOME/.ssh/ssh_android_rsa_key.pub server-user@server-ip
  ```

* In `termux`, create this script:
  ```console
  $ mkdir -p $HOME/scripts
  $ vi $HOME/sync-android-with-server
    > #! /data/data/com.termux/files/usr/bin/bash
    >
    > mkdir -p $HOME/.tmp
    > mkdir -p $HOME/.logs
    >
    > rsync --archive --compress --human-readable --progress $HOME/storage/dcim/OpenCamera server-user@server-ip:/path/to/android/backup >| $HOME/.logs/last-rsync-cam.log
    >
    > termux-sms-list >| $HOME/.tmp/termux-sms-list
    >
    > rsync --archive --compress --human-readable --progress $HOME/.tmp/termux-sms-list server-user@server-ip:/path/to/android/backup >| $HOME/.logs/last-rsync-sms.log
    >
    > termux-contact-list >| $HOME/.tmp/termux-contact-list
    >
    > rsync --archive --compress --human-readable --progress $HOME/.tmp/termux-contact-list server-user@server-ip:/path/to/android/backup >| $HOME/.logs/last-rsync-contacts.log
    >
    > #termux-location >| $HOME/.tmp/termux-location
    >
    > #rsync --archive --compress --human-readable --progress $HOME/.tmp/termux-location server-user@server-ip:/path/to/android/backup >| $HOME/.logs/last-rsync-location.log
    >
  $ chmod +x sync-android-with-server
  ```

* OPTIONAL: In `termux`, you can create a widget shortcut to trigger the previous script manually:
  ```console
  $ mkdir -p $HOME/.shortcuts/tasks
  $ ln -s $HOME/scripts/sync-android-with-server $HOME/.shortcuts/tasks
  ```

* Install `termux-job-scheduler` instead of `cronie` (for Cron), in order to get periodic scripts
  execution without having to prevent the device from sleeping, e.g. with the previous script
  (every hour, even after reboots):
  ```console
  $ pkg install termux-job-scheduler
  $ termux-job-scheduler -s $HOME/scripts/sync-android-with-server --period-ms 3600000 --persisted true
  $ termux-job-scheduler -p # check that the script is active
  ```

### misc

* In `termux`, install `runnit` (`termux-services`), install `cronie` and enable it, and edit your
  `crontab`:
  ```console
  $ pkg install cronie termux-services
  $ sv-enable crond
  $ crontab -e
  ```

### TODO

* extract apps list


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
