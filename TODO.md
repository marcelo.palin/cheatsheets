# TODO

- Monitoring tools:
    - zenith : terminal system monitor with histograms, written in Rust
    - sysstat : a collection of performance monitoring tools (iostat,isag,mpstat,pidstat,sadf,sar)
    - dstat : a versatile resource statistics tool
    - glances : CLI curses-based monitoring tool
    - nmon : AIX & Linux Performance Monitoring tool
    - btop : a monitor of system resources, bpytop ported to C++
    - top and htop : interactive process viewer
    - iotop : interactive I/O processes viewer?

- [pytest](https://docs.pytest.org/en/7.4.x/) for tests in Python.

- [mypy](https://github.com/python/mypy) for tests in Python.

- [Semantic Line Breaks](https://sembr.org/) for all `.md` files. Ideally with a parser.

- [vale](https://vale.sh/)

- `$ grep -ri "todo" ./docs | grep -o "^\S*\.md" | sort -u`

- Rename `cheatsheets` to `wiki`, or `memorandum` (or just `memo`?), or `pages`?

- Replace `cheatsheets.stephane.plus` by `wiki.stephane.plus`, or `memorandum.stephane.plus` (or
  `memo.stephane.plus`), or `pages.stephane.plus`).
    - Make sure a good redirection from `cheatsheets.stephane.plus` is in place.
    - Also host on [GitLab
      pages](https://squidfunk.github.io/mkdocs-material/publishing-your-site/#gitlab-pages) (an
      not only readthedocs)?

- Split `./distro/common_installation_steps.md` into a directory with disctinct `.md` files in it.

- Merge <git@gitlab.com:stephane.tzvetkov/tech-practice.git> into the `dev` section.

- Replace `A correct [kernel config]` by `For Gentoo users only: a correct [kernel config]`.

- Change sections names, maybe like done here:
  <https://wiki.archlinux.org/index.php/List_of_applications>?

- Ideally, sections names should be tags names. This way, a single page could be referenced in
  multiple sections.

- Remove `System Administration` and `Sysadmin` tags? Those are too vague...

- Add comments at the end of each page ? Via Mastodon ? See
  <https://bwog-notes.chagratt.site/2023/des-commentaires-via-le-f%C3%A9diverse/>.

- Whenever possible, link related man pages in the references (e.g. <https://manned.org/sysctl.8>
  for sysctl).

- Replace the `Double check here:` instruction (in Gentoo kernel configuration notes) by something
  more visible like `⚠️ DOUBLE CHECK HERE⚠️:`.

- Add a header information/warning about the last time a memo has been *thoroughly reviewed* (not
  updated, but reviewed), i.e. when was the last time the associated references have been studied
  and the full memo has been followed / walked through and tested.

- Add a HTTP/HTTPS client interface, like `cheat.sh` (see `$ curl cheat.sh`).

- Add a CLI interface, like like `cht.sh` (see `$ curl cheat.sh/:intro`)?

- Add MIT License

- Add `monitoring` tag to relevant cheatsheets (like cockpit.md).

- Clean tags of cheatsheets in the `distros` folder.

- Look for new `mkdocs-material` functionalities:
    - check <https://squidfunk.github.io/mkdocs-material/setup/extensions/>
    - check <https://squidfunk.github.io/mkdocs-material/setup/changing-the-colors/>
    - check <https://squidfunk.github.io/mkdocs-material/setup/extensions/>
    - check <https://squidfunk.github.io/mkdocs-material/insiders/>

- Add a comment system?
    - <https://squidfunk.github.io/mkdocs-material/setup/adding-a-comment-system/>
    - <https://github.com/posativ/isso>

- Add code blocks [lines
  highlighting](https://squidfunk.github.io/mkdocs-material/reference/code-blocks/#highlighting-specific-lines)
  where possible.

- Add code blocks
  [annotations](https://squidfunk.github.io/mkdocs-material/reference/code-blocks/#adding-annotations)
  where possible.

- Add [inline
  blocks](https://squidfunk.github.io/mkdocs-material/reference/admonitions/#inline-blocks) where
  possible.

- Add the `nix` package manager installation option like in `cron.md`.
- Add more package managers options (see
  <https://en.wikipedia.org/wiki/List_of_software_package_management_systems> and
  <https://www.slant.co/topics/344/~best-linux-package-managers>). In particular, `apk` and `xbps`
- Sort package managers options alphabetically?

- Look self-hosted solutions: <https://github.com/awesome-selfhosted/awesome-selfhosted>

- Replace certificates sections of the `server` category by a reference to the `certbot` section of
  `nginx`.

- Replace `${XDG_DATA_HOME:-${HOME/.local/share}}/<...>` by
  `${XDG_DATA_HOME:-$HOME/.local/share}/<...>`.

- Improve services related cheat sheets.
    - See [`nginx`](./docs/server/nginx.md) cheat sheet as a reference!
    - E.g. replace:
      ```console
      # sv up service_name
      # ln -s /etc/runit/sv/service_name /run/runit/service/
      ```
      by:

      Depending on your `runit` implementation, either run:
      ```console
      # ln -s /etc/runit/sv/service_name /service
      ```
      **or** run:
      ```console
      # ln -s /etc/runit/sv/service_name /var/service
      ```
      **or** run:
      ```console
      # ln -s /etc/runit/sv/service_name /run/runit/service
      ```

      In any case, finaly run:
      ```console
      # sv up service_name
      ```

- `.venv` instead of `.pyenv` or `pyenv`
- `pip` instead of `python -m pip` (after checking with `$ which pip && which python`)

- Add support links (XMR donations, <https://opencollective.com>, <https://liberapay.com/>,
  <https://coindrop.to/>,?)

- Remove `Programs`, `Tools`, etc, from tags names?

- Rename `security` section into `security and privacy`?

- When installing with a Gentoo overlay, use the same installation format than `browsh.md`

- When installing with `AUR`, use the same installation format than `browsh.md`

---
## TO TEST

- Test that code blocks have a dedicate `'''console` line (and not `'''console and some characters
  here that have been misplaced`)
- Test that directories names do not contains `_` but `-` as word separators
- Test that markdown file names do not contains `-` but `_` as word separators
- Test the tags in header
- Test that indented admonitions must have 4 spaces (or 8, or 12, or any multiple of 4 spaces) in
  front of them. And not any other number of spaces (even 2 spaces, e.g. in a list)!
- Test that any line directly after list item (`*`,`-`) must be indented with 4 spaces?
- Test that any line that is a list item (`*`,`-`) must start with `*   ` or `-   `?
- Test that every paragraph starting with `*` has an empty line above it (except if this line above
  contains `!!!` or `???`)


---
## Cheat sheets to complete

- Add `suite66` details to the `init_sytems.md` cheat sheet. See
  <https://wiki.artixlinux.org/Main/Suite66#Start>

---
## New cheat sheets to add

- Cheat sheet about the `{ip,ip6,arp,eb}tables` framework and [`netfilter`](https://netfilter.org/)
  (in network section) linking the `iptables` cheat sheet and the bellow cheat sheets:
    - Cheat sheet about `ip6tables`.
    - Cheat sheet about `arptables`.
    - Cheat sheet about `ebtables`.
    - Cheat sheet about `nftables`.

- Cheat sheets about [SystemD](https://wiki.archlinux.org/title/Systemd) (in a dedicated section or
  subsection):
    - <https://wiki.archlinux.org/index.php?search=Systemd&title=Special%3ASearch&profile=default&fulltext=1>
    - <https://wiki.archlinux.org/title/Systemd/User>
    - <https://wiki.archlinux.org/title/Systemd/Timers>
    - <https://wiki.archlinux.org/title/Systemd/Journal>
    - <https://wiki.archlinux.org/title/Systemd/FAQ>
    - <https://wiki.archlinux.org/title/Systemd-timesyncd>
    - <https://www.freedesktop.org/wiki/Software/systemd/timedated/>
    - <https://wiki.archlinux.org/title/Systemd-nspawn>

- Cheat sheet about system time (in dedicated time section? With NTP cheat sheets?):
  <https://wiki.archlinux.org/title/System_time>

- Cheat sheet about default shortcuts (in admin section), e.g. `CTRL+R`.

- Cheat sheet about `fuck` (in admin section): <https://github.com/nvbn/thefuck>
- Cheat sheet about `paperless-ng` (or paperless-ngx) (in self-hosted section)

- Cheat sheet about `sysctl` (in admin section)
- Cheat sheet about `systeroid` (in admin section): <https://github.com/orhun/systeroid>

- Cheat sheet about `inxi`
- Cheat sheet about `xmouseless` (in graphical section): <https://github.com/jbensmann/xmouseless>
  <https://www.youtube.com/watch?v=h080D6-dgt4> (suckless like tool)
- Cheat sheet about `keynav` (in graphical section): <https://github.com/jordansissel/keynav>
  <https://www.youtube.com/watch?v=h080D6-dgt4>

- Cheat sheet about `wall` (in admin section):
  <https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-3/>
- Cheat sheet about `who` (in admin section):
  <https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-3/>
- Cheat sheet about `chfn` (in admin section):
  <https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-3/>
- Cheat sheet about `zcat`, `zgrep` and `zless` (in admin section):
  <https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-3/>
- Cheat sheet about `cat` and `tac` (in admin section):
  <https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-3/>
- Cheat sheet about `history` (in admin section):
  <https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-3/>
- Cheat sheet about `chattr` and `lsattr` (in admin section):
  <https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-1/>
- Cheat sheet about `last` and `lastb` (in admin section):
  <https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-1/>
- Cheat sheet about `yes` (in admin section):
  <https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-1/>
- Cheat sheet about `wc` (in admin section):
  <https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-2/>
- Cheat sheet about `chage` (in admin section):
  <https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-2/>
- Cheat sheet about `tr` (in admin section):
  <https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-2/>

- Cheat sheet about `whatfiles` (in admin section): <https://github.com/spieglt/whatfiles>,
  <https://www.it-connect.fr/linux-tracer-les-acces-au-systeme-de-fichiers-dun-programme/>
- +++ Cheat sheet about `strace` (in admin section):
  <https://www.it-connect.fr/linux-tracer-les-acces-au-systeme-de-fichiers-dun-programme/>


- Cheat sheet about `/etc/services` (in file section?)

- Cheat sheets about "new-ish" commands:
  <https://jvns.ca/blog/2022/04/12/a-list-of-new-ish--command-line-tools/>

- Cheat sheets about all Unix commands: <https://en.wikipedia.org/wiki/List_of_Unix_commands> (in
  admin section?) AND create an associated `Unix Commands` tag.
- Cheat sheets about all Core commands:
  <https://en.wikipedia.org/wiki/List_of_GNU_Core_Utilities_commands> (in admin section?) AND
  create an associated `GNU Core Utilities Commands` tag.
- Cheat sheets about Standard Unix commands:
  <https://en.wikipedia.org/wiki/Category:Standard_Unix_programs> (in admin section?) AND create an
  associated `Unix Commands` tag.
- Cheat sheets about all Unix Commands: <https://en.wikipedia.org/wiki/Category:Unix_software> (in
  admin section?) AND create an associated `Unix Command` tag.
- Cheat sheets about Non-Core Utilities Commands: <https://en.wikipedia.org/wiki/Util-linux> (in
  admin section?) AND create an associated `Non-Core Utilities Commands` tag.
- Cheat sheets about GNU Binary Utilities: <https://en.wikipedia.org/wiki/GNU_Binutils> (in admin
  section?) AND create associated `GNU Binary Utilities`, `binutils` tags.

- Cheat sheet about `hash` and `rehash` (in admin section):
  <https://superuser.com/questions/490983/how-to-rehash-executables-in-path-with-bash>
- Cheat sheet about `debuginfod` (where?): <https://wiki.archlinux.org/title/Debuginfod>,
  <https://wiki.archlinux.org/title/Debugging/Getting_traces>
- Cheat sheet about `firefox` (in misc section): `$ curl cheat.sh/firefox`
- Cheat sheet about `mcfly` (in admin section): <https://github.com/cantino/mcfly>
- Cheat sheet about `atuin` (admin section): <https://github.com/ellie/atuin>
- Cheat sheet about `screen` (productivity section)
- Cheat sheet about `mosh` (productivity section): <https://wiki.archlinux.org/title/Mosh>
- Cheat sheet about `losetup` (admin section) (to mount .iso)
- Cheat sheet about `bash shortcuts` (admin section):
    - <https://www.tecmint.com/linux-command-line-bash-shortcut-keys/>
    - <https://kapeli.com/cheat_sheets/Bash_Shortcuts.docset/Contents/Resources/Documents/index>
    - <https://www.howtogeek.com/howto/ubuntu/keyboard-shortcuts-for-bash-command-shell-for-ubuntu-debian-suse-redhat-linux-etc/>
    - <https://gist.github.com/tuxfight3r/60051ac67c5f0445efee>
- Cheat sheet about `starship`: <https://github.com/starship/starship> (shell section)
- Cheat sheet about `swag`: <https://github.com/linuxserver/docker-swag>
- Cheat sheet about `authelia`: <https://github.com/authelia/authelia>
- Cheat sheet about `ddclient`: <https://github.com/ddclient/ddclient>
- Cheat sheet about `PiKVM`: <https://github.com/pikvm/pikvm>
    - or an alternative: <https://duckduckgo.com/?q=alternative+to+pikvm&t=ffab&ia=web>
- Cheat sheet about `Pi-hole`: <https://github.com/pi-hole/pi-hole>
    - or an alternative: <https://duckduckgo.com/?t=ffab&q=alternative+to+pihole&ia=web>
- Cheat sheet about `home assistant`:
  <https://github.com/linuxserver/docker-homeassistant#linuxserverhomeassistant>
    - or an alternative:
      <https://github.com/linuxserver/docker-homeassistant#linuxserverhomeassistant>
- Cheat sheet about `MergerFS+SnapRAID`:
  <https://web.archive.org/web/20210306172014/https://blog.fuzzymistborn.com/mergerfs/>
- Cheat sheet about `top`?
- Cheat sheet about `htop`?
- Cheat sheet about `btop`?
- Cheat sheet about `iotop`?
- Cheat sheet about `atop`?
- Cheat sheet about <https://wiki.gentoo.org/wiki/Recommended_tools>
- Cheat sheet about `gprof` (as an alternative to `valgrind`, in dev/tools section):
  <https://web.archive.org/save/https://www.mjr19.org.uk/sw/op2kcg/callgrind.html>

- Cheat sheet about a self-hosted photos server:
    - [photoview](https://github.com/photoview/photoview)
    - [piwigo](https://github.com/Piwigo/Piwigo)
    - [librephotos](https://github.com/LibrePhotos/librephotos)
    - [lychee](https://github.com/LycheeOrg/Lychee)
    - [photoprism](https://github.com/photoprism/photoprism)
    - <https://alternativeto.net/software/google-photos/?license=opensource&platform=self-hosted>

- Cheat sheets about more self-hosted applications (see <https://landchad.net/> and
  <https://ouafnico.shivaserv.fr/posts/infra-hebergement/>):
    - mails server (dovecot/postfix/spamassassin?)
    - server monitoring (prometheus/alertmanager/grafana?)
    - CCTV (motion+motioneye?)
    - git server (gitea?)

- Cheat sheets about hardware detection: <https://wiki.gentoo.org/wiki/Hardware_detection>

- Cheat sheets about security related topics:
    - <https://www.youtube.com/watch?v=lGgwPmS_VuU>
    - <https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot>
    - <https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot#See_also>
    - <https://wiki.gentoo.org/wiki/Security_Handbook/Full>
    - <https://wiki.archlinux.org/index.php/Security>
    - <https://en.wikipedia.org/wiki/Linux_Security_Modules> (with the associated `Linux Security
      Modules` tag)

- Cheat sheets about general recommandations:
    - <https://wiki.archlinux.org/index.php/General_recommendations>

- Cheat sheets about some of the applications presented in the Arch Wiki:
  <https://wiki.archlinux.org/index.php/List_of_applications>

- Cheat sheets about network tools:
  <https://www.reddit.com/r/linux/comments/mzncxc/linux_networking_tool_with_simpler_understanding/>

---
## Useful references

- <https://github.com/awesome-selfhosted/awesome-selfhosted>
- <https://unix.stackexchange.com/questions/62355/is-there-a-tool-website-to-compare-package-status-in-different-linux-distributio>
- <https://repology.org/>
- <https://wiki.archlinux.org/index.php/List_of_applications>
- <https://www.youtube.com/watch?v=f5jNJDaztqk>


---
## Useful commands to run on cheat sheets

- Find and replace a string on multiple lines, in every cheat sheets:
```console
$ for filename in $(find . -type f -iname '*.md'); do cat $filename | tr '\n' '\f' | sed 's/Bad string\fon multiple lines/g' | tr '\f' '\n' | tee "$filename".tmp && rm "$filename" && mv "$filename".tmp "$filename"; done
```
> Note that the characters needing to be escaped in the `sed` substitution command are: `$.*[\^`


- Insert `New Text` on a new line after `SearchPattern`:
```console
$ sed -i '/SearchPattern/aNew Text' somefile.txt
```

- Insert `New Text` on a new line before `SearchPattern`:
```console
$ sed -i '/SearchPattern/iNew Text' somefile.txt
```

- Replace `* ` by `*   `:
```console
$ sed -r -i 's/\*[[:space:]]{1,99}([^[:space:]])/\*   \1/g' ./*
$ sed -r -i 's/\*[[:space:]]{1,99}([0-9A-Za-z])/\*   \1/g' ./*
```
