#!/bin/sh

# Check no absolute links:
check=$(
    /bin/grep -r 'stephane-cheatsheets.readthedocs' ../docs \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
if [ -n "$check" ]; then
    >&2 echo "❌ absolute links"
    >&2 echo ""
    >&2 echo "The following files do contain absolute links (direct references to"
    >&2 echo "'https://stephane-cheatsheets.readthedocs.io'), please those links by relative links"
    >&2 echo "(e.g. https://stackoverflow.com/a/7658676)."
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    return 1
fi

echo "✅ absolute links"
