#!/bin/sh

# check '!!! Note "Prerequisite(s)"'
check=$( \
      /bin/grep -ri '^!!! Note "pre' ../docs \
    | /bin/grep -v '.md:!!! Note "Prerequisite(s)"$' \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
if [ -n "$check" ] ; then
    >&2 echo "❌ prerequisites"
    >&2 echo ""
    >&2 echo "The following files do contain admonitions 'Prerequisite(s)' notes"
    >&2 echo "(see https://squidfunk.github.io/mkdocs-material/reference/admonitions) that are not"
    >&2 echo "formatted as expected (the expected format is '!!! Note \"Prerequisite(s)\"')."
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    return 1
fi

echo "✅ prerequisites"
