#!/bin/sh

# check '_' instead of '-' in directory name
check=$( \
      /bin/find ../docs -type d -name '*_*'
)
if [ -n "$check" ] ; then
    >&2 echo "❌ directories name"
    >&2 echo ""
    >&2 echo "The following directories name contain the '_' character (instead of '-'),"
    >&2 echo "please replace '_' by '-'."
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    return 1
fi

echo "✅ directories name"
