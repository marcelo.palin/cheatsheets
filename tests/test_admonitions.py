import os
import re
import sys


def print_error_help():
    print(
        "See https://squidfunk.github.io/mkdocs-material/reference/admonitions "
        + "for more details about the syntax.\n",
        file=sys.stderr,
    )
    print(
        "Admonitions syntax rules implemented here:\n"
        + "\n"
        + "    - The line before an admonition block must be empty.\n"
        + "\n"
        + "    - The first line of an admonition block must start with either 0 whitespace,\n"
        + "      or a multiple of 4 whitespaces (with a maximum of 16 whitespaces).\n"
        + "\n"
        + "    - After that, the first line of an admonition block must contain either:\n"
        + "        - `!!!` (not collapsible)\n"
        + "        - `???` (collapsible and collapsed)\n"
        + "        - `???+` (collapsible and not collapsed)\n"
        + "\n"
        + "    - After that, the first line of an admonition block must contain "
        + "a type qualifier.\n"
        + "      E.g. `note`, `warning`, `tip`, etc. See the full list of supported types:\n"
        + "      https://squidfunk.github.io/mkdocs-material/"
        + "reference/admonitions/#supported-types\n"
        + "\n"
        + "    - After that, the first line of an admonition block must contain either:\n"
        + "        - ` inline` (aligned to the left of the bellow text/content block)\n"
        + "        - ` inline end` (aligned to the right of the bellow text/content block)\n"
        + "        - `` (default: full width of the viewport)\n"
        + "\n"
        + "    - After that, the first line of an admonition block must contain either:\n"
        + '        - ` "whatever characters here..."` (a title for the admonition)\n'
        + "        - `` (default: no title)\n"
        + "\n"
        + "    - After that, the end of line must be matched.\n"
        + "\n"
        + "    - The line after the first line of an admonition block must be either:\n"
        + "        - indented with 4 more spaces than the first line\n"
        + "        - `` (empty) and the next line must be indented with 4 more spaces\n"
        + "",
        file=sys.stderr,
    )


def contains_admonition(line: str) -> bool:
    if "!!!" in line or "???" in line:
        return True
    else:
        return False


def check_before_first_line(line: str, line_before: str, line_nb: int, file_path: str) -> bool:
    if not re.match(r"^$", line_before):

        # regex details:
        ################
        #
        # `^$` : match an empty line

        line_before = line_before.replace("\n", "")
        line = line.replace("\n", "")
        print(
            "Bad admonition:\n"
            + f"    The line '{line_before}' (line {line_nb-1}) before '{line}' (line {line_nb})\n"
            + f"    is not empty when it should (file {file_path}).\n",
            file=sys.stderr,
        )
        return True
    else:
        return False


def check_after_first_line(
    line: str, line_after: str, line_after_after: str, line_nb: int, file_path: str
) -> bool:
    if (
        not (
            re.match(r"^(\s){0}(\!\!\!|\?\?\?|\?\?\?\+)\s(.*?)$", line)
            and (
                (re.match(r"^$", line_after) and re.match(r"^(\s){4}", line_after_after))
                or re.match(r"^(\s){4}", line_after)
            )
        )
        and not (
            re.match(r"^(\s){4}(\!\!\!|\?\?\?|\?\?\?\+)\s(.*?)$", line)
            and (
                (re.match(r"^$", line_after) and re.match(r"^(\s){8}", line_after_after))
                or re.match(r"^(\s){8}", line_after)
            )
        )
        and not (
            re.match(r"^(\s){8}(\!\!\!|\?\?\?|\?\?\?\+)\s(.*?)$", line)
            and (
                (re.match(r"^$", line_after) and re.match(r"^(\s){12}", line_after_after))
                or re.match(r"^(\s){12}", line_after)
            )
        )
        and not (
            re.match(r"^(\s){12}(\!\!\!|\?\?\?|\?\?\?\+)\s(.*?)$", line)
            and (
                (re.match(r"^$", line_after) and re.match(r"^(\s){16}", line_after_after))
                or re.match(r"^(\s){16}", line_after)
            )
        )
        and not (
            re.match(r"^(\s){16}(\!\!\!|\?\?\?|\?\?\?\+)\s(.*?)$", line)
            and (
                (re.match(r"^$", line_after) and re.match(r"^(\s){20}", line_after_after))
                or re.match(r"^(\s){20}", line_after)
            )
        )
    ):

        # regex details:
        ################
        #
        # `^` : match beginning of the line
        #
        # `(\s){0}`  : match 0 whitespace
        # `(\s){4}`  : match 4 whitespaces
        # `(\s){8}`  : match 8 whitespaces
        # `(\s){12}` : match 12 whitespaces
        # `(\s){16}` : match 16 whitespaces
        # `(\s){20}` : match 20 whitespaces
        #
        # `(\!\!\!|\?\?\?|\?\?\?\+)` : match `!!!` (not collapsible)
        #                              or match `???` (collapsible and collapsed)
        #                              or match `???+` (collapsible and not collapsed)
        #
        # `\s` : match a whitespace
        #
        # `(.*?)` : match `whatever characters here...`
        #
        # `$` : match end of line

        line = line.replace("\n", "")
        line_after = line_after.replace("\n", "")
        line_after_after = line_after_after.replace("\n", "")
        if len(line_after) != 0:
            print(
                   "Bad admonition:\n"
                + f"    The line '{line_after}' (line {line_nb+1}) "
                + f"after '{line}' (line {line_nb})\n"
                +  "    is not correctly indented: should be 4 more whitespaces "
                + f"(file {file_path}).\n",
                file=sys.stderr,
            )
        else:
            print(
                   "Bad admonition:\n"
                + f"    The line '{line_after_after}' (line {line_nb+2}) "
                + f"after after '{line}' (line {line_nb})\n"
                +  "    is not correctly indented: should be 4 more whitespaces "
                + f"(file {file_path}).\n",
                file=sys.stderr,
            )
        return True
    else:
        return False


def check_first_line(line: str, line_nb: int, file_path: str) -> bool:
    if not re.match(
        r"^((\s){0}|(\s){4}|(\s){8}|(\s){12}|(\s){16})(\!\!\!|\?\?\?|\?\?\?\+)\s"
        + "("
        + "abstract|"
        + "|attention"
        + "|bug"
        + "|caution"
        + "|check"
        + "|cite"
        + "|danger"
        + "|done"
        + "|error"
        + "|example"
        + "|fail"
        + "|failure"
        + "|faq"
        + "|help"
        + "|hint"
        + "|important"
        + "|info"
        + "|missing"
        + "|note"
        + "|question"
        + "|quote"
        + "|star"
        + "|success"
        + "|summary"
        + "|tip"
        + "|tldr"
        + "|todo"
        + "|warning"
        + ")"
        + "( inline| inline end|)"
        + '( "(.*?)"|)$',
        line,
        re.IGNORECASE,
    ):

        # regex details:
        ################
        # (note that the regex is case insensitive)
        #
        # `^` : match beginning of the line
        #
        # `((\s){0}|(\s){4}|(\s){8}|(\s){12}|(\s){16})` : match 0, 4, 8, 12 or 16 whitespace(s)
        #
        # `(\!\!\!|\?\?\?|\?\?\?\+)` : match `!!!` (not collapsible)
        #                              or match `???` (collapsible and collapsed)
        #                              or match `???+` (collapsible and not collapsed)
        #
        # `\s` : match a whitespace
        #
        # `(abstract`
        # `|attention`
        # `|bug`
        # `|...)`     : match a type qualifier, e.g. `abstract` or `attention` or `bug` or ...
        #  (see https://squidfunk.github.io/mkdocs-material/reference/admonitions/#supported-types)
        #
        # `( inline| inline end|)` : match ` inline` (aligned to the left of the text block)
        #                            or match ` inline end` (aligned to the right of the text block)
        #                            or match `` (default: full width of the viewport)
        #
        # `( \"(.*?)\"|)` : match ` "whatever characters here..."` (a title for the admonition)
        #                   or match `` (default: no title)
        #
        # `$` : match end of line

        line = line.replace("\n", "")
        print(
               "Bad admonition:\n"
            + f"    The line '{line}' (line {line_nb})\n"
            + f"    contains an admonition with a wrong syntax (file {file_path}).\n",
            file=sys.stderr,
        )
        return True
    else:
        return False


def contains_bad_admonition(
    line: str,
    line_before: str,
    line_after: str,
    line_after_after: str,
    line_nb: int,
    file_path: str,
) -> bool:
    has_bad_admonition: bool = False
    if check_before_first_line(line, line_before, line_nb, file_path):
        has_bad_admonition = True
    if check_first_line(line, line_nb, file_path):
        has_bad_admonition = True
    if check_after_first_line(line, line_after, line_after_after, line_nb, file_path):
        has_bad_admonition = True
    return has_bad_admonition


def find_markdown_files(path, extension) -> list:
    markdown_files: list = []
    for root, dirs, files in os.walk(path):
        for f in files:
            if f.endswith(extension):
                markdown_files.append(root + "/" + f)
    return markdown_files


def test_admonitions(docs_location):
    has_bad_admonition: bool = False
    md_files_path: list = find_markdown_files(docs_location, ".md")
    for file_path in md_files_path:
        with open(file_path, "r") as file:
            lines = file.readlines()  # see https://stackoverflow.com/a/3277516
            lines = [line for line in lines]

            line_before: str = ""
            line_after: str = ""
            line_after_after: str = ""
            line_nb: int = 0

            for i in range(len(lines)):
                line_nb = i + 1
                line = lines[i]
                if i > 0:
                    line_before = lines[i - 1]
                if i < (len(lines) - 1):
                    line_after = lines[i + 1]
                else:
                    line_after = ""
                if i < (len(lines) - 2):
                    line_after_after = lines[i + 2]
                else:
                    line_after_after = ""

                if contains_admonition(line) and contains_bad_admonition(
                    line, line_before, line_after, line_after_after, line_nb, file_path
                ):
                    has_bad_admonition = True
                line_before = line

    if has_bad_admonition:
        print_error_help()
    assert not has_bad_admonition


if __name__ == "__main__":
    test_admonitions("./docs")
