#!/bin/sh

# Check blank line above `!!!` and `???`
check=$( \
      /bin/grep -r --no-group-separator -B1 -e '!!!' -e '???' ../docs \
    | /bin/grep -v -e '!!!' -e '???' \
    | /bin/grep -v "\.md-$" \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
if [ -n "$check" ]; then
    >&2 echo "❌ empty line above admonitions"
    >&2 echo ""
    >&2 echo "The following files do contain admonitions"
    >&2 echo "(see https://squidfunk.github.io/mkdocs-material/reference/admonitions) that are not"
    >&2 echo "without a blank line above them, please add that blank line."
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    return 1
fi

echo "✅ empty line above admonitions"
