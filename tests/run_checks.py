"""
Entry point of the checking scripts.
Those scripts will check the syntax format of all the markdown files.
"""

import tests.check_files_name
import tests.check_directories_name
import tests.check_shell_configuration_references
import tests.check_dotfile_madness_references
import tests.check_headers_and_footers
import tests.check_main_sections
import tests.check_sub_sections
import tests.check_deep_sub_sections
import tests.check_toc
import tests.check_tags
import tests.check_spelling_mistakes


def run() -> None:
    """
    Run all checking scripts
    """

    tests.check_files_name.check()
    tests.check_directories_name.check()
    #tests.check_shell_configuration_references.check()
    tests.check_dotfile_madness_references.check()
    tests.check_tags.check()
    tests.check_headers_and_footers.check()
    #tests.check_diataxis.check()  # ?
    #tests.check_vale.check()  # ?
    #tests.check_main_sections.check()
    #tests.check_sub_sections.check()
    tests.check_deep_sub_sections.check()
    tests.check_toc.check()
    #tests.check_deep_sub_sections.check()
    tests.check_spelling_mistakes.check()

# TODO: add the possibility to check a single markdown file at the time

# TODO: add the possibility to check a single directory at the time


if __name__ == "__main__":
    run()
