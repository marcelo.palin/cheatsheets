#!/bin/sh

# Check separating line (`---`) above subsection ('## '):
check=$( \
      /bin/grep -r --no-group-separator -B1 "^## " ../docs \
    | /bin/grep -v "## " \
    | /bin/grep -v ".md----$" \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
if [ -n "$check" ] ; then
    >&2 echo "❌ line above sub sections"
    >&2 echo ""
    >&2 echo "The following files do not contain a separating line ('---')"
    >&2 echo "above each subsection ('## '), please add that separating line."
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    return 1
fi

echo "✅ line above sub sections"
