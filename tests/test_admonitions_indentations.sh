#!/bin/sh

# Check indentation below '!!!' and '???'
check1=$( \
      /bin/grep -r --no-group-separator -A1 -e '^!!!' -e '^???' ../docs \
    | /bin/grep -v -e '!!!' -e '???' \
    | /bin/grep -v "\.md-$" \
    | /bin/grep -v "\.md-    \S" \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
check2=$( \
      /bin/grep -r --no-group-separator -A1 -e '^    !!!' -e '^    ???' ../docs \
    | /bin/grep -v -e '!!!' -e '???' \
    | /bin/grep -v "\.md-$" \
    | /bin/grep -v "\.md-        \S" \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
check3=$( \
      /bin/grep -r --no-group-separator -A1 -e '^        !!!' -e '^        ???' ../docs \
    | /bin/grep -v -e '!!!' -e '???' \
    | /bin/grep -v "\.md-$" \
    | /bin/grep -v "\.md-            \S" \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
check4=$( \
      /bin/grep -r --no-group-separator -A1 -e '^            !!!' -e '^            ???' ../docs \
    | /bin/grep -v -e '!!!' -e '???' \
    | /bin/grep -v "\.md-$" \
    | /bin/grep -v "\.md-                \S" \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
check5=$( \
      /bin/grep -r --no-group-separator -A1 -e '^                !!!' -e '^                ???' ../docs \
    | /bin/grep -v -e '!!!' -e '???' \
    | /bin/grep -v "\.md-$" \
    | /bin/grep -v "\.md-                    \S" \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
check=$( \
      printf "%s\n%s\n%s\n%s" "$check1" "$check2" "$check3" "$check4" "$check5"\
    | sort -u \
)
if [ -n "$check" ] ; then
    >&2 echo "❌ admonitions indentations"
    >&2 echo ""
    >&2 echo "The following files do not contain the correct indentation below each '!!!' and/or '???',"
    >&2 echo "please correct that indentation (4 spaces per indentation)."
    >&2 echo ""
    >&2 echo "(sometimes, striping the trailing whitespaces is enough)"
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    >&2 echo "Tip: you can run './tests/print_admonitions_indentations.sh'"
    >&2 echo "in order to get the file and the file number of each bad indentation."
    return 1
fi

echo "✅ admonitions indentations"
