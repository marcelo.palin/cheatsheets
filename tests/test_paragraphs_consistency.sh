#!/bin/sh

set -e

# Check for paragraphs consistency:

#check=$(for filename in $(find ../docs -type f -iname '*.md'); do awk '/^\s\s./ && ! /===/' RS="\n\n" ORS="\n\n" $filename; done)

#check=$(awk '/^[[:blank:]]/ && !/^\s\s\s\s/ && !/===/' RS="\n\n" ORS="\n\n" "$file")

find ../docs -type f -iname "*.md" | while read -r file
do
    check=$(awk '/^[[:blank:]]/ && !/^    / && !/===/' RS="\n\n" ORS="\n\n" "$file")
    if [ -n "$check" ]; then
        >&2 echo "❌ paragraphs consistency"
        >&2 echo ""
        >&2 echo "The file '$file' might contain paragraphs that are not consistent."
        >&2 echo "I.e. some lines might appear in dedicated paragraphs when they shouldn't."
        >&2 echo ""
        >&2 echo "Here are the lines that might need to be \"merged back\" into "
        >&2 echo "their original paragraph:"
        >&2 echo ""
        >&2 echo "$check"
        >&2 echo ""
        >&2 echo "(generally, \"merging back\" just mean removing the previous blank line,"
        >&2 echo "or filling it...)"
        >&2 echo ""
        return 1
    fi
done

echo "✅ paragraphs consistency"
