import pytest


def pytest_addoption(parser):
    parser.addoption("--docs-location", action="store", default="./docs", help="")
    parser.addoption("--dict-location", action="store", default="./custom_dict.txt", help="")


@pytest.fixture
def docs_location(request):
    return request.config.getoption("--docs-location")


@pytest.fixture
def dict_location(request):
    return request.config.getoption("--dict-location")
