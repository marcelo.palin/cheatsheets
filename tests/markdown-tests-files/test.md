---
tags:
  - Test
  - Tset
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Test

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim id est laborum.

???+ Note "Reference(s)"
    * <https://ref.erence/>
    * `$ reference --help`
    * `$ man reference`


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [First sub section name](#first-sub-section-name)
* [Other sub section name](#other-sub-section-name)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->


---
## First sub section name

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim id est laborum.


---
## Other sub section name

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim id est laborum.

!!! Note ""

    === "apk"
        ```console
        # apk add test
        ```

    === "apt"
        ```console
        # apt install test
        ```

    === "dnf"
        ```console
        # dnf install test
        ```

    === "emerge"
        ```console
        # emerge -a test/test
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.test
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.test
            ```

    === "pacman"
        ```console
        # pacman -S test
        ```

        !!! Tip "For Artix users"
            * **If** using `dinit`:
            ```console
            # pacman -S test test-dinit
            ```
            * **If** using `openrc`:
            ```console
            # pacman -S test test-openrc
            ```
            * **If** using `runit`:
            ```console
            # pacman -S test test-runit
            ```
            * **If** using `s6`:
            ```console
            # pacman -S test test-s6
            ```

    === "yum"
        ```console
        # yum install test
        ```

    === "xbps"
        ```console
        # xbps-install -S test
        ```

    === "zypper"
        ```console
        # zypper install test
        ```

!!! Note ""

    === "OpenRC"
        ```console
        # rc-update add test default
        # rc-service test start
        ```

    === "Runit"
        Depending on your `runit` implementation, either run:
        ```console
        # ln -s /etc/runit/sv/test /service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/test /var/service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/test /run/runit/service
        ```
        In any case, finally run:
        ```console
        # sv up test
        ```

    === "SysVinit"
        ```console
        # service test start
        # chkconfig test on
        ```

    === "SystemD"
        ```console
        # systemctl enable test
        # systemctl start test
        ```


---
## Config

```console
# vi /etc/test
    >
    >
    >
    >
    >
    >
    >
    >
```


---
## Use

* Test:
  ```console
  $ test
  ```

* Test:
  ```console
  $ test
  ```

* Test:
  ```console
  $ test
  ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
