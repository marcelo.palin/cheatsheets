#!/bin/sh

# Check empty line above '==='
check=$( \
      /bin/grep -r --no-group-separator -B1 "===" ../docs \
    | /bin/grep -v "===" \
    | /bin/grep -v "\.md-$" \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
if [ -n "$check" ]; then
    >&2 echo "❌ empty line above content tabs"
    >&2 echo ""
    >&2 echo "The following files do not contain a blank line above each '===',"
    >&2 echo "please add that blank line."
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    return 1
fi

echo "✅ empty line above content tabs"
