#!/bin/sh

# Check⚠️  instead of /!\
check=$( \
    /bin/grep -r '\/!\\' ../docs \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
if [ -n "$check" ]; then
    >&2 echo "❌ warning signs"
    >&2 echo ""
    >&2 echo "The following files do contain '/!\', please replace it by ⚠️ "
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    return 1
fi

echo "✅ warning signs"
