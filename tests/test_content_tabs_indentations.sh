#!/bin/sh

# Check indentation below '==='
check1=$( \
      /bin/grep -r --no-group-separator -A1 '^===' ../docs \
    | /bin/grep -v '===' \
    | /bin/grep -v "\.md-$" \
    | /bin/grep -v "\.md-    \S" \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
check2=$( \
      /bin/grep -r --no-group-separator -A1 '^    ===' ../docs \
    | /bin/grep -v '===' \
    | /bin/grep -v "\.md-$" \
    | /bin/grep -v "\.md-        \S" \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
check3=$( \
      /bin/grep -r --no-group-separator -A1 '^        ===' ../docs \
    | /bin/grep -v '===' \
    | /bin/grep -v "\.md-$" \
    | /bin/grep -v "\.md-            \S" \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
check4=$( \
      /bin/grep -r --no-group-separator -A1 '^            ===' ../docs \
    | /bin/grep -v '===' \
    | /bin/grep -v "\.md-$" \
    | /bin/grep -v "\.md-                \S" \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
check=$( \
      printf "%s\n%s\n%s\n%s" "$check1" "$check2" "$check3" "$check4" \
    | sort -u \
)
if [ -n "$check" ] ; then
    >&2 echo "❌ content tabs indentations"
    >&2 echo ""
    >&2 echo "The following files do not contain the correct indentation below each '===',"
    >&2 echo "please correct that indentation (4 spaces per indentation)."
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    return 1
fi

echo "✅ content tabs indentations"
